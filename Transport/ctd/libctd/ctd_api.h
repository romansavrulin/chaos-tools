#ifndef CTD_API_H
#define CTD_API_H

#include "ctd_srv.h"
#include "ctd_cli.h"

namespace Componentality
{
	namespace CTD
	{

		enum PROTOCOL
		{
			TCP,
			UDP
		};

		Server* createServer(const PROTOCOL, const int port);
		CST::Common::thread* runServer(Server*);
		void stopServer(CST::Common::thread*, Server*);

		Client* createClient(const PROTOCOL, const std::string host, const int port, const int lport = -1);
		CST::Common::thread* runClient(Client*, const bool blocking = true);
		void stopClient(CST::Common::thread*, Client*);

		Agent* queueToCheck(Server*, const std::string readfolder, const std::string writefolder, const PROTOCOL);
		Agent* queueToPut(Server*, const std::string readfolder, const std::string writefolder, const PROTOCOL);
		Agent* queueToCheck(Server*, const std::list<std::string> sendlist, const std::string folder, const PROTOCOL);
		Agent* queueToPut(Server*, const std::list<std::string> sendlist, const std::string folder, const PROTOCOL);
		Agent* queueToGet(Server*, const std::list<std::string> files, const std::string folder, const PROTOCOL);

		Agent* queueToCheck(Client*, const std::string readfolder, const std::string writefolder, const PROTOCOL);
		Agent* queueToPut(Client*, const std::string readfolder, const std::string writefolder, const PROTOCOL);
		Agent* queueToCheck(Client*, const std::list<std::string> sendlist, const std::string folder, const PROTOCOL);
		Agent* queueToPut(Client*, const std::list<std::string> sendlist, const std::string folder, const PROTOCOL);
		Agent* queueToGet(Client*, const std::list<std::string> files, const std::string folder, const PROTOCOL);

		Agent* setupExchange(Client*, const std::string readfolder, const std::string writefolder);
		Agent* setupExchange(Server*, const std::string readfolder, const std::string writefolder);

		void stopTransfer(Client*, Agent*);
		void stopTransfer(Server*, Agent*);
	}
}

#endif