/****************************************************************************************************/
/* Componentality Chaos Transfer Library             * ctd_cli.h    								*/
/****************************************************************************************************/
/* Copyright (C) Componentality Oy, 2014                                                            */
/****************************************************************************************************/
/* CGP protocol implementation: basic classes          												*/
/****************************************************************************************************/

#ifndef CTD_CMD_H
#define CTD_CMD_H

#include "../../CommonLibs/common/common_utilities.h"
#include "ctd_chunk.h"
#include "ctd_ctx.h"

namespace Componentality
{
	namespace CTD
	{
		// Command processor itself
		class CommandProcessor
		{
		protected:
			ChunkProvider& mProvider;					// Abstract provider of named data items (chunks) from cache or file system
		public:
			CommandProcessor(ChunkProvider& provider) : mProvider(provider) {};
			virtual ~CommandProcessor() {};
			// CGP protocol execution
			virtual CST::Common::blob operator()(const CST::Common::blob&);
		};

		class Exchanger;

		// Agents perform actions upon connection, disconnection, communication start and end
		class Agent
		{
		public:
			Agent() {};
			virtual ~Agent() {};
		public:
			virtual void onStart(Exchanger&) = 0;
			virtual void onEnd(Exchanger&) = 0;
			virtual void onConnect(Exchanger&) = 0;
			virtual void onDisconnect(Exchanger&) = 0;
		};

		// Default agent is used for folder-based synchronization
		class DefAgent : public Agent
		{
		protected:
			std::string mReadFolder;
			std::string mWriteFolder;
			Context& mContext;
		public:
			DefAgent(Context& ctx, const std::string readfolder, const std::string writefolder);
			virtual ~DefAgent();
		public:
			virtual void onStart(Exchanger&);
			virtual void onEnd(Exchanger&);
			virtual void onConnect(Exchanger&);
			virtual void onDisconnect(Exchanger&);
		};

		// Standard agent is used for per-file synchronization (check and put only depending on put flag)
		class StdAgent : public Agent
		{
		protected:
			std::string mReadFolder;
			std::string mWriteFolder;
			bool mOneAtATime;
			bool mPut;
			Context& mContext;
		public:
			StdAgent(Context& ctx, const std::string readfolder, const std::string writefolder, const bool put = false, const bool one_at_a_time = false);
			virtual ~StdAgent();
		public:
			virtual void onStart(Exchanger&);
			virtual void onEnd(Exchanger&);
			virtual void onConnect(Exchanger&);
			virtual void onDisconnect(Exchanger&);
		};

		// Get agent is used for retrieval of the given set of files (specified by "request")
        class GetAgent : public Agent
		{
		protected:
			std::list<std::string> mRequest;
			std::string mWriteFolder;
			bool mOneAtATime;
			int mCounter;
			Context& mContext;
		public:
			GetAgent(Context& ctx, const std::list<std::string>& request, const std::string writefolder, const bool one_at_a_time = false);
			virtual ~GetAgent();
		public:
			virtual void onStart(Exchanger&);
			virtual void onEnd(Exchanger&);
			virtual void onConnect(Exchanger&);
			virtual void onDisconnect(Exchanger&);
		};

		// Send agent is used to initiate conditional (put=false) or unconditional (put=true) put of the given set of files
		class SendAgent : public Agent
		{
		protected:
			std::list<std::string> mSendList;
			std::string mWriteFolder;
			bool mOneAtATime;
			bool mPut;
			Context& mContext;
		public:
			SendAgent(Context& ctx, const std::list<std::string>& sendlist, const std::string writefolder, const bool put = false, const bool one_at_a_time = false);
			virtual ~SendAgent();
		public:
			virtual void onStart(Exchanger&);
			virtual void onEnd(Exchanger&);
			virtual void onConnect(Exchanger&);
			virtual void onDisconnect(Exchanger&);
		};

		// Protocol exchange engine. Performs thread save CGP execution for multiple registered agents
		class Exchanger : protected CommandProcessor
		{
		protected:
			CST::Common::blob mReadBuffer;
			CST::Common::blob mWriteBuffer;
			CST::Common::mutex mReadLock;
			CST::Common::mutex mWriteLock;
			CST::Common::mutex mAgentsLock;
			std::list<Agent*> mAgents;
		public:
			Exchanger(ChunkProvider&);
			virtual ~Exchanger();
		public:
			virtual void onReceived(CST::Common::blob data);
			virtual void send(CST::Common::blob data);					// Unconditional data sending to the write buffer
			virtual Exchanger& operator()(void);						// Perform exchange process
			virtual bool isThereReadCommand();							// Check if there is a complete command in read buffer
			virtual bool isThereWriteCommand();							// Check if there is a complete command in write buffer
			virtual CST::Common::blob getDataToWrite(const size_t maxdata);	// Retrieve data from write buffer
			virtual Exchanger& operator+= (Agent&);						// Add new agent
			virtual Exchanger& operator-= (Agent&);						// Remove existing registered agent
			virtual operator std::list<Agent*>();						// Get list of registered agents
		public:
			virtual void check(const std::string& chunk_name);
			virtual void get(const std::string& chunk_name);
			virtual void put(const std::string& chunk_name, CST::Common::blob data);
		public:
			virtual void onStart();
			virtual void onEnd();
			virtual void onConnect();
			virtual void onDisconnect();
		};

		// Retriever of Exchanger, used for Exchanger aggregation to clients and servers
		class ExchangeProvider
		{
		public:
			ExchangeProvider() {};
			virtual ~ExchangeProvider() {};
		public:
			virtual operator Exchanger&() = 0;
		};

		// Handler used to manage Exchanger from server threads
		struct SERVER_HANDLER
		{
			int mSocket;								// Socket id
			Exchanger* mProcessor;						// Exchanger
			CST::Common::ThreadSet* mRunnable;			// Thread pool
			bool mExitIfNoData;							// Flag to exit if no more data to transfer
		};

	}
}

#endif