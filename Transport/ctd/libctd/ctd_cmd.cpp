#include "ctd_cmd.h"
#include <memory.h>

using namespace Componentality::CTD;

// Current stream state
enum __State
{
	COMMAND,
	CHUNK_NAME,
	CONTENT
};

// Command type
enum __Type
{
	UNKNOWN,
	CHECK,
	GET,
	PUT
};

// CGP protocol execution
CST::Common::blob CommandProcessor::operator()(const CST::Common::blob& cmdset)
{
	CST::Common::blob response;
	__State state = COMMAND;
	__Type type = UNKNOWN;
	std::string name;
	char* content_ptr = NULL;
	for (size_t i = 0; i < cmdset.mSize; i++)
	{
		switch (state)
		{
		case COMMAND:
			{
				switch (cmdset.mData[i])
				{
				case 'c':
					type = CHECK;
					break;
				case 'g':
					type = GET;
					break;
				case 'p':
					type = PUT;
					break;
				default:
					type = UNKNOWN;
				}
				if (type == UNKNOWN)
					return CST::Common::blob();
				else
					state = CHUNK_NAME;
			}
			break;
		case CHUNK_NAME:
			if (cmdset.mData[i] != 0)
				name += cmdset.mData[i];
			else
				if (type == PUT)
					state = CONTENT, content_ptr = cmdset.mData + i + 1;
				else
					return CST::Common::blob();
			break;
		}
	}
	switch (type)
	{
	case CHECK:
		if (!mProvider.exists(name))
		{
			response.mSize = name.size() + 1;
			response.mData = new char[response.mSize];
			response.mData[0] = 'g';
			memcpy(response.mData + 1, name.c_str(), name.size());
		}
		break;
	case GET:
		if (mProvider.exists(name))
		{
			CST::Common::blob content = mProvider.read(name);
			response.mSize = 1 + name.size() + 1 + content.mSize;
			response.mData = new char[response.mSize];
			response.mData[0] = 'p';
			memcpy(response.mData + 1, name.c_str(), name.size());
			response.mData[name.size() + 1] = 0;
			memcpy(response.mData + name.size() + 2, content.mData, content.mSize);
		}
		break;
	case PUT:
		CST::Common::blob content;
		if (content_ptr)
		{
			content.mSize = cmdset.mSize - (content_ptr - cmdset.mData);
			if (content.mSize)
			{
				content.mData = new char[content.mSize];
				memcpy(content.mData, content_ptr, content.mSize);
			}
			mProvider.write(name, content);
			content.mData = NULL;
			content.mSize = 0;
		}
		break;
	}

	return response;
}

/***************************************************************************************************/

Exchanger::Exchanger(ChunkProvider& cp) : CommandProcessor(cp)
{
}

Exchanger::~Exchanger()
{
}

void Exchanger::onReceived(CST::Common::blob data)
{
	mReadLock.lock();

	CST::Common::blob readdata;
	readdata.mSize = mReadBuffer.mSize + data.mSize;
	if (readdata.mSize)
		readdata.mData = new char[readdata.mSize];
	if (mReadBuffer.mSize)
		memcpy(readdata.mData, mReadBuffer.mData, mReadBuffer.mSize);
	if (data.mSize)
		memcpy(readdata.mData + mReadBuffer.mSize, data.mData, data.mSize);
	mReadBuffer.purge();
	mReadBuffer = readdata;

	mReadLock.unlock();
}

void Exchanger::send(CST::Common::blob data)
{
	mWriteLock.lock();

	CST::Common::blob writedata;
	writedata.mSize = mWriteBuffer.mSize + data.mSize;
	if (writedata.mSize)
		writedata.mData = new char[writedata.mSize];
	if (mWriteBuffer.mSize)
		memcpy(writedata.mData, mWriteBuffer.mData, mWriteBuffer.mSize);
	if (data.mSize)
		memcpy(writedata.mData + mWriteBuffer.mSize, data.mData, data.mSize);
	mWriteBuffer.purge();
	mWriteBuffer = writedata;

	mWriteLock.unlock();
}

Exchanger& Exchanger::operator()(void)
{
	mReadLock.lock();
	bool zero = false;
	bool ok = false;
	char* nextptr = mReadBuffer.mData;
	for (size_t i = 0; i < mReadBuffer.mSize; i++)
	{
		if (mReadBuffer.mData[i])
		{
			if (zero)
				nextptr += 2, zero = false;
			else
				nextptr += 1;
		}
		else
		{
			if (zero)
			{
				ok = true;
				break;
			}
			else
			{
				zero = true;
			}
		}
	}

	if (ok)
	{
		CST::Common::blob command;
		command.mSize = nextptr - mReadBuffer.mData;
		if (command.mSize)
		{
			command.mData = new char[command.mSize];
			memcpy(command.mData, mReadBuffer.mData, command.mSize);
		}
		CST::Common::blob tail;
		tail.mSize = mReadBuffer.mSize - command.mSize - 2;
		if (tail.mSize)
		{
			tail.mData = new char[tail.mSize];
			memcpy(tail.mData, mReadBuffer.mData + command.mSize + 2, tail.mSize);
		}
		mReadBuffer.purge();
		mReadBuffer = tail;
		mReadLock.unlock();

		CST::Common::blob response = CommandProcessor::operator()(command);
		command.purge();
		if (response.mSize)
		{
			CST::Common::blob rsp;
			rsp.mSize = response.mSize + 2;
			rsp.mData = new char[rsp.mSize];
			memcpy(rsp.mData, response.mData, response.mSize);
			rsp.mData[response.mSize] = 0;
			rsp.mData[response.mSize + 1] = 0;
			response.purge();
			send(rsp);
			rsp.purge();
		}
	}
	return *this;
}

bool Exchanger::isThereReadCommand()
{
	mReadLock.lock();
	bool zero = false;
	bool ok = false;
	for (size_t i = 0; i < mReadBuffer.mSize; i++)
	{
		if (mReadBuffer.mData[i])
		{
			zero = false;
		}
		else
		{
			if (zero)
			{
				ok = true;
				break;
			}
			else
			{
				zero = true;
			}
		}
	}
	mReadLock.unlock();
	return ok;
}

bool Exchanger::isThereWriteCommand()
{
	mWriteLock.lock();
	bool zero = false;
	bool ok = false;
	for (size_t i = 0; i < mWriteBuffer.mSize; i++)
	{
		if (mWriteBuffer.mData[i])
		{
			zero = false;
		}
		else
		{
			if (zero)
			{
				ok = true;
				break;
			}
			else
			{
				zero = true;
			}
		}
	}
	mWriteLock.unlock();
	return ok;
}
			
void Exchanger::check(const std::string& chunk_name)
{
	CST::Common::blob command;
	command.mSize = chunk_name.size() + 3;
	command.mData = new char[command.mSize];
	command.mData[0] = 'c';
	memcpy(command.mData + 1, chunk_name.c_str(), chunk_name.size());
	command.mData[command.mSize - 2] = 0;
	command.mData[command.mSize - 1] = 0;
	send(command);
	command.purge();
}

void Exchanger::get(const std::string& chunk_name)
{
	CST::Common::blob command;
	command.mSize = chunk_name.size() + 3;
	command.mData = new char[command.mSize];
	command.mData[0] = 'g';
	memcpy(command.mData + 1, chunk_name.c_str(), chunk_name.size());
	command.mData[command.mSize - 2] = 0;
	command.mData[command.mSize - 1] = 0;
	send(command);
	command.purge();
}

void Exchanger::put(const std::string& chunk_name, CST::Common::blob data)
{
	CST::Common::blob command;
	command.mSize = chunk_name.size() + 4 + data.mSize;
	command.mData = new char[command.mSize];
	command.mData[0] = 'p';
	memcpy(command.mData + 1, chunk_name.c_str(), chunk_name.size());
	command.mData[chunk_name.size() + 1] = 0;
	memcpy(command.mData + chunk_name.size() + 2, data.mData, data.mSize);
	command.mData[command.mSize - 2] = 0;
	command.mData[command.mSize - 1] = 0;
	send(command);
	command.purge();
}

CST::Common::blob Exchanger::getDataToWrite(const size_t maxdata)
{
	CST::Common::blob result;
	mWriteLock.lock();
	if (maxdata > 0)
		result.mSize = maxdata < mWriteBuffer.mSize ? maxdata : mWriteBuffer.mSize;
	else
		result.mSize = mWriteBuffer.mSize;
	if (result.mSize)
	{
		result.mData = new char[result.mSize];
		memcpy(result.mData, mWriteBuffer.mData, result.mSize);
		CST::Common::blob newbuf;
		newbuf.mSize = mWriteBuffer.mSize - result.mSize;
		if (newbuf.mSize)
		{
			newbuf.mData = new char[newbuf.mSize];
			memcpy(newbuf.mData, mWriteBuffer.mData + result.mSize, newbuf.mSize);
		}
		mWriteBuffer.purge();
		mWriteBuffer = newbuf;
	}
	mWriteLock.unlock();
	return result;
}

void Exchanger::onStart()
{
	mAgentsLock.lock();
	for (std::list<Agent*>::iterator i = mAgents.begin(); i != mAgents.end(); i++)
		(*i)->onStart(*this);
	mAgentsLock.unlock();
}

void Exchanger::onEnd()
{
	mAgentsLock.lock();
	for (std::list<Agent*>::iterator i = mAgents.begin(); i != mAgents.end(); i++)
		(*i)->onEnd(*this);
	mAgentsLock.unlock();
}

void Exchanger::onConnect()
{
	mAgentsLock.lock();
	for (std::list<Agent*>::iterator i = mAgents.begin(); i != mAgents.end(); i++)
		(*i)->onConnect(*this);
	if (!mAgents.empty())
	{
		mAgents.push_back(mAgents.front());
		mAgents.pop_front();
	}
	mAgentsLock.unlock();
}

void Exchanger::onDisconnect()
{
	mAgentsLock.lock();
	for (std::list<Agent*>::iterator i = mAgents.begin(); i != mAgents.end(); i++)
		(*i)->onDisconnect(*this);
	mAgentsLock.unlock();
}

Exchanger& Exchanger::operator+= (Agent& agent)
{
	mAgentsLock.lock();
	mAgents.push_back(&agent);
	mAgentsLock.unlock();
	return *this;
}

Exchanger& Exchanger::operator-= (Agent& agent)
{
	mAgentsLock.lock();
	std::list<Agent*> newlist;
	for (std::list<Agent*>::iterator i = mAgents.begin(); i != mAgents.end(); i++)
		if (&agent != *i)
			newlist.push_back(*i);
	mAgents = newlist;
	mAgentsLock.unlock();
	return *this;
}

Exchanger::operator std::list<Agent*>()
{
	std::list<Agent*> result;
	mAgentsLock.lock();
	result = mAgents;
	mAgentsLock.unlock();
	return result;
};

/****************************************************************************************************************************************/

StdAgent::StdAgent(Context& ctx, const std::string readfolder, const std::string writefolder, const bool put, const bool one_at_a_time) : mContext(ctx)
{
	mReadFolder = readfolder;
	mWriteFolder = writefolder;
	mOneAtATime = one_at_a_time;
	mPut = put;
}

StdAgent::~StdAgent()
{
}

void StdAgent::onStart(Exchanger& exc)
{
	mContext.loadFolder(mReadFolder);
}

void StdAgent::onEnd(Exchanger& exc)
{
	mContext.saveFolder(mWriteFolder);
}

void StdAgent::onConnect(Exchanger& exc)
{
	if (mOneAtATime && exc.isThereWriteCommand())
		return;
	for (size_t i = 0; i < mContext.getSize(); i++)
	{
		std::pair<std::string, CST::Common::blob> item = mContext.get();
		++mContext;
		if (!item.first.empty())
		{
			if (!mPut)
				exc.check(item.first);
			else
				exc.put(item.first, item.second);
		}
		if (mOneAtATime)
			break;
	}
}

void StdAgent::onDisconnect(Exchanger& exc)
{
	mContext.saveFolder(mWriteFolder);
	mContext.loadFolder(mReadFolder);
}

/************************************************************************************************************/

GetAgent::GetAgent(Context& ctx,
	const std::list<std::string>& request,
	const std::string writefolder,
	const bool one_at_a_time) : mContext(ctx), mRequest(request), mWriteFolder(writefolder), mOneAtATime(one_at_a_time)
{
	mCounter = 0;
}

GetAgent::~GetAgent()
{
}

void GetAgent::onStart(Exchanger& exc)
{
	if (mOneAtATime && !exc.isThereWriteCommand())
	{
		std::list<std::string>::iterator i = mRequest.begin();
		if (i != mRequest.end())
		{
			exc.get(*i);
			mRequest.push_back(mRequest.front());
			mRequest.pop_front();
		}
		mCounter += 1;
	}
}

void GetAgent::onEnd(Exchanger& exc)
{
	mContext.saveFolder(mWriteFolder);
}

void GetAgent::onConnect(Exchanger& exc)
{
	if (mOneAtATime && exc.isThereWriteCommand())
	{
		return;
	}

	if ((mCounter == mRequest.size()) && mOneAtATime)
	{
		mCounter = 0;
	}
	else
	{
		for (std::list<std::string>::iterator i = mRequest.begin(); i != mRequest.end(); i++)
		{
			exc.get(*i);
			mCounter += 1;
			if (mOneAtATime)
			{
				mRequest.push_back(mRequest.front());
				mRequest.pop_front();
				break;
			}
		}
	}
}

void GetAgent::onDisconnect(Exchanger& exc)
{
}

/****************************************************************************************************************************************/

DefAgent::DefAgent(Context& ctx, const std::string readfolder, const std::string writefolder) : mContext(ctx)
{
	mReadFolder = readfolder;
	mWriteFolder = writefolder;
}

DefAgent::~DefAgent()
{
}

void DefAgent::onStart(Exchanger& exc)
{
}

void DefAgent::onEnd(Exchanger& exc)
{
}

void DefAgent::onConnect(Exchanger& exc)
{
	mContext.loadFolder(mReadFolder);
}

void DefAgent::onDisconnect(Exchanger& exc)
{
	mContext.saveFolder(mWriteFolder);
}

/************************************************************************************************************/

SendAgent::SendAgent(Context& ctx, const std::list<std::string>& sendlist, const std::string writefolder, const bool put, const bool one_at_a_time) : mContext(ctx)
{
	mSendList = sendlist;
	mWriteFolder = writefolder;
	mOneAtATime = one_at_a_time;
	mPut = put;
}

SendAgent::~SendAgent()
{
}

void SendAgent::onStart(Exchanger& exc)
{
	for (std::list<std::string>::iterator i = mSendList.begin(); i != mSendList.end(); i++)
		mContext.loadFile(*i);
}

void SendAgent::onEnd(Exchanger& exc)
{
	mContext.saveFolder(mWriteFolder);
}

void SendAgent::onConnect(Exchanger& exc)
{
	if (mOneAtATime && exc.isThereWriteCommand())
		return;
	for (size_t i = 0; i < mContext.getSize(); i++)
	{
		std::pair<std::string, CST::Common::blob> item = mContext.get();
		++mContext;
		if (!item.first.empty())
		{
			if (!mPut)
				exc.check(item.first);
			else
				exc.put(item.first, item.second);
		}
		if (mOneAtATime)
			break;
	}
}

void SendAgent::onDisconnect(Exchanger& exc)
{
	mContext.saveFolder(mWriteFolder);
}

/************************************************************************************************************/
