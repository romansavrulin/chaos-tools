#ifndef CTD_CHUNK_H
#define CTD_CHUNK_H

#include "../../CommonLibs/common/common_utilities.h"
#include <string>
#include <map>

namespace Componentality
{
	namespace CTD
	{
		struct Chunk
		{
			CST::Common::blob mContent;
			bool mReceived;
			bool mSent;
			Chunk() { mReceived = false; mSent = false; }
		};

		typedef std::map<std::string, Chunk> ChunkList;

		class ChunkRotator
		{
		protected:
			ChunkList mChunks;
			size_t mIndex;
			CST::Common::mutex mLock;
		protected:
			std::map<std::string, Chunk>::iterator locate();
		public:
			ChunkRotator();
			virtual ~ChunkRotator();
			virtual std::pair<std::string, CST::Common::blob> get();
			virtual size_t getIndex();
			virtual ChunkRotator& operator++();
			virtual size_t getSize();
		};

		class ChunkProvider
		{
		public:
			ChunkProvider() {};
			virtual ~ChunkProvider() {};
		public:
			virtual bool exists(const std::string&) = 0;
			virtual CST::Common::blob read(const std::string&) = 0;
			virtual void write(const std::string&, const CST::Common::blob) = 0;
			virtual void remove(const std::string&) = 0;
		};

	}
}

#endif