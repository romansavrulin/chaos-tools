#ifndef CTD_CTX_H
#define CTD_CTX_H

#include "ctd_chunk.h"

namespace Componentality
{
	namespace CTD
	{

		class Context : public ChunkProvider, public ChunkRotator
		{
		public:
			Context();
			virtual ~Context();
		public:
			virtual void load(const std::string& chunk_name, const CST::Common::blob);
			virtual void loadAs(const std::string& chunk_name, const std::string& file_name);
			virtual void loadFile(const std::string& file_name);
			virtual void loadFolder(const std::string& folder_name);
			virtual CST::Common::blob save(const std::string& chunk_name);
			virtual void saveAs(const std::string& chunk_name, const std::string& file_name);
			virtual void saveFile(const std::string& chunk_name, const std::string& folder_name);
			virtual void saveFolder(const std::string& folder_name, const bool received = true);
			virtual std::list<std::string> list(const bool received_only = false, const bool sent_only = false);
		public:
			virtual bool exists(const std::string&);
			virtual CST::Common::blob read(const std::string&);
			virtual void write(const std::string&, const CST::Common::blob);
			virtual void remove(const std::string&);
			virtual void reset();
		};

	}
}

#endif