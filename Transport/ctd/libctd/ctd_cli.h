/****************************************************************************************************/
/* Componentality Chaos Transfer Library             * ctd_cli.h    								*/
/****************************************************************************************************/
/* Copyright (C) Componentality Oy, 2014                                                            */
/****************************************************************************************************/
/* Abstract client class for CGP-based data exchange   												*/
/****************************************************************************************************/

#ifndef CTD_CLI_H
#define CTD_CLI_H

#include "ctd_cmd.h"
#include "ctd_ctx.h"

namespace Componentality
{
	namespace CTD
	{
		// Socket client. Connects to the server and initiates CGP exchange
		class Client : public Context, protected CST::Common::ThreadSet, public ExchangeProvider
		{
		protected:
			Exchanger mExchange;					// CGP processor
			std::string mHost;						// IP address of the target host
			int mPort;								// Target port number
		public:
			Client(const std::string host, const int port);
			virtual ~Client();
			virtual operator Exchanger&() { return mExchange; }
			virtual void exit() { CST::Common::ThreadSet::exit(); }
		public:
			// Client and server must be run manually (don't run upon construction)
			virtual void run() = 0;
		};


		class TCPClient : public Client
		{
		public:
			TCPClient(const std::string host, const int port);
			virtual ~TCPClient();
		public:
			virtual void run();
		};

		class UDPClient : public Client
		{
		protected:
			int mLocalPort;
			int mTimeout;
		public:
			UDPClient(const std::string host, const int port, const int localPort, const int timeout = 5);
			UDPClient(const std::string host, const int port, const int timeout = 5);
			virtual ~UDPClient();
		public:
			virtual void run();
		};

	}
}

#endif