#include "cache_utils.h"
#include <memory.h>

using namespace Componentality::CTD;

const char* C16 = "0123456789abcdef";

std::string Componentality::CTD::x_encode(const CST::Common::blob data)
{
	std::string result;
	for (size_t i = 0; i < data.mSize; i++)
	{
		unsigned x = (((unsigned char)data.mData[i]) & 0xF0) >> 4;
		result += C16[x];
		x = ((unsigned char)data.mData[i]) & 0x0F;
		result += C16[x];
	}
	return result;
}

CST::Common::blob Componentality::CTD::x_decode(const std::string str)
{
	CST::Common::blob result;
	result.mSize = str.size() / 2;
	result.mData = new char[result.mSize];
	for (size_t i = 0; i < str.size() / 2; i++)
	{
		char x = str[2 * i];
		char* pos = (char*) memchr(C16, x, 16);
		if (!pos)
		{
			result.purge();
			return CST::Common::blob();
		}
		result.mData[i] = (pos - C16) << 4;
		x = str[2 * i + 1];
		pos = (char*) memchr(C16, x, 16);
		result.mData[i] |= (pos - C16) & 0x0F;
	}
	return result;
}
