#ifndef CACHE_CHUNK_H
#define CACHE_CHUNK_H

#include "cache_utils.h"
#include "../../CommonLibs/sha1/CompoSHA.h"
#include "../../CommonLibs/common/common_utilities.h"

namespace Componentality
{
	namespace CTD
	{
		class Chunkenizer
		{
		public:
			Chunkenizer() {};
			virtual ~Chunkenizer() {};
		public:
			std::pair<std::list<std::string>, std::string> split(const std::string& file, const std::string& target_dir, const size_t chunk_size = 4096);
			std::pair<bool, std::pair<std::string, std::string> > merge(const std::string& first_chunk, const std::string& source_dir, std::string& target_dir);
		};
	}
}

#endif