#ifndef CACHE_UTILS_H
#define CACHE_UTILS_H

#include "../../CommonLibs/common/common_utilities.h"

namespace Componentality
{
	namespace CTD
	{
		std::string x_encode(const CST::Common::blob);
		CST::Common::blob x_decode(const std::string);
	}
}

#endif