/****************************************************************************************************/
/* CST Middleware 2009                        * Serializable.h									    */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/* Copyright (C) Componentality Oy, 2012-2013                                                       */
/* Copyright (C) Konstantin Khait, 2014                                                             */
/****************************************************************************************************/
/* Abstract description for object which can be serialized.											*/
/****************************************************************************************************/

#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H

#include <string>
#include <map>

namespace CST
{

	namespace Common
	{

		namespace LibJSON
		{

			typedef std::map<std::string, std::string> OPTIONS;

			class Serializable
			{
			public:
				Serializable();
				virtual ~Serializable();
			public:
				virtual std::string Serialize(const OPTIONS options = OPTIONS()) = 0;
			};

		}; // namespace LibJSON

	}; // namespace Common

}; // namespace CST

#endif