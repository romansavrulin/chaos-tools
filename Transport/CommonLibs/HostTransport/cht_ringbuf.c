/****************************************************************************************************/
/* Componentality Common Library             * cht_ringbuf.c   										*/
/****************************************************************************************************/
/* Copyright (C) Componentality Oy, 2014                                                            */
/****************************************************************************************************/
/* Ring buffer implementation in C                  												*/
/****************************************************************************************************/
#include "cht_ringbuf.h"
#include <memory.h>

#define min(x, y) ((x < y) ? x : y)

// Create ring buffer in the given memory block
RINGBUF chtRingBufferCreate(char* datablock, int size)
{
	RINGBUF result;
	result.mData = datablock;
	result.mSize = size;
	chtRingBufferReset(&result);
	return result;
}

// Write data to the buffer. Returns number of actually written bytes
int chtRingBufferWrite(RINGBUF* buffer, char* data, int size)
{
	int numbytes = min(buffer->mSize - buffer->mUsed, size);
	int t_portion = buffer->mData + buffer->mSize - buffer->mTail;
	t_portion = min(t_portion, numbytes);
	if (t_portion > 0)
	{
		memcpy(buffer->mTail, data, t_portion);
		buffer->mTail += t_portion;
		buffer->mUsed += t_portion;
	}
	if (numbytes > t_portion)
	{
		memcpy(buffer->mData, data + t_portion, numbytes - t_portion);
		buffer->mTail = buffer->mData + numbytes - t_portion;
		buffer->mUsed += numbytes - t_portion;
	}
	if (buffer->mTail == buffer->mData + buffer->mSize)
		buffer->mTail = buffer->mData;
	return numbytes;
}

// Read data from the buffer. Returns number of actually read bytes
int chtRingBufferRead(RINGBUF* buffer, char* data, int maxsize)
{
	int numbytes = min(buffer->mUsed, maxsize);
	int t_portion = buffer->mData + buffer->mSize - buffer->mHead;
	t_portion = min(t_portion, numbytes);
	if (t_portion > 0)
	{
		memcpy(data, buffer->mHead, t_portion);
		buffer->mHead += t_portion;
		buffer->mUsed -= t_portion;
	}
	if (t_portion < numbytes)
	{
		memcpy(data + t_portion, buffer->mData, numbytes - t_portion);
		buffer->mHead = buffer->mData + numbytes - t_portion;
		buffer->mUsed -= numbytes - t_portion;
	}
	if (buffer->mHead == buffer->mData + buffer->mSize)
		buffer->mHead = buffer->mData;
	return numbytes;
}

// Sets the given ring buffer to the default empty state
void chtRingBufferReset(RINGBUF* buffer)
{
	buffer->mHead = buffer->mData;
	buffer->mTail = buffer->mData;
	buffer->mUsed = 0;
}
