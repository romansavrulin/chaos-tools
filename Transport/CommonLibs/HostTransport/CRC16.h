#ifndef __CRC16_H
#define __CRC16_H

#ifdef __cplusplus
extern "C" {
#endif

#define bool int
#define TRUE 1
#define FALSE 0

//---------------------------------------------------------------------------
// Подсчет CRC и запись в последние 2 байта массива
//---------------------------------------------------------------------------
void vCRC16Create(char* pcData, int iLen);

//---------------------------------------------------------------------------
// Проверка CRC Значение CRC в полседних 2х байтах
//---------------------------------------------------------------------------
bool bCRC16Check(const char* pcData, int iOffs, int iLen);

#ifdef __cplusplus
}
#endif

#endif

