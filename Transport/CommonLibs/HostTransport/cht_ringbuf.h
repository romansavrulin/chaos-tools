/****************************************************************************************************/
/* Componentality Common Library             * cht_ringbuf.h   										*/
/****************************************************************************************************/
/* Copyright (C) Componentality Oy, 2014                                                            */
/****************************************************************************************************/
/* Ring buffer implementation in C                  												*/
/****************************************************************************************************/
#ifndef CHT_RINGBUF_H
#define CHT_RINGBUF_H

// Ring buffer description structure
typedef struct _RINGBUF
{
	char* mData;				// Data buffer
	char* mHead;				// Read pointer
	char* mTail;				// Write pointer
	int   mSize;				// Buffer size
	int   mUsed;				// Number of used bytes
} RINGBUF;

#ifdef __cplusplus
extern "C" {
#endif

// Create ring buffer in the given memory block
extern RINGBUF chtRingBufferCreate(char* datablock, int size);
// Write data to the buffer. Returns number of actually written bytes
extern int     chtRingBufferWrite(RINGBUF* buffer, char* data, int size);
// Read data from the buffer. Returns number of actually read bytes
extern int     chtRingBufferRead(RINGBUF* buffer, char* data, int maxsize);
// Sets the given ring buffer to the default empty state
extern void    chtRingBufferReset(RINGBUF* buffer);

#ifdef __cplusplus
}
#endif

#endif