/****************************************************************************************************/
/* Componentality Common Library             * cht_protocol.c  										*/
/****************************************************************************************************/
/* Copyright (C) Componentality Oy, 2014                                                            */
/****************************************************************************************************/
/* Easy host protocol implementation																*/
/****************************************************************************************************/
#include "cht_protocol.h"
#include "CRC16.h"
#include <memory.h>

// Create linear buffer
LIN_BUFFER chtBufferCreate(char* data, int size)
{
	LIN_BUFFER result;
	result.mData = data;
	result.mSize = size;
	result.mUsed = 0;
	return result;
}

// Store channel data in the linear buffer (1 if success, 0 if failed)
int chtBufferWrite(LIN_BUFFER* buffer, unsigned short channel, char* channel_data, int _datalen)
{
	int overhead = 5;
	unsigned short datalen = (unsigned short)_datalen;
	if (buffer->mSize - buffer->mUsed < datalen + overhead)
		return 0;
	buffer->mData[buffer->mUsed + 0] = (char)((channel >> 0) & (0xFF));
	buffer->mData[buffer->mUsed + 1] = (char)((channel >> 8) & (0xFF));
	buffer->mData[buffer->mUsed + 2] = (char)((datalen >> 0) & (0xFF));
	buffer->mData[buffer->mUsed + 3] = (char)((datalen >> 8) & (0xFF));
	memcpy(buffer->mData + buffer->mUsed + 4, channel_data, datalen);
	buffer->mData[buffer->mUsed + 4 + datalen] = 0x00;
	buffer->mUsed += datalen + overhead;
	return 1;
}

// Restore channels data to the linear buffer (number of read bytes if success, 0 if failed)
int chtBufferRead(LIN_BUFFER* buffer, char* data)
{
	data += 2;													// Skip channel ID
	int len = 0;
	len += (unsigned short) ((unsigned char)*data++);							// Deserialize data length
	len += ((unsigned short)((unsigned char)*data++)) << 8;
	if (buffer)
	{
		if (buffer->mSize < len)
			return 0;
		if (data[len] != 0x00)
			return 0;
		memcpy(buffer->mData, data, len);							// Read data
		buffer->mUsed = len;
	}
	return len + 5;
}

CHANNEL_DESCRIPTOR* __chtFindChannel(CHANNEL_DESCRIPTOR* first_channel, unsigned short id)
{
	while (first_channel != NULL)
	{
		if (first_channel->mChannel == id)
			return first_channel;
		else
			first_channel = first_channel->mNext;
	}
	return NULL;
}

// Store data from the list of channels
int chtSerialize(LIN_BUFFER* buffer, CHANNEL_DESCRIPTOR* channels)
{
	CHANNEL_DESCRIPTOR* current = channels;
	if (buffer->mSize < 1)
		return 0;
	buffer->mData[0] = CHT_START;
	buffer->mUsed = 1;

	while (current != NULL)
	{
		if (current->mData.mUsed > 0)
		{
			if (0 == chtBufferWrite(buffer, current->mChannel, current->mData.mData, current->mData.mUsed))
				return 0;
		};
		current = current->mNext;
	}
	
	if (buffer->mSize - buffer->mUsed < 3)
		return 0;

	vCRC16Create(buffer->mData + 1, buffer->mUsed + 1);
	buffer->mUsed += 3;
	buffer->mData[buffer->mUsed - 1] = CHT_STOP;

	return 1;
}

// Restore data from the list of channels
int chtDeserialize(LIN_BUFFER* buffer, CHANNEL_DESCRIPTOR* channels)
{
	if (buffer->mUsed < 4)
		return 0;
	if (buffer->mData[0] != CHT_START)
		return 0;
	if (buffer->mData[buffer->mUsed - 1] != CHT_STOP)
		return 0;
	int check = bCRC16Check(buffer->mData + 1, 0, buffer->mUsed - 2);
	if (!check)
		return 0;
	int endptr = buffer->mUsed - 3;
	buffer->mUsed = 1;
	while (buffer->mUsed < endptr)
	{
		unsigned short channel = 0;
		channel += (unsigned short)((unsigned char)buffer->mData[buffer->mUsed + 0]);
		channel += ((unsigned short)((unsigned char)buffer->mData[buffer->mUsed + 1])) << 8;
		CHANNEL_DESCRIPTOR* channel_dsc = __chtFindChannel(channels, channel);
		int shift = 0;
		if (channel_dsc)
		{
			channel_dsc->mData.mUsed = 0;
			shift = chtBufferRead(&channel_dsc->mData, buffer->mData + buffer->mUsed);
			if (!shift)
				return 0;
		}
		else
			shift = chtBufferRead(NULL, buffer->mData + buffer->mUsed);
		buffer->mUsed += shift;
	}
	return 1;
}

// Read data from the ring buffer to linear buffer and do interpreting if needed
// Returns 0 upon error, 1 if data read, but not interpreted and 2 if data interpreted
int chtReadData(RINGBUF* host_buf, LIN_BUFFER* packet_buf, CHANNEL_DESCRIPTOR* channels)
{
	int result = 1;
	if (packet_buf->mUsed == 0)
	{
		while (host_buf->mUsed)
		{
			char dummy;
			if (*host_buf->mHead != CHT_START)
				chtRingBufferRead(host_buf, &dummy, 1);
			else
				break;
		}
	}
	while (host_buf->mUsed > 0)
	{
		char data;
		int esc = 0;
		if (packet_buf->mUsed > 0)
		{
			if (packet_buf->mData[packet_buf->mUsed - 1] == CHT_ESC)
			{
				esc = 1;
			}
		}
		if (!chtRingBufferRead(host_buf, &data, 1))
			return 1;
		if (esc)
		{
			packet_buf->mData[packet_buf->mUsed - 1] = data & ~0x80;
			esc = 0;
			if (!chtRingBufferRead(host_buf, &data, 1))
				return 1;
		}
		if (packet_buf->mUsed < packet_buf->mSize - 1)
			packet_buf->mData[packet_buf->mUsed++] = data;
		else
			return 0;
		if (data == CHT_STOP)
		{
			if (!chtDeserialize(packet_buf, channels))
				result = 0;
			else
				result = 2;
			packet_buf->mUsed = 0;
			return result;
		}
	};
	return result;
}

// Write data to the ring buffer from linear buffer after gathering it from channels
// Returns number of bytes transferred
int chtWriteData(RINGBUF* host_buf, LIN_BUFFER* packet_buf, int offset, CHANNEL_DESCRIPTOR* channels)
{
	int i;
	if (offset == 0)
		if (!chtSerialize(packet_buf, channels))
			return 0;
	for (i = offset; i < packet_buf->mUsed; i++)
	{
		char data = packet_buf->mData[i];
		if (((data == CHT_START) && (i != 0)) || ((data == CHT_STOP) && (i != packet_buf->mUsed - 1)) || (data == CHT_ESC))
		{
			char sign = CHT_ESC;
			if (!chtRingBufferWrite(host_buf, &sign, 1))
				return i - offset;
			data |= 0x80;
		}
		if (!chtRingBufferWrite(host_buf, &data, 1))
			return i - offset;
	}
	return packet_buf->mUsed - offset;
}

