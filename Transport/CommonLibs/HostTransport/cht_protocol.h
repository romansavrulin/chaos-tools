/****************************************************************************************************/
/* Componentality Common Library             * cht_protocol.h  										*/
/****************************************************************************************************/
/* Copyright (C) Componentality Oy, 2014                                                            */
/****************************************************************************************************/
/* Easy host protocol implementation																*/
/****************************************************************************************************/
#ifndef CHT_PROTOCOL_H
#define CHT_PROTOCOL_H

#include "cht_ringbuf.h"

#define CHT_START 0x00
#define CHT_STOP  0x01
#define CHT_ESC   0x02

#define CHT_PROTO_VER 0x80

#ifndef NULL
#define NULL 0L
#endif

#ifdef __cplusplus
extern "C" {
#endif

	// Linear buffer
	typedef struct _LIN_BUFFER
	{
		char* mData;
		int   mSize;
		int   mUsed;
	} LIN_BUFFER;

	// Set of channels
	typedef struct _CHANNEL_DESCRIPTOR
	{
		unsigned short mChannel;
		LIN_BUFFER     mData;
		struct _CHANNEL_DESCRIPTOR* mNext;
	} CHANNEL_DESCRIPTOR;

	// Create linear buffer
	extern LIN_BUFFER chtBufferCreate(char* data, int size);
	// Store channel data in the linear buffer (1 if success, 0 if failed)
	extern int chtBufferWrite(LIN_BUFFER* buffer, unsigned short channel, char* channel_data, int datalen);
	// Restore channels data to the linear buffer (number of read bytes if success, 0 if failed)
	extern int chtBufferRead(LIN_BUFFER* buffer, char* data);
	// Store data from the list of channels
	extern int chtSerialize(LIN_BUFFER* buffer, CHANNEL_DESCRIPTOR* channels);
	// Restore data from the list of channels
	extern int chtDeserialize(LIN_BUFFER* buffer, CHANNEL_DESCRIPTOR* channels);

	// Read data from the ring buffer to linear buffer and do interpreting if needed
	// Returns 0 upon error, 1 if data read, but not interpreted and 2 if data interpreted
	extern int chtReadData(RINGBUF* host_buf, LIN_BUFFER* packet_buf, CHANNEL_DESCRIPTOR* channels);
	// Write data to the ring buffer from linear buffer after gathering it from channels
	// Returns number of bytes transferred
	extern int chtWriteData(RINGBUF* host_buf, LIN_BUFFER* packet_buf, int offset, CHANNEL_DESCRIPTOR* channels);

#ifdef __cplusplus
}
#endif

#endif