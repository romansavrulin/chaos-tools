/****************************************************************************************************/
/* Componentality Common Library             * cht_api.h    										*/
/****************************************************************************************************/
/* Copyright (C) Componentality Oy, 2014                                                            */
/****************************************************************************************************/
/* C API for easy host communication protocol       												*/
/****************************************************************************************************/

#ifndef CHT_API_H
#define CHT_API_H

#include "cht_protocol.h"
#include "cht_ringbuf.h"

#ifdef __cplusplus
extern "C" {
#endif

// Channel IDs
#define CHANNEL_ID_PROTOCOL_VERSION			0x0000
#define CHANNEL_ID_ERROR_CODE				0x0001
#define CHANNEL_ID_RESET					0x0002

#define CHANNEL_ID_ADC_00					0x0100
#define CHANNEL_ID_ADC_01					0x0101
#define CHANNEL_ID_ADC_02					0x0102
#define CHANNEL_ID_ADC_03					0x0103
#define CHANNEL_ID_ADC_04					0x0104
#define CHANNEL_ID_ADC_05					0x0105
#define CHANNEL_ID_ADC_06					0x0106
#define CHANNEL_ID_ADC_07					0x0107
#define CHANNEL_ID_ADC_08					0x0108
#define CHANNEL_ID_ADC_09					0x0109
#define CHANNEL_ID_ADC_0A					0x010A
#define CHANNEL_ID_ADC_0B					0x010B
#define CHANNEL_ID_ADC_0C					0x010C
#define CHANNEL_ID_ADC_0D					0x010D
#define CHANNEL_ID_ADC_0E					0x010E
#define CHANNEL_ID_ADC_0F					0x010F

#define CHANNEL_ID_SERIAL_00				0x1000
#define CHANNEL_ID_SERIAL_01				0x1001
#define CHANNEL_ID_SERIAL_02				0x1002
#define CHANNEL_ID_SERIAL_03				0x1003

#define CHANNEL_ID_SERIAL_SETTINGS_00		0x1100
#define CHANNEL_ID_SERIAL_SETTINGS_01		0x1101
#define CHANNEL_ID_SERIAL_SETTINGS_02		0x1102
#define CHANNEL_ID_SERIAL_SETTINGS_02		0x1103

#define CHANNEL_ID_DO						0x0200


// The macro creates a ring buffer with given name and size. Use it for incoming and outgoing buffers
#define CHT_MAKE_RINGBUF(bufname, bufsize)								\
	char    bufname##__data[bufsize];									\
	RINGBUF bufname = chtRingBufferCreate(bufname##__data, bufsize);

// The macro creates a linear buffer (just a piece of memory with handlers). Use it for decoded data storage
#define CHT_MAKE_LINBUF(bufname, bufsize)								\
	char	bufname##__data[bufsize];									\
	LIN_BUFFER bufname = chtBufferCreate(bufname##__data, bufsize);

// Channel definition. Provide channel name, ID and buffer size for the channel.
// Use this macro for static channel definitions outside of function scope
#define CHT_DEF_CHANNEL(channel, id, bufsize)							\
	CHANNEL_DESCRIPTOR channel;											\
	const int channel##__datasize = bufsize;							\
	const unsigned short channel##__id = id;							\
	char channel##__buffer_data[bufsize];

// Channel initialization. Channel definition shall be available for the channel with given name.
// First and last channel must be initialized using separate macros. Please remember that
// all channels are to be linked to the list using 'next' argument
#define CHT_MAKE_CHANNEL(channel, next)											  \
	channel.mData = chtBufferCreate(channel##__buffer_data, channel##__datasize); \
	channel.mChannel = channel##__id;							      			  \
	channel.mNext = &next;

// Macro initializes last channel. Applies linked list terminator.
#define CHT_MAKE_LAST_CHANNEL(channel)											  \
	channel.mData = chtBufferCreate(channel##__buffer_data, channel##__datasize); \
	channel.mChannel = channel##__id;							      			  \
	channel.mNext = 0L;

// Macro initializes first channel. Besides normal channel initialization it also creates
// CHANNELS macro with a pointer to the head of linked list
#define CHT_MAKE_FIRST_CHANNEL(channel, next)							\
	CHT_MAKE_CHANNEL(channel, next);									\
	CHANNEL_DESCRIPTOR* __channels = &channel;

// Linked list of channels. Shall be provided to chtReadData and chtWriteData
#define CHANNELS __channels

/***************************************************************************/
/* Functions to manage a single channel: read and write data to its buffer */
/***************************************************************************/

// Put data to the channel buffer. Returns 1 if success and 0 if failed
extern int chtPutToChannel(CHANNEL_DESCRIPTOR* channel, char* data, int size);
// Get data from channel buffer and empty it. Returns the number of bytes read
extern int chtGetFromChannel(CHANNEL_DESCRIPTOR* channel, char* data, int maxsize);
// Put 16-bit unsigned to channel buffer. Returns 1 if success and 0 if failed
extern int chtPutU16ToChannel(CHANNEL_DESCRIPTOR* channel, unsigned short data);
// Get 16-bit unsigned from channel buffer. Returns 1 if success and 0 if failed
extern int chtGetU16FromChannel(CHANNEL_DESCRIPTOR* channel, unsigned short* data);
// Put serial config information to the channel
extern int chtPutSerialConfigToChannel(CHANNEL_DESCRIPTOR* channel, unsigned short datarate, 
	char bits, char parity, char stopbits);
// Get serial config information from the channel
extern int chtGetSerialConfigFromChannel(CHANNEL_DESCRIPTOR* channel, unsigned short* datarate, 
	char* bits, char* parity, char* stopbits);

/***************************************************************************/
/* Functions to gather data from all channels and send it to/from the host */
/* and get and parse it back. Ring buffer becomes a data source            */
/* Ring buffer maybe small enough, in this case data shall be read or      */
/* write in several iterations                                             */
/***************************************************************************/

// Read data from the ring buffer to linear buffer and do interpreting if needed
// Returns 0 upon error, 1 if data read, but not interpreted and 2 if data interpreted
extern int chtReadData(RINGBUF* host_buf, LIN_BUFFER* packet_buf, CHANNEL_DESCRIPTOR* channels);
// Write data to the ring buffer from linear buffer after gathering it from channels
// Returns number of bytes transferred
extern int chtWriteData(RINGBUF* host_buf, LIN_BUFFER* packet_buf, int offset, CHANNEL_DESCRIPTOR* channels);

#ifdef __cplusplus
}
#endif

#endif