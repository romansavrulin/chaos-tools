/****************************************************************************************************/
/* Componentality Common Library             * cht_api.c    										*/
/****************************************************************************************************/
/* Copyright (C) Componentality Oy, 2014                                                            */
/****************************************************************************************************/
/* C API for easy host communication protocol       												*/
/****************************************************************************************************/
#include "cht_api.h"
#include <memory.h>

// Put data to the channel buffer
int chtPutToChannel(CHANNEL_DESCRIPTOR* channel, char* data, int size)
{
	if (size > channel->mData.mSize)
		return 0;
	memcpy(channel->mData.mData, data, size);
	channel->mData.mUsed = size;
	return 1;
}

// Get data from channel buffer and empty it
int chtGetFromChannel(CHANNEL_DESCRIPTOR* channel, char* data, int maxsize)
{
	int result = channel->mData.mUsed;
	if (channel->mData.mUsed > maxsize)
		return 0;
	memcpy(data, channel->mData.mData, channel->mData.mUsed);
	channel->mData.mUsed = 0;
	return result;
}

// Put 16-bit unsigned to channel buffer
int chtPutU16ToChannel(CHANNEL_DESCRIPTOR* channel, unsigned short data)
{
	char _data[2];
	_data[0] = (unsigned char)data;
	_data[1] = (unsigned char)(data >> 8);
	return chtPutToChannel(channel, _data, 2);
}

// Get 16-bit unsigned from channel buffer
int chtGetU16FromChannel(CHANNEL_DESCRIPTOR* channel, unsigned short* data)
{
	char _data[2];
	if (!chtGetFromChannel(channel, _data, 2))
		return 0;
	*data = (((unsigned short)_data[1]) << 8) | (unsigned char)_data[0];
	return 1;
}

// Put serial config information to the channel
int chtPutSerialConfigToChannel(CHANNEL_DESCRIPTOR* channel, unsigned short datarate,
	char bits, char parity, char stopbits)
{
	char _data[5];
	_data[0] = (unsigned char)datarate;
	_data[1] = (unsigned char)(datarate >> 8);
	_data[2] = bits;
	_data[3] = parity;
	_data[4] = stopbits;
	return chtPutToChannel(channel, _data, 5);
}

// Get serial config information from the channel
int chtGetSerialConfigFromChannel(CHANNEL_DESCRIPTOR* channel, unsigned short* datarate,
	char* bits, char* parity, char* stopbits)
{
	char _data[5];
	if (!chtGetFromChannel(channel, _data, 5))
		return 0;
	*datarate = (((unsigned short)_data[1]) << 8) | (unsigned char)_data[0];
	*bits = _data[2];
	*parity = _data[3];
	*stopbits = _data[4];
	return 1;
}
