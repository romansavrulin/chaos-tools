#ifndef JSYNC_H
#define JSYNC_H

#include "jstreambase.h"
#include <time.h>

namespace CST
{
	namespace Common
	{

		namespace JStream
		{

			class SyncRecord : public CST::Common::LibJSON::Serializable, public CST::Common::LibJSON::Parseable
			{
			protected:
				CST::Common::LibJSON::Object mData;
				CST::Common::LibJSON::Object mTimeStamps;
			public:
				SyncRecord() {};
				SyncRecord(CST::Common::LibJSON::Object&);
				SyncRecord(CST::Common::LibJSON::Object&, const time_t);
				SyncRecord(const std::string str);
				virtual ~SyncRecord() {};
			public:
				virtual CST::Common::LibJSON::Object& getData() { return mData; };
				virtual CST::Common::LibJSON::Object& getTimeStamps() { return mTimeStamps; };
			protected:
				virtual void makeTimeStamps(const time_t);
				virtual void makeTimeStamps(CST::Common::LibJSON::Object&);
			public:
				virtual std::string Serialize(const CST::Common::LibJSON::OPTIONS options = CST::Common::LibJSON::OPTIONS());
				virtual std::pair<size_t, size_t> Parse(const std::string&, const size_t, const size_t);
			};
			
			class Synchronizer
			{
			public:
				Synchronizer();
				virtual ~Synchronizer();
			public:
				// Two ways synchronization
				virtual SyncRecord& sync(SyncRecord&, SyncRecord&);
				// One way synchronization
				virtual void update(SyncRecord& dest, SyncRecord& src);
			};

		} // namespace JStream

	} // namespace Common

} // namespace CST

#endif