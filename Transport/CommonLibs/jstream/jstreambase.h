#ifndef JSTREAMBASE_H
#define JSTREAMBASE_H

#include "../common/common_utilities.h"
#include "../libjson/Json.h"
#include <string>

namespace CST
{

	namespace Common
	{

		namespace JStream
		{

			class Receiver
			{
			protected:
				std::string mBuffer;
				CST::Common::mutex mLock;
			public:
				Receiver() {};
				virtual ~Receiver() {};
			public:
				virtual void onData(const std::string&);
				virtual void onData(const CST::Common::blob);
				virtual CST::Common::LibJSON::Object* getObject();
				virtual void reset();
			};

			class Sender
			{
			protected:
				std::string mBuffer;
				CST::Common::mutex mLock;
			public:
				Sender() {};
				virtual ~Sender() {}
			public:
				virtual Sender& operator<<(CST::Common::LibJSON::Object& obj);
				virtual Sender& operator<<(const std::string& str);
				virtual std::string get();
			};

			class Stream : public Sender, public Receiver
			{
			public:
				Stream();
				virtual ~Stream();
			};

		} // namespace JStream

	} // namespace Common

} // namespace CST

#endif