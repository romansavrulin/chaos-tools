#include "vcard.h"

using namespace CST::Common::JStream;
using namespace CST::Common::LibJSON;
using namespace CST::Common;

VCard::VCard(const std::string type) : mType(type)
{
}


VCard::~VCard()
{
}

std::string VCard::Serialize(const CST::Common::LibJSON::OPTIONS options)
{
	std::string result;
	result += "BEGIN:" + mType + '\n';
	std::vector<std::string> keys = mStorage.GetKeys();
	for (std::vector<std::string>::iterator i = keys.begin(); i != keys.end(); i++)
	{
		std::string key = *i;
		if (key[0] == '\"')
			key = key.substr(1);
		if (key[key.size() - 1] == '\"')
			key = key.substr(0, key.size() - 1);
		result += key + ':' + ((String*)mStorage[*i])->GetValue() + '\n';
	}
	result += "END:" + mType + '\n';
	return result;
}

static std::string parseLine(const std::string& str, size_t& begin, size_t& end)
{
	std::string result;
	size_t i = begin;
	for (; i < end; i++)
		if (str[i] > ' ')
			break;
	for (; i < end; i++)
		if (str[i] == '\n')
			break;
		else
			if (str[i] != '\r')
				result += str[i];
	end = i + 1;
	return result;
}

std::pair<size_t, size_t> VCard::Parse(const std::string& src, const size_t begin, const size_t end)
{
	std::pair<size_t, size_t> begend = std::pair<size_t, size_t>(begin, end);
	String* str = NULL;
	while (begend.first < end)
	{
		begend.second = end;
		std::string line = parseLine(src, begend.first, begend.second);
		begend.first = begend.second;
		size_t pos = line.find(':');
		if (pos == (size_t)-1)
		{
			if (str)
				str->SetValue(str->GetValue() + line);
			continue;
		}
		std::string key = line.substr(0, pos);
		std::string val = line.substr(pos + 1);
		if (key == "BEGIN")
			mType = val;
		else
		{
			if (key == "END")
				return std::pair<size_t, size_t>(begin, begend.second);
			str = new String(val);
			if (key[0] != '\"')
				key = std::string("\"") + key + '\"';
			mStorage.AddObject(key, str);
		}
	}
	return std::pair<size_t, size_t>(begin, begend.second);
}

std::pair<size_t, size_t> VCard::fromJSON(CST::Common::LibJSON::Object& jobj)
{
	std::string str = jobj.Serialize();
	mStorage.Reset();
	std::pair<size_t, size_t> result = mStorage.Parse(str, 0, str.size());
	return result;
}

std::string VCard::operator[](const std::string key)
{
	JsonObject* obj = mStorage[key];
	if (obj)
		if (obj->GetObjectType() == JsonObject::STRING)
			return ((String*)obj)->GetValue();
	return std::string();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

VCard& VCardSet::operator[](const size_t idx)
{
	if (idx > mStorage.size())
	{
		mStorage.resize(idx + 1);
		for (size_t i = mStorage.size(); i <= idx; i++)
			mStorage[i] = new VCard;
	}
	return *mStorage[idx];
}

VCardSet::~VCardSet()
{
	for (size_t i = 0; i < mStorage.size(); i++)
		delete mStorage[i];
}

std::string VCardSet::Serialize(const CST::Common::LibJSON::OPTIONS options)
{
	std::string result;
	for (size_t i = 0; i < mStorage.size(); i++)
		result += mStorage[i]->Serialize() + "\n";
	return result;
}

void VCardSet::reset()
{
	for (std::vector<VCard*>::iterator i = mStorage.begin(); i != mStorage.end(); i++)
		delete *i;
	mStorage.clear();
}

std::pair<size_t, size_t> VCardSet::Parse(const std::string& str, const size_t begin, const size_t end)
{
	reset();
	std::pair<size_t, size_t> result = std::pair<size_t, size_t>(begin, end);
	size_t vcard_begin = result.first;
	do
	{
		VCard& v = *new VCard("");
		result = v.Parse(str, vcard_begin, end);
		vcard_begin = result.second;
		if ((result == Parseable::ERROR_VAL) || (v.getType().empty()))
		{
			delete &v;
		}
		else
		{
			this->mStorage.push_back(&v);
		}
	} while (result.first < result.second);
	result.first = begin;
	return result;
}

std::pair<size_t, size_t> VCardSet::fromJSON(CST::Common::LibJSON::Object& obj)
{
	reset();
	std::vector<std::string> keys = obj.GetKeys();
	for (std::vector<std::string>::iterator i = keys.begin(); i != keys.end(); i++)
	{
		JsonObject* subobj = obj[*i];
		if (subobj && (subobj->GetObjectType() == JsonObject::OBJECT))
		{
			VCard& v = *new VCard;
			if (Parseable::ERROR_VAL != v.fromJSON(*(Object*)subobj))
				this->mStorage.push_back(&v);
			else
				delete &v;
		}
	}
	return std::pair<size_t, size_t>(0, 0);
}

CST::Common::LibJSON::Object& VCardSet::toJSON() 
{
	Object& result = *new Object();

	int id = 1;
	for (std::vector<VCard*>::iterator i = mStorage.begin(); i != mStorage.end(); i++)
	{
		Object& subobj = (*i)->toJSON();
		char buf[24];
		sprintf(buf, "\"vcard%d\"", id);
		result.AddObject(buf, &subobj);
	}
	return result;
};
