#ifndef JFTP_H
#define JFTP_H

#include "../libjson/Parseable.h"
#include "../libjson/Serializable.h"
#include "../libjson/Json.h"

namespace CST
{
	namespace Common
	{
		namespace JStream
		{

			class Item
			{
			protected:
				std::string mUID;
				std::string mName;
				CST::Common::LibJSON::Object* mMetadata;
			public:
				Item(const std::string& uid, CST::Common::LibJSON::Object* metadata = NULL);
				Item(const std::string& uid, const std::string& name, CST::Common::LibJSON::Object* metadata = NULL);
				virtual ~Item();
			};

			class File : public Item
			{

			};

		} // namespace JFTP
	} // namespace Common
} // namespace CST


#endif