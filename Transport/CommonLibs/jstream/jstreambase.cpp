#include "jstreambase.h"

using namespace CST::Common::JStream;
using namespace CST::Common;
using namespace CST::Common::LibJSON;

void Receiver::onData(const std::string& data)
{
	mLock.lock();
	mBuffer += data;
	mLock.unlock();
}

void Receiver::onData(const CST::Common::blob data)
{
	mLock.lock();
	for (size_t i = 0; i < data.mSize; i++)
		mBuffer += data.mData[i];
	mLock.unlock();
}

CST::Common::LibJSON::Object* Receiver::getObject()
{
	mLock.lock();
	size_t start = 0;
	size_t end = mBuffer.size();
	CST::Common::LibJSON::Object* obj = new CST::Common::LibJSON::Object();
	std::pair<size_t, size_t> result = obj->Parse(mBuffer, start, end);
	if (result == CST::Common::LibJSON::JsonObject::ERROR_VAL)
	{
		delete obj;
		obj = NULL;
	}
	else
	{
		mBuffer = mBuffer.substr(result.second, mBuffer.size() - result.second);
	}
	mLock.unlock();
	return obj;
}

void Receiver::reset()
{
	mLock.lock();
	mBuffer.clear();
	mLock.unlock();
}

////////////////////////////////////////////////////////////////////////////////////////////////

Sender& Sender::operator<<(CST::Common::LibJSON::Object& obj) 
{ 
	mLock.lock();
	mBuffer += obj.Serialize(CST::Common::LibJSON::OPTIONS()); 
	mLock.unlock();
	return *this;
}

Sender& Sender::operator<<(const std::string& str)
{
	mLock.lock();
	mBuffer += str;
	mLock.unlock();
	return *this;
}

std::string Sender::get()
{
	mLock.lock();
	std::string result = mBuffer;
	mBuffer.clear();
	mLock.unlock();
	return result;
}

///////////////////////////////////////////////////////////////////////////////////////

Stream::Stream() : Sender(), Receiver()
{
}

Stream::~Stream()
{
}