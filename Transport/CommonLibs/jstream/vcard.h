#ifndef VCARD_H
#define VCARD_H

#include "../libjson/Parseable.h"
#include "../libjson/Serializable.h"
#include "../libjson/Json.h"
#include <vector>

namespace CST
{

	namespace Common
	{

		namespace JStream
		{

			// VCard parser and composer
			class VCard : public CST::Common::LibJSON::Serializable, public CST::Common::LibJSON::Parseable
			{
			protected:
				CST::Common::LibJSON::Object mStorage;
				std::string mType;
			public:
				VCard(const std::string type = "VCARD");
				virtual ~VCard();
				virtual std::string Serialize(const CST::Common::LibJSON::OPTIONS options = CST::Common::LibJSON::OPTIONS());
				virtual std::pair<size_t, size_t> Parse(const std::string&, const size_t, const size_t);
				virtual std::pair<size_t, size_t> fromJSON(CST::Common::LibJSON::Object&);
				virtual CST::Common::LibJSON::Object& toJSON() { return mStorage; };
				virtual std::string getType() const { return mType; }
				virtual std::string operator[](const std::string);
			};

			class VCardSet : public CST::Common::LibJSON::Serializable, public CST::Common::LibJSON::Parseable
			{
			protected:
				std::vector<VCard*> mStorage;
			public:
				VCardSet() {};
				virtual ~VCardSet();
				virtual size_t getSize() const { return mStorage.size(); }
				virtual VCard& operator[](const size_t);
				virtual std::string Serialize(const CST::Common::LibJSON::OPTIONS options = CST::Common::LibJSON::OPTIONS());
				virtual std::pair<size_t, size_t> Parse(const std::string&, const size_t, const size_t);
				virtual std::pair<size_t, size_t> fromJSON(CST::Common::LibJSON::Object&);
				virtual CST::Common::LibJSON::Object& toJSON();
				virtual void reset();
			};

		} // namespace JStream

	} // namespace Common

} // namespace CST

#endif