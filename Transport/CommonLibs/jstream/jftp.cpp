#include "jftp.h"

using namespace CST::Common;
using namespace CST::Common::LibJSON;
using namespace CST::Common::JStream;

Item::Item(const std::string& uid, CST::Common::LibJSON::Object* metadata)
{
	mUID = uid;
	mName = uid;
	mMetadata = metadata;
}

Item::Item(const std::string& uid, const std::string& name, CST::Common::LibJSON::Object* metadata)
{
	mUID = uid;
	mName = name;
	mMetadata = metadata;
}

Item::~Item()
{
}
