#include "jsync.h"

using namespace CST::Common::JStream;
using namespace CST::Common::LibJSON;

const char* __DATA = "\"data\"";
const char* __TIME = "\"time\"";
const char* __TMS  = "\"tms\"";

void SyncRecord::makeTimeStamps(const time_t timestamp)
{
	std::vector<std::string> keys = mData.GetKeys();
	tm* gtime = gmtime(&timestamp);
	time_t utc = mktime(gtime);

	mTimeStamps.Reset();
	for (std::vector<std::string>::iterator i = keys.begin(); i != keys.end(); i++)
	{
		Number&n = *new Number((long long) utc);
		mTimeStamps.AddObject(*i, &n);
	}
}

SyncRecord::SyncRecord(const std::string str)
{
	Parse(str, 0, str.size());
	time_t timeval;
	time(&timeval);
	makeTimeStamps(timeval);
}

SyncRecord::SyncRecord(CST::Common::LibJSON::Object& data)
{
	std::string content = data.Serialize();
	mData.Parse(content, 0, content.size());
	time_t timeval;
	time(&timeval);
	makeTimeStamps(data);
	makeTimeStamps(timeval);
}

SyncRecord::SyncRecord(CST::Common::LibJSON::Object& data, const time_t timeval)
{
	std::string content = data.Serialize();
	mData.Parse(content, 0, content.size());
	makeTimeStamps(timeval);
}

std::string SyncRecord::Serialize(const OPTIONS options)
{
	Object obj;
	obj.AddObject(__DATA, &mData);
	obj.AddObject(__TIME, &mTimeStamps);
	std::string result = obj.Serialize(options);
	obj.RemoveObject(__DATA);
	obj.RemoveObject(__TIME);
	return result;
}

std::pair<size_t, size_t> SyncRecord::Parse(const std::string& src, const size_t begin, const size_t end)
{
	Object obj;
	std::string content;
	std::pair<size_t, size_t> result = obj.Parse(src, begin, end);
	if (result == ERROR_VAL)
		return ERROR_VAL;
	if (obj[__DATA] == NULL)
		content = obj.Serialize();
	else
		content = obj[__DATA]->Serialize();
	mData.Reset();
	mData.Parse(content, 0, content.size());
	mTimeStamps.Reset();
	if (obj[__TIME])
	{
		content = obj[__TIME]->Serialize();
		mTimeStamps.Parse(content, 0, content.size());
	}
	makeTimeStamps(obj);
	return result;
}

void SyncRecord::makeTimeStamps(Object& obj)
{
	time_t tms = 0;
	if (!obj[__TMS])
		return;
	if (obj[__TMS]->GetObjectType() != JsonObject::NUMBER)
	{
		if (obj[__TMS]->GetObjectType() == JsonObject::STRING)
		{
			String* jstr = (String*)obj[__TMS];
			double _tms;
			sscanf(jstr->GetValue().c_str(), "%lf", &_tms);
			tms = (time_t)_tms;
		}
		else
			return;
	}
	else
	{
		Number* jnum = (Number*)obj[__TMS];
		if (jnum->GetType() == Number::FLOAT)
		{
			double _tms = jnum->GetValue().float_value;
			tms = (time_t)_tms;
		}
		else if (jnum->GetType() == Number::INTEGER)
		{
			tms = (time_t)jnum->GetValue().integer_value;
		}
		else
			return;
	}
	std::vector<std::string> keys = mData.GetKeys();
	for (std::vector<std::string>::iterator i = keys.begin(); i != keys.end(); i++)
	{
		if (!mTimeStamps[*i])
		{
			Number* num = new Number((long long)tms);
			mTimeStamps.AddObject(*i, num);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////

Synchronizer::Synchronizer()
{
}

Synchronizer::~Synchronizer()
{
}

SyncRecord& Synchronizer::sync(SyncRecord& x, SyncRecord& y)
{
	SyncRecord& result = *new SyncRecord();
	update(result, x);
	update(result, y);
	return result;
}

static time_t get_timestamp(std::string& id, Object& obj)
{
	if (!obj[id])
		return 0;
	if (obj[id]->GetObjectType() != JsonObject::NUMBER)
		return 0L;
	Number* n = (Number*)obj[id];
	if (n->GetType() != Number::INTEGER)
		return 0L;
	return n->GetValue().integer_value;
}

void Synchronizer::update(SyncRecord& dest, SyncRecord& src)
{
	Object& objdst = dest.getData();
	Object& objsrc = src.getData();
	Object& tmsdst = dest.getTimeStamps();
	Object& tmssrc = src.getTimeStamps();
	std::vector<std::string> srckeys = objsrc.GetKeys();
	for (std::vector<std::string>::iterator i = srckeys.begin(); i != srckeys.end(); i++)
	{
		time_t timesrc = get_timestamp(*i, tmssrc);
		time_t timedst = get_timestamp(*i, tmsdst);
		if (timesrc > timedst)
		{
			std::string newdstline = objsrc[*i]->Serialize();
			JsonObject* newdstobj = JsonObject::ParseObj(newdstline, 0, newdstline.size()).second;
			if (newdstobj)
			{
				JsonObject* oldobj = objdst[*i];
				objdst.RemoveObject(*i);
				objdst.AddObject(*i, newdstobj);
				if (oldobj)
					delete oldobj;
				Number::VALUE val; val.integer_value = timesrc;
				if (tmsdst[*i])
					((Number*)tmsdst[*i])->Set(val, Number::INTEGER);
				else
					tmsdst.AddObject(*i, new Number(val, Number::INTEGER));
			}
		}
	}
}