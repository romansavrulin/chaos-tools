#include "../jstream/jstreambase.h"
#include "../jstream/jsync.h"
#include "../jstream/vcard.h"
#include "../common/common_utilities.h"
#include "../HostTransport/cht_api.h"

using namespace CST::Common;
using namespace CST::Common::LibJSON;
using namespace CST::Common::JStream;

CHT_DEF_CHANNEL(ch0, 1000, 12)
CHT_DEF_CHANNEL(ch1, 1001, 12)
CHT_DEF_CHANNEL(ch2, 1002, 12)

int main(int argc, char* argv[])
{
	char* channel0 = "0 channel";
	char* channel1 = "1 channel";
	char* channel2 = "2 channel";

	CHT_MAKE_FIRST_CHANNEL(ch0, ch1);
	CHT_MAKE_CHANNEL(ch1, ch2);
	CHT_MAKE_LAST_CHANNEL(ch2);

	CHT_MAKE_RINGBUF(ringbuf, 1024);
	CHT_MAKE_LINBUF(output, 1024);
	CHT_MAKE_LINBUF(input, 1024);

	chtPutToChannel(&ch0, channel0, strlen(channel0));
	chtPutSerialConfigToChannel(&ch1, 38400, 8, 0, 1);
	chtPutToChannel(&ch2, channel2, strlen(channel2));

	int res0 = chtWriteData(&ringbuf, &output, 0, CHANNELS);
	int res1 = chtReadData(&ringbuf, &input, CHANNELS);

	char x[100], p, s, b;
	unsigned short val;
	res0 = chtGetFromChannel(&ch0, x, 100);
	chtGetSerialConfigFromChannel(&ch1, &val, &b, &p, &s);
	res0 = chtGetFromChannel(&ch2, x, 100);

	return 0;

	VCardSet vcs;
	std::string vcs_content = blobToString(fileRead("c:\\temp\\kostia.khait.vcf"));
	vcs.Parse(vcs_content, 0, vcs_content.size());
	std::string vcs_x = vcs.Serialize();
	blob vcs_b = stringToBlob(vcs_x);
	fileWrite("c:\\temp\\_.vcf", vcs_b);
	Object& jobjx = vcs.toJSON();
	std::string jobjx_str = jobjx.Serialize();
	std::string fn = vcs[0]["FN"];

	Receiver rcv;
	rcv.onData("{\"a\":\"b\"} {\"c\":\"d\"}");
	Object* obj = NULL;
	do
	{
		obj = rcv.getObject();
		if (obj)
		{
			SyncRecord syr(*obj);
			std::string syrs = syr.Serialize();
		}
	} while (obj);
	SyncRecord syr1("{\"a\":\"b\"}");
	SyncRecord syr2("{\"c\":\"d\"}");
	Synchronizer sync;
	SyncRecord& sr = sync.sync(syr1, syr2);
	sleep(2500);
	SyncRecord syr1a("{\"a\":\"bs\", \"c\":\"ds\"}");
	SyncRecord& sra = sync.sync(sr, syr1a);
	return 1;
}