#include "common_utilities.h"
#include <memory.h>

#ifdef _MSC_BUILD
#include <Windows.h>
#else
#include <pthread.h>
#endif

using namespace CST::Common;

/******************************************************************************************************/

#ifdef WIN32
thread::thread(thread::threadfunc func, void* arg)
{
	HANDLE thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)func, arg, 0, NULL);
	mHandler = (void*)thread;
}

thread::~thread()
{
}

thread::operator void*()
{
	return mHandler;
}

void thread::join()
{
	WaitForSingleObject((HANDLE)mHandler, INFINITE);
}

void CST::Common::sleep(const unsigned int msec)
{
	::Sleep(msec);
}

mutex::mutex()
{
	HANDLE* cs = new HANDLE(CreateMutexA(NULL, FALSE, NULL));
	mHandler = cs;
}

mutex::~mutex()
{
	HANDLE* cs = (HANDLE*)mHandler;
	ReleaseMutex(*cs);
}

void mutex::lock()
{
	HANDLE* cs = (HANDLE*)mHandler;
	WaitForSingleObject(*cs, INFINITE);
}

void mutex::unlock()
{
	HANDLE* cs = (HANDLE*)mHandler;
	ReleaseMutex(*cs);
}

#else

thread::thread(thread::threadfunc func, void* arg)
{
	typedef void*(*pfunc)(void*);
	pthread_t* _thread = new pthread_t;
	pthread_create(_thread, NULL, (pfunc)func, arg);
	mHandler = _thread;
}

thread::~thread()
{
	delete (pthread_t*)mHandler;
}

thread::operator void*()
{
	return mHandler;
}

void thread::join()
{
	pthread_join(*(pthread_t*)mHandler, NULL);
}

void CST::Common::sleep(const unsigned int msec)
{
	usleep(1000 * msec);
}

mutex::mutex()
{
	pthread_mutex_t* lock = new pthread_mutex_t;
	pthread_mutexattr_t   mta;
	pthread_mutexattr_init(&mta);
	pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(lock, &mta);
	mHandler = lock;
}

mutex::~mutex()
{
	pthread_mutex_t* lock = (pthread_mutex_t*)mHandler;
	pthread_mutex_unlock(lock);
	pthread_mutex_destroy(lock);
	delete lock;
}

void mutex::lock()
{
	pthread_mutex_t* lock = (pthread_mutex_t*)mHandler;
	pthread_mutex_lock(lock);
}

void mutex::unlock()
{
	pthread_mutex_t* lock = (pthread_mutex_t*)mHandler;
	pthread_mutex_unlock(lock);
}

#endif

static unsigned long long scount = 0;
static bool scount_running = false;
static mutex scount_lock;
static void __secondscounter(void)
{
	scount_lock.lock();
	scount += 1;
	scount_lock.unlock();
	CST::Common::sleep(1000);
}
unsigned long long CST::Common::getSecondsCounter()
{
	scount_lock.lock();
	if (!scount_running)
		new thread((thread::threadfunc) __secondscounter);
	unsigned long long result = scount;
	scount_lock.unlock();
	return result;
}

/***************************************************************************************/
#include <fstream>
#ifdef WIN32
#include <Windows.h>
#else
#include <dirent.h>
#endif

#ifndef WIN32
#define SLASH '/'
#else
#define SLASH '\\'
#endif

// Read entire file to a [binary] string
blob CST::Common::fileRead(const std::string& name)
{
	File ifile;
	blob result;
	ifile.open(name, File::READ);
	result.mSize = ifile.size();
	if (result.mSize >= 0)
	{
		result.mData = new char[result.mSize];
		ifile.read(result.mData, result.mSize);
	}
	ifile.close();
	return result;
}

// Write entire file from a binary string
bool CST::Common::fileWrite(const std::string& name, const blob& content)
{

	File ofile;
	ofile.open(name, File::WRITE);
	ofile.write(content.mData, content.mSize);
	ofile.close();
	return true;
}

// Check if file exists
bool CST::Common::fileExists(const std::string name)
{
	File ifile;
	ifile.open(name, File::READ);
	bool result = ifile.good() && !ifile.bad();
	ifile.close();
	return result;
}

// Concatenate directory and file paths
std::string CST::Common::fileJoinPaths(const std::string& dir, const std::string& file)
{
	if (dir.size() == 0)
		return file;
	if (file.size() == 0)
		return dir;
	if (dir[dir.size() - 1] == SLASH)
		return dir + file;
	return dir + SLASH + file;
}

std::string CST::Common::fileShortName(const std::string& fname)
{
	std::string shorted_name = fname;
	char slash = getSlashCharacter();
	for (int i = (int)fname.size() - 1; i >= 0; i--)
	{
		if (fname[i] == slash)
		{
			shorted_name = fname.substr((size_t)i + 1);
			break;
		}
	}
	return shorted_name;
}


// List all files in the given directory
std::list<std::string> CST::Common::listFiles(const std::string& dir)
{
#ifdef WIN32
	{
		std::list<std::string> names;
		std::string search_path = fileJoinPaths(dir, "*.*");
		WIN32_FIND_DATAA fd;
		HANDLE hFind = ::FindFirstFileA(search_path.c_str(), &fd);
		if (hFind != INVALID_HANDLE_VALUE)
		{
			do
			{
				if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				{
					names.push_back(fd.cFileName);
				}
			} while (::FindNextFileA(hFind, &fd));
			::FindClose(hFind);
		}
		return names;
	}
#else
	{
	std::list<std::string> names;
	DIR *_dir;
	struct dirent *de;

	_dir = opendir(dir.c_str());
	while (_dir)
	{
		de = readdir(_dir);
		if (!de) break;
		if (de->d_type == 0x08)
			names.push_back(de->d_name);
	}
	closedir(_dir);
	return names;
}
#endif
}

// Encode all pre-space characters
blob CST::Common::escapeCoding(const blob src)
{
	std::string result;
	for (size_t i = 0; i < src.mSize; i++)
	{
		if ((src.mData[i] == 1) || (src.mData[i] == 0))
		{
			char c = src.mData[i];
			result += 1;
			c |= 0xF0;
			result += c;
		}
		else
			result += src.mData[i];
	}
	blob _result;
	_result.mSize = result.size();
	_result.mData = new char[result.size()];
	memcpy(_result.mData, result.c_str(), result.size());
	return _result;
}

// Decode string with pre-space characters encoded
blob CST::Common::escapeDecoding(const blob src)
{
	std::string result;
	for (size_t i = 0; i < src.mSize; i++)
	{
		if ((src.mData[i] == 1) && (i < src.mSize - 1))
		{
			char c = src.mData[i + 1];
			c &= ~(char)0xF0;
			result += c;
			i += 1;
		}
		else
			result += src.mData[i];
	}
	blob _result;
	_result.mSize = result.size();
	_result.mData = new char[result.size()];
	memcpy(_result.mData, result.c_str(), result.size());
	return _result;
}

char CST::Common::getSlashCharacter()
{
	return SLASH;
}

/***********************************************************************************************/

File::File()
{
}

File::~File()
{
	if (mHandler.is_open())
		close();
	filelist[mName] = NULL;
}

void File::open(const std::string name, const File::READWRITE rw)
{
	mName = name;
	mLock.lock();
	while (filelist[mName] != NULL)
	{
		mLock.unlock();
		CST::Common::sleep(500);
		mLock.lock();
	}
	filelist[mName] = &mHandler;
	if (rw == READ)
		mHandler.open(name.c_str(), std::ios::binary | std::ios::in);
	else
		mHandler.open(name.c_str(), std::ios::binary | std::ios::out);
	if (!mHandler.is_open())
	{
		filelist[mName] = NULL;
	}
	mLock.unlock();
}

bool File::good()
{
	return mHandler.is_open() && mHandler.good();
}

bool File::bad()
{
	return !mHandler.is_open() || mHandler.bad();
}

bool File::eof()
{
	return !mHandler.is_open() || mHandler.eof();
}

size_t File::read(const char* buf, size_t size)
{
	if (!mHandler.is_open())
		return 0;
	mHandler.read((char*)buf, size);
	return (size_t)mHandler.gcount();
}

void File::write(const char* buf, size_t size)
{
	if (!mHandler.is_open())
		return;
	mHandler.write((char*)buf, size);
	return;
}

void File::close()
{
	mLock.lock();
	if (mHandler.is_open())
		mHandler.close();
	filelist[mName] = NULL;
	mLock.unlock();
}

size_t File::size()
{
	if (!mHandler.is_open())
		return 0;
	mHandler.seekg(0, std::ios::end);
	size_t result = (size_t)mHandler.tellg();
	mHandler.seekg(0);
	return result;
}

unsigned long long File::lsize()
{
	if (!mHandler.is_open())
		return 0;
	mHandler.seekg(0, std::ios::end);
	unsigned long long result = (unsigned long long)mHandler.tellg();
	mHandler.seekg(0);
	return result;
}

std::map<std::string, std::fstream*> File::filelist;

/*********************************************************************************/

ThreadSet::~ThreadSet()
{
	exit();
	mLock.lock();
	for (std::list<thread*>::iterator i = mThreads.begin(); i != mThreads.end(); i++)
	{
		(*i)->join();
		delete *i;
	}
	mLock.unlock();
}

void ThreadSet::run(thread::threadfunc threadf)
{
	mLock.lock();
	mThreads.push_back(new thread(threadf, this));
	mLock.unlock();
}

void ThreadSet::run(thread::threadfunc threadf, void* data)
{
	mLock.lock();
	mThreads.push_back(new thread(threadf, data));
	mLock.unlock();
}

void ThreadSet::exit()
{
	mLock.lock();
	mExit = true;
	mLock.unlock();
}

bool ThreadSet::getExit()
{
	mLock.lock();
	bool result = mExit;
	mLock.unlock();
	return result;
}

std::string CST::Common::blobToString(const blob b)
{
	std::string result;
	for (size_t i = 0; i < b.mSize; i++)
		result += b.mData[i];
	return result;
}

blob CST::Common::stringToBlob(const std::string& s)
{
	blob result;
	result.mSize = s.size();
	result.mData = (char*)s.c_str();
	return result;
}
