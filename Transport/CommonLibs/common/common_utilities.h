#ifndef COMMON_UTILITIES_H
#define COMMON_UTILITIES_H

#include <string>
#include <map>
#include <fstream>
#include <list>

#ifndef NULL
#define NULL (0L)
#endif

namespace CST
{
	namespace Common
	{

		class thread
		{
		public:
			typedef void(*threadfunc)(...);
		protected:
			void* mHandler;
		public:
			thread(threadfunc, void* = NULL);
			virtual ~thread();
			virtual operator void*();
			virtual void join();
		};

		class mutex
		{
		protected:
			void* mHandler;
		public:
			mutex();
			virtual ~mutex();
			virtual void lock();
			virtual void unlock();
		};

		void sleep(const unsigned int msec);
		unsigned long long getSecondsCounter();

		struct blob
		{
			char* mData;
			size_t mSize;
			blob() { mData = NULL; mSize = 0; }
			virtual ~blob() { }
			virtual void purge() { if (mData) delete[] mData; mData = NULL; mSize = 0; }
		};

		// Read entire file content
		blob fileRead(const std::string& name);
		// Write entire file content
		bool fileWrite(const std::string& name, const blob& content);
		// Check file existence
		bool fileExists(const std::string name);
		// Combine directory and file path
		std::string fileJoinPaths(const std::string& dir, const std::string& file);
		// List all files in the folder
		std::list<std::string> listFiles(const std::string& dir);
		std::string fileShortName(const std::string& fname);
		blob escapeCoding(const blob src);
		blob escapeDecoding(const blob src);
		char getSlashCharacter();
		std::string blobToString(const blob);
		blob stringToBlob(const std::string&);

		class File
		{
		public:
			enum READWRITE
			{
				READ,
				WRITE
			};
		protected:
			static std::map<std::string, std::fstream*> filelist;
		protected:
			std::string mName;
			std::fstream mHandler;
			mutex mLock;
		public:
			File();
			virtual ~File();
			virtual void open(const std::string name, const READWRITE rw);
			virtual bool good();
			virtual bool bad();
			virtual bool eof();
			virtual size_t read(const char*, size_t size);
			virtual void write(const char*, size_t size);
			virtual void close();
			virtual size_t size();
			virtual unsigned long long lsize();
		};

		class ThreadSet
		{
		protected:
			mutex mLock;
			std::list<thread*> mThreads;
			bool mExit;
		public:
			ThreadSet() { mExit = false; };
			virtual ~ThreadSet();
			virtual void run(thread::threadfunc threadf);
			virtual void run(thread::threadfunc threadf, void*);
			virtual void exit();
			virtual bool getExit();
		};

	}
}

#endif