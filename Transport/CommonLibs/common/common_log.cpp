#include "common_log.h"
#include <iostream>
#include <time.h>
#include <stdio.h>

using namespace CST::Common;

Logger::Logger() : mLogStream(std::cout)
{
}

Logger::Logger(std::ostream& stream) : mLogStream(stream)
{
}

Logger::Logger(const std::string fname) : mLogStream(*new std::ofstream(fname.c_str(), std::ios::binary))
{
}

Logger::Logger(const char* fname) : mLogStream(*new  std::ofstream(fname, std::ios::binary))
{
}

Logger::~Logger()
{
	mLogStream.flush();
}

Logger& Logger::operator()(Log& log, std::string data)
{
	time_t _time;
	time(&_time);
	tm* utc = gmtime(&_time);
	char timestamp[240];
	sprintf(timestamp, "%d/%d/%d %d:%d.%d",
		utc->tm_year + 1900,
		utc->tm_mon + 1,
		utc->tm_mday,
		utc->tm_hour,
		utc->tm_min,
		utc->tm_sec);
	mLocker.lock();
	if (mLogNames.find(&log) != mLogNames.end())
	{
		this->mLogStream << timestamp << '\t' << this->mLogNames[&log] << '\t' << data << std::endl;
		this->mLogStream.flush();
	}
	mLocker.unlock();
	return *this;
}

void Logger::AddLog(const std::string logname, Log& log)
{
	mLocker.lock();
	this->mLogNames[&log] = logname;
	mLocker.unlock();
}

void Logger::RemoveLog(Log& log)
{
	mLocker.lock();
	if (mLogNames.find(&log) != mLogNames.end())
		mLogNames.erase(mLogNames.find(&log));
	mLocker.unlock();
}

/***********************************************************************************/

Log::Log(Logger& logger, const std::string name) : mOwner(&logger)
{
	logger.AddLog(name, *this);
}

Log::~Log()
{
	mOwner->RemoveLog(*this);
}

Log& Log::operator<<(const std::string data)
{
	mLocker.lock();
	mBuffer += data;
	mLocker.unlock();

	return *this;
}
	
Log& Log::operator<<(const int val)
{
	char buf[24];
	sprintf(buf, "%d", val);
	mLocker.lock();
	mBuffer += buf;
	mLocker.unlock();

	return *this;
}

Log& Log::operator<<(const float val)
{
	char buf[24];
	sprintf(buf, "%f", val);
	mLocker.lock();
	mBuffer += buf;
	mLocker.unlock();

	return *this;
}

Log& Log::operator<<(const char val)
{
	char buf[24];
	sprintf(buf, "%c", val);
	mLocker.lock();
	mBuffer += buf;
	mLocker.unlock();

	return *this;
}

Log& Log::operator<<(const char* str)
{
	return operator<<(std::string(str));
}

Log& Log::operator<<(StartLogging)
{
	mLocker.lock();
	return *this;
}

Log& Log::operator<<(EndLogging)
{
	mOwner->operator()(*this, mBuffer);
	mBuffer.clear();
	mLocker.unlock();
	return *this;
}

Log& Log::operator<<(const CST::Common::blob blob)
{
    char buf[24];
	mLocker.lock();
    for (size_t i = 0; i < blob.mSize; i++)
    {
		sprintf(buf, "%d", (int) (unsigned char) blob.mData[i]);
		mBuffer += buf;
        mBuffer += ' ';
	}
	mLocker.unlock();
	return *this;
}

StartLogging Log::LOG_BEGIN;
EndLogging Log::LOG_END;
