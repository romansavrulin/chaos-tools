/****************************************************************************************************/
/* RSA Cryptography Components 2008                   * RSACrypt.h                                  */
/****************************************************************************************************/
/* Copyright (C) Konstantin A. Khait, 2008                                                          */
/****************************************************************************************************/
/* RSA cryptography adapter to CST middleware style                                                 */
/****************************************************************************************************/
#ifndef RSACRYPT_H
#define RSACRYPT_H

#include "MessageEngine.h"
#include "StandardComponents.h"

using namespace CSTMiddleWare::MessageEngine;
using namespace CSTMiddleWare::StandardComponents;

namespace Crypto
{

	namespace RSA
	{

		// Pair of secure and public keys
		typedef Joint<DataBlock, DataBlock> KeyPair;

		// Generate RSA secure/public keys pair
		class KeyGen : public Translator<SIGNAL_TYPE, KeyPair>
		{
		public:
			KeyGen() {};
			virtual ~KeyGen() {};
		protected:
			virtual KeyPair operator()(const SIGNAL_TYPE);
		};

		// Encrypt elementary data block (with length shorter than public key)
		// Returns source value if public key not set up!
		class ShortBlockEncrypt : public Function<DataBlock>,					// Convert plain to encrypted
								  public Execute<DataBlock>						// Setup public key
		{
		protected:
			DataBlock public_key;													// Reference to RSA public key
		public:
			ShortBlockEncrypt() {public_key = DataBlock(0);};
			virtual ~ShortBlockEncrypt() {};
		protected:
			virtual void Run(const DataBlock);
			virtual DataBlock operator()(const DataBlock);
		public:
			virtual InputGate<DataBlock>& GetKeyInput() {return Execute<DataBlock>::GetInput();};
			virtual InputGate<DataBlock>& GetDataInput() {return Function<DataBlock>::GetInput();};
		};

		// Decrypt elementary data block (with length shorter than public key)
		// Returns source value if public key not set up!
		class ShortBlockDecrypt : public Function<DataBlock>,					// Convert encrypted to plain
								  public Execute<DataBlock>						// Setup secure key
		{
		protected:
			DataBlock secure_key;												// Reference to RSA secure key
		public:
			ShortBlockDecrypt() {secure_key = DataBlock(0);};
			virtual ~ShortBlockDecrypt() {};
		protected:
			virtual void Run(const DataBlock);
			virtual DataBlock operator()(const DataBlock);
		public:
			virtual InputGate<DataBlock>& GetKeyInput() {return Execute<DataBlock>::GetInput();};
			virtual InputGate<DataBlock>& GetDataInput() {return Function<DataBlock>::GetInput();};
		};

		// Encrypt the long block with RSA
		class Encryptor : public ShortBlockEncrypt
		{
		public:
			Encryptor() {};
			virtual ~Encryptor() {};
		protected:
			virtual DataBlock operator()(const DataBlock);
		};

		// Decrypt the long block with RSA
		class Decryptor : public ShortBlockDecrypt
		{
		public:
			Decryptor() {};
			virtual ~Decryptor() {};
		protected:
			virtual DataBlock operator()(const DataBlock);
		};

		// Sign given data block with secure key (must be converted to string)
		std::string Sign(const std::string data, const std::string secure_key);
		// Validate signature
		bool Verify(const std::string data, const std::string signature, const std::string public_key);
	};

};

#endif