/****************************************************************************************************/
/* CST Middleware 2008                        * BasicDefinitions.h                                  */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Generic interfaces for input/output gates and entities. For inheritance only.                    */
/****************************************************************************************************/
#ifndef BASIC_DEFINITIONS_H
#define BASIC_DEFINITIONS_H

#ifndef NULL
#define NULL 0L
#endif

namespace CSTMiddleWare
{

namespace MessageEngine
{

	template<class Message> class InputGate;
	template<class Message> class OutputGate;
	template<class Message> class Entity;

	/** Abstract interface for classes can't be instantiated out of owner of Entity type          **/
	/** All gates (input and output) must be inherited from this class                            **/
	template<class Message>
	class Owned
	{
	private:
		Entity<Message>& master;						// Owner of this object
	private:
		Owned();										// Can not be created with no owner
	public:
		Owned(Entity<Message>& master);					// Constructing with owner defined
		virtual ~Owned();								// Destructor
		virtual Entity<Message>& GetMaster() const;		// Getting owner
	};

	/** Abstract interface for input gate. Must be overriden in inherited classes **/
	template<class Message>
	class InputGate : public Owned<Message>
	{
	public:
		InputGate(Entity<Message>& master);
		virtual ~InputGate();
		// This method accepts incoming messages
		virtual InputGate& operator()(const Message message) = 0;
	};

	/** Abstract interface for output gate. Must be overriden in inherited classes **/
	template<class Message>
	class OutputGate : public Owned<Message>
	{
	public:
		OutputGate(Entity<Message>& master);
		virtual ~OutputGate();
		virtual OutputGate& operator()(const Message message) = 0;
		virtual OutputGate& Link(InputGate<Message>& input) = 0;
		virtual OutputGate& Unlink(InputGate<Message>& input) = 0;
	};

	/** Abstract interface for entity (object) which typically has one or more gates **/
	template<class Message>
	class Entity
	{
	public:
		Entity();
		virtual ~Entity();
		virtual Entity& GetMessage(InputGate<Message>& source_address, const Message message);
		virtual Entity& SendMessage(OutputGate<Message>& distribution_address, const Message message);
		virtual void Lock() {};
		virtual void Unlock() {};
	};

	/** Signal (no body) message type  **/
	struct SIGNAL_TYPE {bool dummy;};
	/** Signal (no body) message value **/
	const SIGNAL_TYPE SIGNAL = {false};

	/**********************************************************************************************************/
	/************************************** I M P L E M E N T A T I O N ***************************************/
	/**********************************************************************************************************/

	template<class Message>
	InputGate<Message>::InputGate(Entity<Message>& _master) : Owned<Message>(_master)
	{
	};

	template<class Message>
	InputGate<Message>::~InputGate()
	{
	};

	template<class Message>
	Owned<Message>::Owned() : master(*new Entity<Message>)
	{
	};

	template<class Message>
	Entity<Message>& Owned<Message>::GetMaster() const
	{
		return master;
	};

	template<class Message>
	Owned<Message>::Owned(Entity<Message>& _master) : master(_master)
	{
	};

	template<class Message>
	Owned<Message>::~Owned()
	{
	};

	template<class Message>
	OutputGate<Message>::OutputGate(Entity<Message>& _master) : Owned<Message>(_master)
	{
	};

	template<class Message>
	OutputGate<Message>::~OutputGate()
	{
	};

	template<class Message>
	Entity<Message>::Entity()
	{
	};

	template<class Message>
	Entity<Message>::~Entity()
	{
	};

	template<class Message>
	Entity<Message>& Entity<Message>::GetMessage(CSTMiddleWare::MessageEngine::InputGate<Message> &source_address, 
							   const Message message)
	{
		return *this;
	};

	template<class Message>
	Entity<Message>& Entity<Message>::SendMessage(CSTMiddleWare::MessageEngine::OutputGate<Message> &distribution_address, 
								const Message message)
	{
		distribution_address.operator()(message);
		return *this;
	};

};

};

#endif