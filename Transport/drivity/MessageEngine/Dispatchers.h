/****************************************************************************************************/
/* CST Middleware 2008                        * Dispatchers.h										*/
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Representations and auxiliary classes to dispatch messages ranged by priorities.                 */
/****************************************************************************************************/
#ifndef DISPATCHERS_H
#define DISPATCHERS_H

#include "Gates.h"
#include "Entities.h"
#include <list>

namespace CSTMiddleWare
{

namespace MessageEngine
{

	class AbstractUntypedInputInterface
	{
	public:
		virtual void operator()(void* data) = 0;
	};

	class DispatcherInterface
	{
	public:
		struct UntypedMessage
		{
			void*							message_body;
			unsigned int					priority;
			AbstractUntypedInputInterface*	input;
		};
	protected:
		std::list<UntypedMessage> queue;
	public:
		DispatcherInterface() {};
		virtual ~DispatcherInterface() {};
		virtual DispatcherInterface& Dispatch() = 0;
		virtual DispatcherInterface& AddMessage(void* message_body,
												AbstractUntypedInputInterface& receiver,
												unsigned int priority);
	protected:
		virtual UntypedMessage GetMessage() 
			{
				UntypedMessage msg = queue.front(); 
				queue.pop_front(); 
				return msg;
			};
		virtual UntypedMessage PeekMessage()
			{
				UntypedMessage msg = queue.front();
				return msg;
			};
		virtual bool IsEmpty() const 
			{
				unsigned long size = (unsigned long) queue.size();
				return size == 0;
			};
	};

	template<class Message>
	class DispatchingInputGate : public TriggeringInputGate<Message>, public AbstractUntypedInputInterface
	{
	public:
		DispatchingInputGate(Entity<Message>& master);
		virtual ~DispatchingInputGate();
	protected:
		virtual void operator()(void* data);
	};

	template<class Message>
	class DispatchingOutputGate : public DirectOutputGate<Message>
	{
	private:
		unsigned int						priority;
		DispatcherInterface&				dispatcher;
	public:
		DispatchingOutputGate(Entity<Message>& master, 
			DispatcherInterface& dispatcher,
			const unsigned int priority = 0xFFFF);
		virtual ~DispatchingOutputGate();
		virtual unsigned int GetPriority() const {return priority();};
		virtual DispatchingOutputGate<Message>& SetPriority(unsigned int pri);
		virtual OutputGate<Message>& operator()(const Message message);
	};

	class Dispatcher : public Entity<SIGNAL_TYPE>, public Consumer<SIGNAL_TYPE>, public Producer<SIGNAL_TYPE>,
					   public DispatcherInterface
	{
	protected:
		TriggeringInputGate<SIGNAL_TYPE> scheduling_initiator;
		DirectOutputGate<SIGNAL_TYPE>	 message_added_notification;
		DirectOutputGate<SIGNAL_TYPE>	 dispatched_notification;
	public:
		Dispatcher();
		virtual ~Dispatcher();
		virtual DispatcherInterface& AddMessage(void* message_body,
												AbstractUntypedInputInterface& receiver,
												unsigned int priority);
		virtual OutputGate<SIGNAL_TYPE>& GetNotificationGate() {return dispatched_notification;};
	protected:
		virtual Entity<SIGNAL_TYPE>& GetMessage(InputGate<SIGNAL_TYPE>& source_address, const SIGNAL_TYPE message);
		virtual Entity<SIGNAL_TYPE>& SendMessage(OutputGate<SIGNAL_TYPE>& distribution_address, const SIGNAL_TYPE message);
		virtual DispatcherInterface& Dispatch();
	};

	template<class Message>
	class DispatchAdapter : public PassthroughInterface<Message>
	{
	protected:
		TriggeringInputGate<Message>	input;
		DispatchingOutputGate<Message>	output;
	public:
		DispatchAdapter(DispatcherInterface& dispatcher, unsigned int priority = 0xFFFF);
		virtual ~DispatchAdapter();
	};

	template<class Message>
	class UndispatchAdapter : public PassthroughInterface<Message>
	{
	protected:
		DispatchingInputGate<Message>	input;
		DirectOutputGate<Message>		output;
	public:
		UndispatchAdapter();
		virtual ~UndispatchAdapter();
	};

	/****************************************************************************/

	template<class Message>
	DispatchingInputGate<Message>::DispatchingInputGate(Entity<Message>& master)
		: TriggeringInputGate<Message>(master)
	{
	};

	template<class Message>
	DispatchingInputGate<Message>::~DispatchingInputGate()
	{
	};

	template<class Message>
	void DispatchingInputGate<Message>::operator()(void* data)
	{
		Message* typized = reinterpret_cast<Message*>(data);
		Message msg = *typized;
		delete typized;
		TriggeringInputGate<Message>::operator()(msg);
	};

	template<class Message>
	DispatchingOutputGate<Message>::DispatchingOutputGate(Entity<Message>& master, 
			DispatcherInterface& disp,
			const unsigned int pri)
			: DirectOutputGate<Message>(master),
			  dispatcher(disp)
	{
		SetPriority(pri);
	};

	template<class Message>
	DispatchingOutputGate<Message>::~DispatchingOutputGate()
	{
	};

	template<class Message>
	DispatchingOutputGate<Message>& DispatchingOutputGate<Message>::SetPriority(unsigned int pri)
	{
		priority = pri;
		return *this;
	};

	template<class Message>
	OutputGate<Message>& DispatchingOutputGate<Message>::operator()(const Message message)
	{
		Message* typed = new Message(message);
		for (typename std::list<InputGate<Message>*>::iterator i = this->linked_inputs.begin();
			 i != this->linked_inputs.end(); i++)
		{
			if (NULL != dynamic_cast<AbstractUntypedInputInterface*>(*i))
			{
				dispatcher.AddMessage(typed, *dynamic_cast<AbstractUntypedInputInterface*>(*i), GetPriority());
			}
			else
				throw int(0);
		};
		return *this;
	};

	template<class Message>
	DispatchAdapter<Message>::DispatchAdapter(DispatcherInterface& dispatcher, unsigned int priority)
		: input(*this),
		  output(*this, dispatcher, priority),
		  PassthroughInterface<Message>(input, output)
	{
	};

	template<class Message>
	DispatchAdapter<Message>::~DispatchAdapter()
	{
	};

	template<class Message>
	UndispatchAdapter<Message>::UndispatchAdapter()
		: input(*this), output(*this), PassthroughInterface<Message>(input, output)
	{
	};

	template<class Message>
	UndispatchAdapter<Message>::~UndispatchAdapter()
	{
	};

};

};

#endif
