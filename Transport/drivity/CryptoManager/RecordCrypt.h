/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2009-2013 */
/*****************************************************************************/
/* Does encryption and decryption of Manifest + Content records              */
/* shall not be used externally without any real necessity                   */
/*****************************************************************************/

#ifndef RECORDCRYPT_H
#define RECORDCRYPT_H

#include "KeyList.h"
#include "SymCrypt.h"
#include <list>

namespace Componentality
{
	namespace LBS
	{
		// Manifest keys for sender and signature
		extern const std::string ARG_SENDER;
		extern const std::string ARG_SIGNATURE;
	};

	namespace Crypto
	{

		// Attention! This class is not a record, but encryptor/decriptor, i.e. processor!
		class RecordCrypt
		{
		public:
			// Encryption and signature public keys to be provided to remote side
			struct KEYS_TO_REMOTE
			{
				std::string mEncryptionKey;
				std::string mSignatureKey;

				std::string serialize();
				bool deserialize(const std::string& str);
			};
		protected:
			KeyList mKeyList;											// List of public encryption keys for remote side
			std::string mID;											// ID of the local side (device)
			std::map<std::string, std::string> mFingerprints;			// Fingerprint (ID, Encryption key) of remote sides
			std::map<std::string, std::string> mRemoteSignatureKeys;	// (ID, signature key) of remote sides
			std::string mLocalSignatureKey;								// Local signature enctyption key
			std::string mLocalDecryptionKey;							// Local decryption key
			std::string mSymmetricKey;									// Current AES-256 symmetric encryption key
			std::map<std::string, std::string> mKeyCache;				// Cache for symmetric key encrypted with RSA public keys of remote sides
			AES256* mSymmetricCryptor;									// AES-256 symmetric encryptor/decryptor to crypt data content
		public:
			RecordCrypt(const std::string id, const std::string decryption_key = "", const std::string signature_key = "");
			virtual ~RecordCrypt();
			virtual void reset();
			// Encrypt given record and store master key encrypted with keys of all remote sides
			virtual std::string encryptRecord(const std::string decrypted_record);
			// Decrypt record (it automatically extracts keys for given sender if paired)
			virtual std::string decryptRecord(const std::string encrypted_record);
			// Get vehicle ID (cached locally)
			virtual std::string getID() {return mID;};
		public:
			// List all remote sides
			std::list<std::string> listRemoteSides();
			// Pair with remote side
			virtual void addRemoteSide(const std::string& id, const std::string& encryption_key, const std::string& signature_key);
			// Unpair with remote side
			virtual void deleteRemoteSide(const std::string& id);
			// Set keys from remote source (usually upon deserialization)
			virtual void setup(const std::string& decryption_key, const std::string& signature_key);
			// Create key pairs
			virtual KEYS_TO_REMOTE setup();
			// Serialize and deserialize
			virtual std::string serialize();
			virtual bool deserialize(const std::string&);
			// Request keys
			virtual std::string getDecryptionKey() const {return mLocalDecryptionKey;};
			virtual std::string getSignatureKey() const {return mLocalSignatureKey;};
			// Retrieve keys for given side
			virtual KEYS_TO_REMOTE getKeys(const std::string& id);
		protected:
			// Update precached passwords. New master key is generated upon each instantiation or adding/removal of paired side
			virtual void updateCache();
		};

	}; // namespace Crypto

}; // namespace Componentality

#endif