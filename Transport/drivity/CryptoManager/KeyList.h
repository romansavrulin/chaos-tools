/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2009-2013 */
/*****************************************************************************/
/* List of registered public keys. This class is a little bit redundant, but */
/* still widely used for encryption                                          */
/*****************************************************************************/
#ifndef KEYLIST_H
#define KEYLIST_H

#include <string>
#include <map>

namespace Componentality
{
	namespace Crypto
	{

		// List of public keys
		class KeyList
		{
		protected:
			std::map<std::string, std::string> mKeyList;
		public:
			KeyList();
			virtual ~KeyList();
		public:
			// Add public key for a new side
			virtual void addKey(const std::string&);
			// Remove public key for a new side
			virtual void removeKey(const std::string&);
			virtual void clear() {mKeyList.clear();};
			// Save and load list of keys, these functions seem obsolete now
			virtual std::string Serialize();
			virtual void Deserialize(const std::string&);
		public:
			// Encrypt given record with all public keys in the list (obsolete)
			virtual std::map<std::string, std::string> encrypt(const std::string& decrypted_content);
			// Encrypt given record with given public key (doesn't use DRVLOC RSA API)
			virtual std::string encrypt(const std::string& decrypted_content, const std::string& public_key);
			// Decrypt given record (doesn't use DRVLOC RSA API)
			virtual std::string decrypt(const std::string& encrypted_content, const std::string& secure_key);
		};

		struct KEYPAIR
		{
			std::string mEncryptionKey;
			std::string mDecryptionKey;
		};

		KEYPAIR createKeyPair();

	}; // namespace Crypto
}; // namespace Componentality

#endif