/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2009-2013 */
/*****************************************************************************/
/* Symmetric encryption adapter. Very thin and easy                          */
/*****************************************************************************/
#include "SymCrypt.h"
#include "aes256.h"
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <time.h>

using namespace Componentality::Crypto;

AES256::AES256(const std::string key)
{
	if (key.empty())
	{
		renewKey();
	}
	else
		mKey = key;
};

AES256::~AES256()
{
};

void AES256::setKey(const std::string key)
{
	mKey = key;
};

std::string AES256::getKey()
{
	return mKey;
};

void AES256::renewKey()
{
	mKey.clear();
	time_t t;
	time(&t);
	srand(int(t));
	while (mKey.size() < 32)
	{
		mKey += (char) rand();
	};
};

std::string AES256::encrypt(const std::string str)
{
	char len[16];
	sprintf(len, "%ld", str.size());
	std::string to_encrypt = std::string(len) + '\n' + str;
	while (to_encrypt.size() % 16)
		to_encrypt += 'A';

	char* encrypt = new char[to_encrypt.size()];
	memcpy(encrypt, to_encrypt.c_str(), to_encrypt.size());

	aes256_context aes_ctx;
	aes256_init(&aes_ctx, (unsigned char*) mKey.c_str());

	for (long i = 0; i < to_encrypt.size() / 16; i++)
	{
		aes256_encrypt_ecb(&aes_ctx, (unsigned char*) encrypt + i * 16);
	};

	aes256_done(&aes_ctx);

	std::string result;
	for (size_t i = 0; i < to_encrypt.size(); i++)
	{
		result += encrypt[i];
	};

	delete[] encrypt;

	return result;
};

std::string AES256::decrypt(const std::string str)
{
	char* decrypt = new char[str.size()];
	memcpy(decrypt, str.c_str(), str.size());

	aes256_context aes_ctx;
	aes256_init(&aes_ctx, (unsigned char*) mKey.c_str());

	for (long i = 0; i < str.length() / 16; i++)
	{
		aes256_decrypt_ecb(&aes_ctx, (unsigned char*) decrypt + i * 16);
	};

	aes256_done(&aes_ctx);

	std::string result;
	for (size_t i = 0; i < str.size(); i++)
	{
		result += decrypt[i];
	};

	size_t pos = result.find('\n');
	if (pos != (size_t) -1)
	{
		std::string len = result.substr(0, pos);
		std::string content = result.substr(pos + 1);
		unsigned long len_n = atol(len.c_str());
		result = content.substr(0, (size_t) len_n);
	};
	delete[] decrypt;

	return result;
};
