#include "lbsapi.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

#define CRLF '\n'

const std::string COPYRIGHT = std::string() +
	"Drivity Secure Vehicle Tracking Library and Tools" + CRLF +
	"Copyright (C) Componentality Oy, 2013" + CRLF +
	"Version 1.02" + CRLF;

const std::string USAGE_LINE = std::string() +
	"Usage: drivity <command> [<arguments>]" + CRLF + CRLF +
	"Following commands are supported:" + CRLF + 
	"\tinit <vehicle id> - creates and returns key string" + 
		CRLF + "\t\tfor this node" + CRLF +
	"\tpair <remote vehicle id> <remote key file> - " + 
		CRLF + "\t\tregister remote side" + CRLF +
	"\tlist - List all registered sides" + CRLF +
	"\tforget <remote vehicle id> - " + 
		CRLF + "\t\tregister remote side" + CRLF +
	"\ttrack <period> <output_folder> [<input file>] - " +
	    CRLF + "\t\tstart vehicle tracking" + 
		CRLF + "\t\tusing stdin stream or provided file" + 
		CRLF + "\t\tas a source on NMEA 0183" + CRLF +
	"\tparse <file> - do parsing of a record" + 
		CRLF + "\t\twith tracking data" + CRLF +
	"\t+gates <gate list file> - add list of gates" + CRLF +
	"\t-gates {<gate name>} - delete list of gates" + CRLF +
	"\tgates - request list of gates" + CRLF +
	"\tencrypt <input file> <output file> [target] - " + CRLF +
	"\t\tencrypt custom record file" + CRLF +
	"\t\ttarget may specify who can decode it" + CRLF +
	"\tdecrypt <input file> <output file> - " + CRLF +
	"\t\tdecrypt custom record file" + CRLF;

int main(int argc, char** argv)
{
	std::cout << COPYRIGHT;

	if (argc < 2)
	{
		std::cout << USAGE_LINE;
		return 1;
	};

	try {
		std::string command = argv[1];
		Componentality::LBS::LBSAPI api;

		if (command == "init")
		{
			if (argc < 3)
			{
				std::cout << std::endl << "Please provide vehicle identification parameter" << std::endl;
				return 2;
			};
			std::string vid = argv[2];
			std::cout << std::endl << "Service initialized. Key string:" << std::endl << api.init(vid) << std::endl;
		} else
		if (command == "list")
		{
			std::cout << std::endl << "List of paired devices:" << std::endl << api.list() << std::endl;
		} else
		if (command == "pair")
		{
			if (argc < 4)
			{
				std::cout << std::endl << "Please provide vehicle identification and key file parameters" << std::endl;
				return 2;
			};
			std::string vid = argv[2];
			std::string kfile = argv[3];
			std::cout << std::endl << (api.pair(vid, kfile) ? vid + " registered." : std::string("Cannot register ") + vid) << std::endl;
		} else
		if (command == "forget")
		{
			if (argc < 3)
			{
				std::cout << std::endl << "Please provide vehicle identification parameter" << std::endl;
				return 2;
			};
			std::string vid = argv[2];
			std::cout << std::endl << (api.forget(vid) ? vid + " unregistered." : std::string("Cannot unregister ") + vid) << std::endl;
		} else
		if (command == "track")
		{
			if (argc < 4)
			{
				std::cout << std::endl << "Please provide period, output folder and optionally input NMEA file" << std::endl;
				return 2;
			};
			std::string _period = argv[2];
			std::string output_folder = argv[3];
			std::string nmea;
			if (argc == 5)
				nmea = argv[4];
			long period = atol(_period.c_str());
			api.track(period, output_folder, nmea);
		} else
		if (command == "parse")
		{
			if (argc < 3)
			{
				std::cout << std::endl << "Please provide file to parse" << std::endl;
				return 2;
			};
			std::string file = argv[2];
			std::pair<std::string, std::string> result = api.parse(file);
			std::cout << std::endl << "Vehicle information for vehicle " << result.first << ":" << std::endl << result.second << std::endl;
		} else
		if (command == "+gates")
		{
			if (argc < 3)
			{
				std::cout << std::endl << "Please provide gate map file name" << std::endl;
				return 2;
			};
			std::string file = argv[2];
			if (api.addGates(Componentality::LBS::fread(file)))
				std::cout << std::endl << "Gates added successfully" << std::endl;
			else
				std::cout << std::endl << "Gates addition failed" << std::endl;
		} else
		if (command == "gates")
		{
			std::cout << std::endl << "Current gate map:" << std::endl << api.getGates();
		} else
		if (command == "-gates")
		{
			for (int i = 2; i < argc; i++)
			{
				std::string gate = argv[i];
				if (api.removeGate(gate))
					std::cout << std::endl << "Gate " << gate << " removed successfully" << std::endl;
				else
					std::cout << std::endl << "Gate " << gate << " removal failed" << std::endl;
			};
		} else
		if (command == "encrypt")
		{
			if (argc < 4)
			{
				std::cout << std::endl << "Please provide input and output file names" << std::endl;
				return 2;
			};
			std::string infile = argv[2];
			std::string outfile = argv[3];
			std::string content = Componentality::LBS::fread(infile);
			if (argc >= 5)
			{
				std::string target = argv[4];
				content = api.encrypt(content, target);
			}
			else
				content = api.encrypt(content);
			if (content.empty())
			{
				std::cout << "Encryption failed" << std::endl;
				return 3;
			}
			std::ofstream ofile(outfile.c_str(), std::ios::binary);
			ofile.write(content.c_str(), content.size());
			ofile.close();
			std::cout << "Encryption successful" << std::endl;
		} else
		if (command == "decrypt")
		{
			if (argc < 4)
			{
				std::cout << std::endl << "Please provide input and output file names" << std::endl;
				return 2;
			};
			std::string infile = argv[2];
			std::string outfile = argv[3];
			std::string content = Componentality::LBS::fread(infile);
			content = api.decrypt(content);
			if (content.empty())
			{
				std::cout << "Decryption failed" << std::endl;
				return 3;
			}
			std::ofstream ofile;
			ofile.open(outfile.c_str(), std::ios::binary);
			ofile.write(content.c_str(), content.size());
			ofile.close();
			std::cout << "Decryption successful" << std::endl;
		} else
			std::cout << std::endl << "Unknown command. Run drivity with no arguments for help." << std::endl;
	}
	catch (std::string s)
	{
		std::cout << std::endl << s << std::endl;
	}
	catch (...)
	{
		std::cout << std::endl << "Internal issue. Check data formats or contact office@componentality.com for support." << std::endl;
	};

	return 0;
};
