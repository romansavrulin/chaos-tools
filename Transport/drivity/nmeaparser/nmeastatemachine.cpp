/*****************************************************************************/
/* Componentality NMEA parser             * (C) Componentality Oy, 2011-2013 */
/*****************************************************************************/

#include "nmeastatemachine.h"

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <set>
#include <map>
#include <exception>
#include <stdlib.h>

using namespace Componentality;
using namespace Componentality::NMEA;

//////////////////////////////////////////////////////////////////
//   Auxillary functions
//////////////////////////////////////////////////////////////////
static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
{
	std::stringstream ss(s);
	std::string item;
	while(std::getline(ss, item, delim)) 
	{
		elems.push_back(item);
	}
	return elems;
}

//////////////////////////////////////////////////////////////////
//   ParserStateBase implementation
//////////////////////////////////////////////////////////////////
ParserStates ParserStateBase::Process(const char symbol)
{
	switch (symbol)
	{
	case '$': 
	case '*':
	case ',':
	case '\n':
	case '\r':
		return STATE_ERROR;
	default:
		return current;
	};
}

ParserStates ParserStateBase::GetCurrent(){return current;}

//////////////////////////////////////////////////////////////////
//   ParserStateReady implementation
//////////////////////////////////////////////////////////////////
ParserStates ParserStateReady::Process(const char symbol)
{
	if (symbol == '$')
		return STATE_TALKER_IDENTIFICATION;

	return current;
}

std::string ParserStateReady::Report()
{
	return "Ready to scan.";
} 

//////////////////////////////////////////////////////////////////
//   ParserStateError implementation
//////////////////////////////////////////////////////////////////
ParserStates ParserStateError::Enter(Callback* client)
{
	// [TODO]: explain error info. If required.
	stError = "Error occured.";  
	return STATE_READY;
};

ParserStates ParserStateError::Process(const char symbol)
{
	return current;
}

void ParserStateError::Reset(){stError.clear();}

std::string ParserStateError::Report()
{
	return stError;
} 


//////////////////////////////////////////////////////////////////
//   ParserStateTalkerIdentificationCustomVendor implementation
//////////////////////////////////////////////////////////////////
ParserStateTalkerIdentificationCustomVendor::ParserStateTalkerIdentificationCustomVendor()
{
	current=STATE_TALKER_IDENTIFICATION_CUSTOM;
};

std::string ParserStateTalkerIdentificationCustomVendor::Report()
{
	return "Unsupported vendor device.";
};


//////////////////////////////////////////////////////////////////
//   ParserStateTalkerIdentification implementation
//////////////////////////////////////////////////////////////////
ParserStateTalkerIdentification::ParserStateTalkerIdentification(std::map<std::string, std::string>& talkers):Talkers(talkers)
{
	current=STATE_TALKER_IDENTIFICATION;
	talkerID.reserve(MAX_TALKER_LENGTH);
};

ParserStates ParserStateTalkerIdentification::Process(const char symbol)
{
	// ensure that symbols will in one same case (upper)
	char data = (symbol >= 'a' && symbol <= 'z')?(symbol - 'a' + 'A'):symbol;
	
	if (data == 'P' && talkerID.length() == 0)
		return STATE_TALKER_IDENTIFICATION_CUSTOM;

	if (ParserStateBase::Process(data) != current)
		return STATE_ERROR;

	talkerID.push_back(data);

	// magic number 2 is becouse we do not have to support custom manufactures devices. standard talkers all have 2 length
	if (talkerID.length() == 2) 
	{
		if (Talkers.find(talkerID) != Talkers.end())
			return STATE_SENTENCE_IDENTIFICATION;
	}

	return current;
}

void ParserStateTalkerIdentification::Reset()
{
	talkerID.clear();
}

std::string ParserStateTalkerIdentification::Report()
{
	return std::string("talker identify = ") + talkerID;
} 


//////////////////////////////////////////////////////////////////
//   ParserStateSentenceIdentification implementation
//////////////////////////////////////////////////////////////////
ParserStateSentenceIdentification::ParserStateSentenceIdentification(std::map<std::string, std::string>& sentencies):Sentencies(sentencies)
{
	current=STATE_SENTENCE_IDENTIFICATION;
	sentenceID.reserve(MAX_SENTENCE_LENGTH);
};

ParserStates ParserStateSentenceIdentification::Process(const char symbol)
{
	char data = (symbol >= 'a' && symbol <= 'z')?(symbol - 'a' + 'A'):symbol; // ensure that sybols will in one same case (upper)
	
	if (data == ',' && Sentencies.find(sentenceID) != Sentencies.end())
	{
		return STATE_PARAM_IDENTIFICATION;
	}

	if (ParserStateBase::Process(data) != current)
		return STATE_ERROR;

	sentenceID.push_back(data);

	if (sentenceID.length() > MAX_SENTENCE_LENGTH) 
	{
		return STATE_ERROR;
	}

	return current;
}

void ParserStateSentenceIdentification::Reset()
{
	sentenceID.clear();
}

std::string ParserStateSentenceIdentification::Report()
{
	return std::string("sentence identify = ") + sentenceID;
} 

//////////////////////////////////////////////////////////////////
//   ParserStateParamIdentification implementation
//////////////////////////////////////////////////////////////////
ParserStateParamIdentification::ParserStateParamIdentification(std::map<std::string, std::string>& sentencies,
															   ParamParser* paramParser):
																	Sentencies(sentencies),
																	m_paramParser(paramParser)
{
	current=STATE_PARAM_IDENTIFICATION;
	lastParam.reserve(20); // magic ) simple bufer preallocation optimization
	m_client = NULL;
};

ParserStates ParserStateParamIdentification::Process(const char symbol)
{
	if (symbol == ',' || symbol == '*'/*&& lastParam.length() > 0 - some fields can be empty*/)
	{
		// pars concreete param here
		try
		{
			if (m_pCurrentProcessor)
			{
				bool result = m_pCurrentProcessor->ProcessParam(lastParam, m_client);

				if (!result)
					return STATE_ERROR;
				// Ok. Param was processed.
				lastParam = "";

				if (symbol == '*')
					return STATE_READY;

				return current;
			}
			return STATE_ERROR;

		}
		catch(...){return STATE_ERROR;}
		return current;
	}


	// fire error if unexpected system symbol recieved
	if (ParserStateBase::Process(symbol) != current)
		return STATE_ERROR;

	lastParam.push_back(symbol);

	return current;
}

void ParserStateParamIdentification::SetCurrentSentence(const std::string& sentence)
{
	currrentSentence = sentence;
	m_pCurrentProcessor = m_paramParser->SelectMachine(currrentSentence);
}

void ParserStateParamIdentification::Reset()
{
	lastParam.clear();
}

std::string ParserStateParamIdentification::Report()
{
	return std::string("Last parsed param = ") + lastParam;
} 

ParserStates ParserStateParamIdentification::Enter(Callback* client)
{
	m_client = client;
	return current;
}
//////////////////////////////////////////////////////////////////
//   ParserStateMachine implementation
//////////////////////////////////////////////////////////////////
ParserStateMachine::ParserStateMachine(	std::map<std::string, std::string>& talkers, 
										std::map<std::string, std::string>& sentencies,
										ParamParser* paramParser):
	m_ParserStateTalkerIdentification(talkers),
	m_ParserStateSentenceIdentification(sentencies),
	m_ParserStateParamIdentification(sentencies, paramParser)
{
	m_pCurrentState = &m_ParserStateReady;
}

ParserStateMachine::~ParserStateMachine()
{
}

void ParserStateMachine::Process(const char data, Callback* client)
{
	ParserStates newState = m_pCurrentState->Process(data);
	if (newState != m_pCurrentState->GetCurrent())
	{
		m_pCurrentState = TranslateToInstance(newState);

		// process pass-throw state
		ParserStates enteredState = m_pCurrentState->Enter(client);

		if (enteredState != m_pCurrentState->GetCurrent())
		{
			// go throw entered state follow
			m_pCurrentState = TranslateToInstance(enteredState);
		}

		if (m_pCurrentState == &m_ParserStateReady)
		{
			if (client)
				client->onSentenceProcessed();

			// starting new scan cycle, thus, reset the machine
			Reset();
		}
	}
}

ParserStateBase* ParserStateMachine::TranslateToInstance(ParserStates state)
{
	switch (state)
	{
	case STATE_READY:
		return &m_ParserStateReady;
	case STATE_ERROR:
		return &m_ParserStateError;
	case STATE_TALKER_IDENTIFICATION:
		return &m_ParserStateTalkerIdentification;
	case STATE_TALKER_IDENTIFICATION_CUSTOM:
		return &m_ParserStateTalkerIdentificationCustomVendor;
	case STATE_SENTENCE_IDENTIFICATION:
		return &m_ParserStateSentenceIdentification;
	case STATE_PARAM_IDENTIFICATION:
		{
			std::string sentence = m_ParserStateSentenceIdentification.GetSentence();
			m_ParserStateParamIdentification.SetCurrentSentence(sentence);
			return &m_ParserStateParamIdentification;
		}
	case STATE_CHECKSUM_READ:
		// return &;
	case STATE_SENTENCE_PARSED:
		// return &;
		return &m_ParserStateError; // Not implemented yet.
	}

	return &m_ParserStateReady;
}

// resets all states
void ParserStateMachine::Reset()
{
	m_ParserStateSentenceIdentification.Reset();
	m_ParserStateTalkerIdentification.Reset();
	m_ParserStateReady.Reset();
	m_ParserStateError.Reset();
	m_ParserStateTalkerIdentificationCustomVendor.Reset();
	m_ParserStateParamIdentification.Reset();
}

//////////////////////////////////////////////////////////////////
//   state machines
//////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////
//   FieldsParserMachine implementation
//////////////////////////////////////////////////////////////////
FieldsParserMachine::FieldsParserMachine(const std::string& stTemplate)
{
	// Calls once, so, can use not too fast implemetation
	m_bError = false;
	m_currentCmd = 0;

	std::vector<std::string> splittedParams;
	
	split(stTemplate, ',', splittedParams);

	for (size_t i = 0; i  < splittedParams.size(); ++i)
	{
		ConcreeteFieldParser* pNewParser = NULL;

		if (splittedParams[i] == "(%" || splittedParams[i] == "(")
		{
			std::string originalTag = splittedParams[i];
			std::string stSubTemplate;
			stSubTemplate.reserve(stTemplate.length());
			i++; // skip "(%" or "("
			while (splittedParams[i] != ")")
			{
				if (stSubTemplate.length() > 0)
					stSubTemplate += ",";
				stSubTemplate += splittedParams[i++];
			}
			i++; // skip ")"

			pNewParser = GenerateConcreeteFieldParser(originalTag, stSubTemplate);
		}
		else
		{
			pNewParser = GenerateConcreeteFieldParser(splittedParams[i]);
		}

		if (pNewParser)
		{
			m_ParsersStack.push_back(pNewParser);
		}
		else
			throw "Bad format syntax. Unknow template token.";
	}	
}

FieldsParserMachine::~FieldsParserMachine()
{
	for (size_t i = 0; i < m_ParsersStack.size(); ++i)
	{
		delete m_ParsersStack[i];
	}

	m_ParsersStack.clear();
}

bool FieldsParserMachine::ProcessParam(const std::string& param, Callback* client)
{
	if (m_currentCmd >= m_ParsersStack.size())
		return false;

	int iNextCmdOffset = m_ParsersStack[m_currentCmd]->Process(param);
	if ( m_ParsersStack[m_currentCmd]->WasError())
	{
		m_bError = true;
		return false;
	}
	//m_ParsersStack[m_currentCmd]->Report();

	m_currentCmd += iNextCmdOffset;


	return true;
}

void FieldsParserMachine::Reset()
{
	m_bError = false;
	m_currentCmd = 0;
}

// Recommended minimum specific GPS/TRANSIT data
bool RMC_Processor::ProcessParam(const std::string& param, Callback* client)
{
	int iCmd = m_currentCmd;

	bool result = FieldsParserMachine::ProcessParam(param, client);

	if (!result)
		return result;

	if (client)
	{
		double value;
		char direction;
		if (iCmd == 1) // time
		{
			Time gmt;
			m_ParsersStack[0]->WriteData(&gmt);
			client->setTime(gmt);

		}
		if (iCmd == 6) // all position data parsed ()
		{
			Coordinates coords;
			m_ParsersStack[4]->WriteData(&value);
			m_ParsersStack[5]->WriteData(&direction);
			coords.mLongitude = (direction=='W')? -value:value;

			m_ParsersStack[2]->WriteData(&value);
			m_ParsersStack[3]->WriteData(&direction);
			coords.mLatitude = (direction=='S')? -value:value;;

			coords.mAltitude = 0.f;

			client->setPosition(coords);
		}
 
		if (iCmd == 6)
		{
			double fspeed = 0.0;
			m_ParsersStack[iCmd]->WriteData(&fspeed);
			client->setSpeed(fspeed * 1.852);
		}

		// Sentencies["RMC"] = "hhmmss.ss,A,llll.ll,a,yyyyy.yy,a,x.x,x.x,xxxx,x.x,a";
	}

	return result;
}

// GPS DOP and active satellites
bool GSA_Processor::ProcessParam(const std::string& param, Callback* client)
{
	int iCmd = m_currentCmd;

	bool result = FieldsParserMachine::ProcessParam(param, client);

	if (!result)
		return result;

	if (client)
	{
		double value = 0;

		if (iCmd == 2) // FIX
		{
			char fixchar;
			m_ParsersStack[1]->WriteData(&fixchar);

			if (fixchar == '1')
				client->setFix(FIX_NO);
			if (fixchar == '2')
				client->setFix(FIX_2D);
			if (fixchar == '3')
				client->setFix(FIX_3D);
		}

		if (iCmd == 16) // 14 - PDOP, 15-HDOP, 16-VDOP
		{
			Precision prec;
			m_ParsersStack[14]->WriteData(&value);
			prec.mPDOP = value;
			m_ParsersStack[15]->WriteData(&value);
			prec.mHDOP = value;
			m_ParsersStack[16]->WriteData(&value);
			prec.mVDOP = value;

			client->setPrecision(prec);
		}

		// "a,a,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x.x,x.x,x.x";
	}

	return result;
}

// GPS Satellites in view
bool GSV_Processor::ProcessParam(const std::string& param, Callback* client)
{
	int iCmd = m_currentCmd;

	bool result = FieldsParserMachine::ProcessParam(param, client);

	if (!result)
		return result;

	if (client)
	{
		int value[4];

		if (iCmd == 2) // Sattelites in view
		{
			if (m_ParsersStack[iCmd]->WriteData(&value[0]))
			{
				client->setSattelitesInView(value[0]);
			}
		}

		if (iCmd == 3) // subset of 4x group here
		{
			if (m_ParsersStack[iCmd]->WriteData(&value))
			{
				Satellite sat;
				sat.mAzimuth = value[2];
				sat.mID = value[0];
				sat.mSignalStrength = value[3];
				sat.mElevation = value[1];

				client->updateSatelliteInfo(sat);
			}
		}
		// x,x,x,(,x,x,x,x,);
	}

	return result;
}

// Track Made Good and Ground Speed
bool VTG_Processor::ProcessParam(const std::string& param, Callback* client)
{
	int iCmd = m_currentCmd;

	bool result = FieldsParserMachine::ProcessParam(param, client);

	if (!result)
		return result;

	if (client)
	{
		double value;

		if (iCmd == 1) 
		{
			m_ParsersStack[0]->WriteData(&value);

			client->setHeading(value);
		}

		if (iCmd == 5) 
		{
			m_ParsersStack[4]->WriteData(&value);

			client->setSpeed(value * 1.852);
		}
		// x.x,T,x.x,M,x.x,N,x.x,K;
	}

	return result;
}

//Geographic Position, Latitude / Longitude and time.
bool GLL_Processor::ProcessParam(const std::string& param, Callback* client)
{
	int iCmd = m_currentCmd;

	bool result = FieldsParserMachine::ProcessParam(param, client);

	if (!result)
		return result;

	if (client)
	{
		double value;
		char direction;

		if (iCmd == 4) 
		{
			Coordinates coords;	
			m_ParsersStack[2]->WriteData(&value);
			m_ParsersStack[3]->WriteData(&direction);
			coords.mLongitude = (direction=='W')? -value:value;

			m_ParsersStack[0]->WriteData(&value);
			m_ParsersStack[1]->WriteData(&direction);
			coords.mLatitude = (direction=='S')? -value:value;;

			coords.mAltitude = 0.f;

			client->setPosition(coords);
		}

		// llll.ll,a,yyyyy.yy,a,hhmmss.ss,A;
	}

	return result;
}

// Global Positioning System Fix Data
bool GGA_Processor::ProcessParam(const std::string& param, Callback* client)
{
	int iCmd = m_currentCmd;

	bool result = FieldsParserMachine::ProcessParam(param, client);

	if (!result)
		return result;

	if (client)
	{
		double value;
		char direction;

		if (iCmd == 9) 
		{
			Coordinates coords;	
			m_ParsersStack[1]->WriteData(&value);
			m_ParsersStack[2]->WriteData(&direction);
			coords.mLatitude = (direction=='W')? -value:value;

			m_ParsersStack[3]->WriteData(&value);
			m_ParsersStack[4]->WriteData(&direction);
			coords.mLongitude = (direction=='S')? -value:value;;

			m_ParsersStack[8]->WriteData(&value);
			coords.mAltitude = value;

			client->setPosition(coords);
		}
		if (iCmd == 6) // FIX
		{
			int fixchar;
			m_ParsersStack[5]->WriteData(&fixchar);

			if (fixchar == 0)
				client->setFix(FIX_NO);
		}
		if (iCmd == 7) // Sattelites in view
		{
			int iCount;
			if (m_ParsersStack[6]->WriteData(&iCount))
			{
				client->setSattelitesInView(iCount);
			}
		}


		// llll.ll,a,yyyyy.yy,a,hhmmss.ss,A;
	}

	return result;
}

FieldsParserMachine* GenerateParamSpecificFiledParserMachine(const std::string& stSentence, const std::string& stTemplate)
{
	if (stSentence == "RMC")
		return new RMC_Processor(stTemplate);
	if (stSentence == "GSA")
		return new GSA_Processor(stTemplate);
	if (stSentence == "GSV")
		return new GSV_Processor(stTemplate);
	if (stSentence == "VTG")
		return new VTG_Processor(stTemplate);
	if (stSentence == "GLL")
		return new GLL_Processor(stTemplate);
	if (stSentence == "GGA")
		return new GGA_Processor(stTemplate);

	return new FieldsParserMachine(stTemplate);
}

//////////////////////////////////////////////////////////////////
//   ParamParser implementation
//////////////////////////////////////////////////////////////////
ParamParser::~ParamParser()
{
	for (fieldsParsersIterator iter = m_parsers.begin(); iter != m_parsers.end(); ++iter)
	{
		delete iter->second;
	}

	m_parsers.clear();
}

void ParamParser::Construct(std::map<std::string, std::string>& sentencies)
{
	for (std::map<std::string, std::string>::iterator iter = sentencies.begin(); iter != sentencies.end(); ++iter)
	{
		m_parsers[iter->first] = GenerateParamSpecificFiledParserMachine(iter->first, iter->second); // new FieldsParserMachine(iter->second);
	}
}

FieldsParserMachine* ParamParser::SelectMachine(const std::string& sentence)
{
	FieldsParserMachine* machine = m_parsers[sentence];
	machine->Reset();
	return machine;
}



