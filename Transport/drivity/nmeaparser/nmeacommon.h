/*****************************************************************************/
/* Componentality NMEA parser             * (C) Componentality Oy, 2011-2013 */
/*****************************************************************************/
/* Common types and definitions                                              */
/*****************************************************************************/

#ifndef NMEA_API_COMMON
#define NMEA_API_COMMON

#include <list>
#include <string>

namespace Componentality
{
	namespace NMEA
	{

#pragma pack(push, 1)

		// This class incaplusates knowlegies about errors.
		enum Error
		{
			ERROR_NONE,
			ERROR_FORMAT
		};

		// This class incaplusates buffer with data.
		struct Blob
		{
			void* mData;
			unsigned long mLength;
		};

		// This class incaplusates geo coordinates - lat, lon, alt.
		struct Coordinates
		{
			// Did you forget E/W and N/S directions?
			double mLatitude;  // What properly coordinate system is used?
			double mLongitude;
			double mAltitude;
		};

		// This class incaplusates 
		struct Precision
		{
			double mHDOP;
			double mVDOP;
			double mPDOP; // I've added it.
		};

		enum Fix
		{
			FIX_NO,
			FIX_2D,
			FIX_3D
		};

		// This class incaplusates information about concreete satellite.
		struct Satellite
		{
			unsigned int mID;
			double		 mSignalStrength; // should be integer value [0-99]
			double		 mAzimuth;		  // should be integer value [0-359] 	
			double       mElevation;      // should be integer value [0,90]
		};
		
		struct Time
		{
			int m_hh;
			int m_mm;
			int m_ss;
			int m_mss;
		};

		class Callback 
		{
		public:
			// Setters (used in paraser side to SET values to be getted methods above)
			virtual void setPosition(const Coordinates& position) = 0;
			virtual void setPrecision(const Precision& precision) = 0;
			virtual void setFix(Fix fix) = 0;
			virtual void setHeading(double heading) = 0;
			virtual void setSpeed(double speed) = 0;
			virtual void setSatelliteInfo(const std::list<Satellite>& satellites) = 0;
			virtual void setSattelitesInView(size_t count) = 0;
			virtual void setTime(const Time& gmt) = 0;
			virtual void updateSatelliteInfo(const Satellite& info) = 0;

			// Getters (used in client side to GET values)
			virtual Coordinates getPosition() const = 0;
			virtual Precision   getPrecision() const = 0;
			virtual Fix			getFix() const = 0;
			virtual double		getHeading() const = 0; // what is it? is is course?
			virtual double		getSpeed() const = 0;
			virtual size_t		getSattelitesInView() const = 0;
			virtual Time		getTime() const = 0;
			virtual std::list<Satellite> getSatelliteInfo() const = 0;
		public:
			// event
			virtual void onBlobProcessed(Blob&) = 0;
			virtual void onSentenceProcessed() = 0;
		};

#pragma pack(pop)

	}; // namespace NMEA

}; // namespace Componentality

#endif // NMEA_API_COMMON
