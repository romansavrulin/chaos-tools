/****************************************************************************************************/
/* CST Middleware 2008                        * Files.h                                             */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Standard components to represent file management operations.                                     */
/****************************************************************************************************/
#ifndef FILES_H
#define FILES_H

#include "MessageEngine.h"
#include "Memory.h"
#include "strings.h"
#include <string>

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;
using namespace CSTMiddleWare::StandardComponents;

namespace CSTMiddleWare
{

namespace StandardComponents
{

	// Class to write data to the given file
	class FileWriter : public Translator< Joint<std::string, DataBlock>, bool >,
					   public Execute<int>
	{
	protected:
		int position;							// Seek position
	public:
		FileWriter();
		virtual ~FileWriter();
	public:
		// Shortcuts to gates
		virtual InputGate< Joint<std::string, DataBlock> >& GetDataInput()
		{
			return Translator<Joint<std::string, DataBlock>, bool>::GetInput();
		};
		virtual OutputGate<bool>& GetResultOutput()
		{
			return Translator<Joint<std::string, DataBlock>, bool>::GetOutput();
		};
		virtual InputGate<int>& GetSeekInput()
		{
			return Execute<int>::GetInput();
		};
	protected:
		virtual bool operator()(const Joint<std::string, DataBlock> source_info);
		// Seek to the new position
		virtual void Run(const int _new_pos) {position = _new_pos;};
	protected:
		// Thread safety
		virtual void Lock()
		{
			Entity< Joint<std::string, DataBlock> >::Lock();
			Entity<bool>::Lock();
			Entity<int>::Lock();
		};
		virtual void Unlock()
		{
			Entity< Joint<std::string, DataBlock> >::Unlock();
			Entity<bool>::Unlock();
			Entity<int>::Unlock();
		};
	};

	// Class to read data from file
	// If length < 0, reading all data till the EOF
	class FileReader : public Translator< Joint<std::string, int>, DataBlock >,
					   public Execute<std::string>,
					   public Execute<int>
	{
	protected:
		int position;						// Seek position
	public:
		FileReader();
		virtual ~FileReader();
	protected:
		virtual DataBlock operator()(const Joint<std::string, int> source_info);
		virtual void Run(const int _new_pos) {position = _new_pos;};
		virtual void Run(const std::string fname) 
			{Joint<std::string, int> temp; temp.x = fname; temp.y = -1; GetDataInput().operator()(temp);};
	public:
		// Shortcuts to gates
		virtual InputGate< Joint<std::string, int> >& GetDataInput()
		{
			return Translator< Joint<std::string, int>, DataBlock >::GetInput();
		};
		virtual InputGate<std::string>& GetEntireInput()
		{
			return Execute<std::string>::GetInput();
		};
		virtual OutputGate<DataBlock>& GetResultOutput()
		{
			return Translator< Joint<std::string, int>, DataBlock >::GetOutput();
		};
		virtual InputGate<int>& GetSeekInput()
		{
			return Execute<int>::GetInput();
		};
	protected:
		// Thread safety
		virtual void Lock()
		{
			Entity< Joint<std::string, int> >::Lock();
			Entity<DataBlock>::Lock();
			Entity<int>::Lock();
		};
		virtual void Unlock()
		{
			Entity< Joint<std::string, int> >::Unlock();
			Entity<DataBlock>::Unlock();
			Entity<int>::Unlock();
		};
	};

	// Class to check if the given file exists
	class FileExistenceCheck : public Translator<std::string, bool>
	{
	public:
		FileExistenceCheck();
		virtual ~FileExistenceCheck();
	protected:
		virtual bool operator()(const std::string fname);
	};

};
	
};

#endif
