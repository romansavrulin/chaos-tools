/****************************************************************************************************/
/* CST Middleware 2008                        * Numerical.h                                         */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Standard components to represent arithmetic operations.                                          */
/****************************************************************************************************/
#include "Numerical.h"

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;
using namespace CSTMiddleWare::StandardComponents;

// This symbol just to have the .lib file not empty
int ____DUMMY_NUMERICAL = 0;