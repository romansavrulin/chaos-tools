/****************************************************************************************************/
/* Sockets & HTTP Adapter 2008            * Base64.cpp                                              */
/****************************************************************************************************/
/* Copyright (C) Componentality Oy, 2008                                                            */
/****************************************************************************************************/
/* Support MIME/Base64 encoding/decoding functions.                                                 */
/* Reused from http://base64.sourceforge.net/b64.c                                                  */
/****************************************************************************************************/

#include "Base64.h"

using namespace Componentality::Simplicity::Configuration;

/*
** Translation Table as described in RFC1113
*/
static const char cb64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/*
** Translation Table to decode (created by author)
*/
static const char cd64[]="|$$$}rstuvwxyz{$$$$$$$>?@ABCDEFGHIJKLMNOPQRSTUVW$$$$$$XYZ[\\]^_`abcdefghijklmnopq";



/*
** encodeblock
**
** encode 3 8-bit binary bytes as 4 '6-bit' characters
*/
static void encodeblock( unsigned char in[3], unsigned char out[4], int len )
{
	out[0] = cb64[ in[0] >> 2 ];
	out[1] = cb64[ ((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4) ];
	out[2] = (unsigned char) (len > 1 ? cb64[ ((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6) ] : '=');
	out[3] = (unsigned char) (len > 2 ? cb64[ in[2] & 0x3f ] : '=');
}

/*
** encode
**
** base64 encode a stream adding padding and line breaks as per spec.
*/
std::string Componentality::Simplicity::Configuration::BASE64_encode( std::string infile )
{
	std::string outfile;
	unsigned char in[3], out[4];
	int i, len = 0;

	while( !infile.empty() ) {
		len = 0;
		for( i = 0; i < 3; i++ ) 
		{
			if( infile.empty() ) {
				in[i] = 0;
			}
			else {
				in[i] = (unsigned char) infile[0];
				infile.erase(0, 1);
				len++;
			}
		}
		if( len ) 
		{
			encodeblock( in, out, len );
			for( i = 0; i < 4; i++ ) {
				outfile += out[i];
			}
		}
	}
	return outfile + '$';
}

/*
** decodeblock
**
** decode 4 '6-bit' characters into 3 8-bit binary bytes
*/
static void decodeblock( unsigned char in[4], unsigned char out[3] )
{   
	out[ 0 ] = (unsigned char ) (in[0] << 2 | in[1] >> 4);
	out[ 1 ] = (unsigned char ) (in[1] << 4 | in[2] >> 2);
	out[ 2 ] = (unsigned char ) (((in[2] << 6) & 0xc0) | in[3]);
}

/*
** decode
**
** decode a base64 encoded stream discarding padding, line breaks and noise
*/
std::string Componentality::Simplicity::Configuration::BASE64_decode( std::string infile )
{
	std::string outfile;
	unsigned char in[4], out[3], v;
	int i, len;

	while( !infile.empty() ) 
	{
		for( len = 0, i = 0; i < 4 && !infile.empty(); i++ ) {
			v = 0;
			while( !infile.empty() && v == 0 ) {
				v = (unsigned char) infile[0];
				infile.erase(0, 1);
				v = (unsigned char) ((v < 43 || v > 122) ? 0 : cd64[ v - 43 ]);
				if( v ) {
					v = (unsigned char) ((v == '$') ? 0 : v - 61);
				}
			}
			if( !infile.empty() ) 
			{
				len++;
				if( v ) {
					in[ i ] = (unsigned char) (v - 1);
				}
			}
			else {
				in[i] = 0;
			}
		}
		if( len ) {
			decodeblock( in, out );
			for( i = 0; i < len - 1; i++ ) {
				outfile += out[i];
			}
		}
	}
	return outfile;
}

