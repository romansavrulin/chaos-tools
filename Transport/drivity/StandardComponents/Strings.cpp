/****************************************************************************************************/
/* CST Middleware 2008                        * Strings.cpp                                         */
/****************************************************************************************************/
/* Copyright (C) Componentality, Konstantin A. Khait, 2008                                          */
/****************************************************************************************************/
/* Strings management components.                                                                   */
/****************************************************************************************************/
#include "Strings.h"
#include <string>

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;
using namespace CSTMiddleWare::StandardComponents;

//// String concatenator

StringConcatenator::StringConcatenator()
{
};

StringConcatenator::~StringConcatenator()
{
};

std::string StringConcatenator::operator()(const Joint<std::string, std::string> message)
{
	return message.x + message.y;
};

//// String splitter

StringSplitter::StringSplitter()
{
};

StringSplitter::~StringSplitter()
{
};

Joint<std::string, std::string> StringSplitter::operator ()(const Joint<std::string, int> message)
{
	Joint<std::string, std::string> result;
	
	result.x = message.x.substr(0, message.y);
	result.y = message.x.substr(message.y);

	return result;
};

//// String finder

StringFinder::StringFinder()
{
};

StringFinder::~StringFinder()
{
};

int StringFinder::operator ()(const Joint<std::string, std::string> message)
{
	return static_cast<int>(message.y.find(message.x));
};

//// String cutter
StringCutter::StringCutter()
{
};

StringCutter::~StringCutter()
{
};

std::string StringCutter::operator()(const Joint< std::string, Joint<int, int> > message)
{
	register size_t position = message.y.x;
	register size_t length   = message.y.y;
	std::string source = message.x;

	return source.erase(position, length);
};

//// StringSubExtractor - substring extraction component

StringSubExtractor::StringSubExtractor()
{
};

StringSubExtractor::~StringSubExtractor()
{
};

std::string StringSubExtractor::operator()(const Joint< std::string, Joint<int, int> > message)
{
	register size_t position = message.y.x;
	register size_t length   = message.y.y;
	std::string source = message.x;

	return source.substr(position, length);
};

// StringTrimmer

StringTrimmer::StringTrimmer(const TrimmingType _type, const std::string _char_list)
	: type(_type), char_list(_char_list)
{
};

StringTrimmer::~StringTrimmer()
{
};

std::string StringTrimmer::operator ()(const std::string message)
{
	std::string result = message;
	if ((type == LEFT_ONLY) || (type == LEFT_AND_RIGHT))
	{
		int i;
		for (i = 0; i < static_cast<int>(result.length()); i++)
		{
			if (IsAllowed(result[i]))
				break;
		};
		result.erase(0, i);
	};
	if ((type == RIGHT_ONLY) || (type == LEFT_AND_RIGHT))
	{
		int i;
		for (i = static_cast<int>(result.length()) - 1; i >= 0; i--)
		{
			if (IsAllowed(result[i]))
				break;
		};
		result.erase(i + 1);
	};
	return result;
};

bool StringTrimmer::IsAllowed(const char c)
{
	if (char_list == "")
		return static_cast<unsigned char>(c) > ' ';
	else
		return static_cast<int>(char_list.find(c)) < 0;
};

//// StringSubRemover
StringSubRemover::StringSubRemover()
{
};

StringSubRemover::~StringSubRemover()
{
};

std::string StringSubRemover::operator()(const Joint< std::string, Joint<int, int> > message)
{
	register int position = message.y.x;
	register int length = message.y.y;
	std::string source = message.x;

	return source.erase(position, length);
};

//// StringLength
StringLength::StringLength()
{
};

StringLength::~StringLength()
{
};

int StringLength::operator ()(const std::string message)
{
	return static_cast<int>(message.size());
};

//// StringToBlock
StringToBlock::StringToBlock()
{
};

StringToBlock::~StringToBlock()
{
};

DataBlock StringToBlock::operator ()(const std::string message)
{
	DataBlock result;

	result.length = static_cast<int>(message.size());
	if (result.length > 0)
	{
		result.data = new char[result.length];
		memcpy(result.data, message.c_str(), result.length);
	}
	else
		result.data = NULL;

	return result;
};

//// BlockToString
BlockToString::BlockToString()
{
};

BlockToString::~BlockToString()
{
};

std::string BlockToString::operator ()(const DataBlock message)
{
	if ((message.length == 0) || (message.data == NULL))
		return "";
	else
	{
		std::string result;
		for (int i = 0; i < message.length; i++)
			result += (reinterpret_cast<const char*>(message.data))[i];
		return result;
	};
};

//// StringToText
StringToText::StringToText()
{
	delimiters.push_back("\r\n");
	delimiters.push_back("\n\r");
	delimiters.push_back("\n");
	delimiters.push_back("\r");
};

StringToText::StringToText(const std::list<std::string> _delims)
{
	delimiters = _delims;
};

StringToText::~StringToText()
{
};

void StringToText::Run(const std::list<std::string> _delims)
{
	delimiters = _delims;
};

std::list<std::string> StringToText::operator()(const std::string _source)
{
	std::list<std::string> result;
	std::string source = _source;

	while (1)
	{
		std::string delimiter;
		size_t      position;
		if (!GetDelimPos(source, delimiter, position))
		{
			if (source.size() > 0)
				result.push_back(source);
			return result;
		};
		std::string new_line = source.substr(0, position);
		result.push_back(new_line);
		source = source.substr(position + delimiter.length());
	};
};

bool StringToText::GetDelimPos(const std::string source, std::string& delimiter, size_t& position)
{
	position = static_cast<size_t>(-1);
	delimiter = "";
	for (std::list<std::string>::iterator i = delimiters.begin(); i != delimiters.end(); i++)
	{
		size_t current_pos = source.find(*i);
		if (current_pos != static_cast<size_t>(-1))
		{
			if ((current_pos < position) ||
				((current_pos == position) && (i->length() > delimiter.length())))
			{
				position = current_pos;
				delimiter = *i;
			};
		};
	};
	return position != static_cast<size_t>(-1);
};

TextToString::TextToString()
{
	delimiter = "\n";
};

TextToString::TextToString(const std::string delim)
{
	delimiter = delim;
};

TextToString::~TextToString()
{
};

void TextToString::Run(const std::string delim)
{
	delimiter = delim;
};

std::string TextToString::operator()(const std::list<std::string> _source)
{
	std::list<std::string> source = _source;
	std::string result;
	for (std::list<std::string>::iterator i = source.begin(); i != source.end(); i++)
	{
		if (result != "")
			result += delimiter;
		result += *i;
	};
	return result;
};


//// StringToPair

StringToPair::StringToPair()
	 : error_output(*this), Producer<SIGNAL_TYPE>(error_output), 
	   result_output(*this), Producer< Joint<std::string, std::string> >(result_output)
{
	delimiters.push_back("=");
};

StringToPair::StringToPair(const std::list<std::string> delims)
	 : error_output(*this), Producer<SIGNAL_TYPE>(error_output), 
	   result_output(*this), Producer< Joint<std::string, std::string> >(result_output)
{
	delimiters = delims;
};

StringToPair::~StringToPair()
{
};

void StringToPair::Run(const std::list<std::string> delims)
{
	delimiters = delims;
};

void StringToPair::Run(const std::string source)
{
	Joint<std::string, std::string> result;

	if (delimiters.size() == 0)
	{
		Entity<SIGNAL_TYPE>::SendMessage(error_output, SIGNAL);
		return;
	};

	size_t position = static_cast<size_t>(-1);
	std::string delimiter;

	// Check for every delimiter in the list
	for (std::list<std::string>::iterator i = delimiters.begin(); i != delimiters.end(); i++)
	{
		size_t pos = source.find(*i);
		if (pos < position)
		{
			position = pos;
			delimiter = *i;
		};
	};

	if (position != static_cast<size_t>(-1))
	{
		result.x = source.substr(0, position);
		result.y = source.substr(position + delimiter.length());

		Entity< Joint<std::string, std::string> >::SendMessage(result_output, result);
	}
	else
	{
		Entity<SIGNAL_TYPE>::SendMessage(error_output, SIGNAL);
	};
};
