/****************************************************************************************************/
/* CST Middleware 2008                        * Converters.h                                        */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/****************************************************************************************************/
/* Converters for several standard types.                                                           */
/****************************************************************************************************/
#include "Converters.h"

using namespace CSTMiddleWare::Utilities;

/////////////////////////////////// TextToMap ///////////////////////////

TextToMap::TextToMap(const std::string delim) : delimiter(delim)
{
};

TextToMap::~TextToMap()
{
};

std::map<std::string, std::string> TextToMap::operator()(const Text message)
{
	std::map<std::string, std::string> result;
	if (delimiter.size() == 0)
		return result;

	Text source = message;
	Text delimiters;
	delimiters.push_back(delimiter);
	StringToPair pairing(delimiters);
	StringTrimmer trimmerKey, trimmerValue;
	Split<std::string> splitter;
	Cell<std::string> key;
	Cell<std::string> value;
	for (Text::iterator i = source.begin(); i != source.end(); i++)
	{
		pairing.GetDataOutput().Link(splitter.GetInput());
		splitter.GetOutputX().Link(trimmerKey.GetInput());
		splitter.GetOutputY().Link(trimmerValue.GetInput());
		trimmerKey.GetOutput().Link(key.GetDataInput());
		trimmerValue.GetOutput().Link(value.GetDataInput());
		pairing.GetDataInput().operator ()(*i);
		result[key.GetValue()] = value.GetValue();
	};

	return result;
};

///////////////////////////////////////// MapToText //////////////////////////////////

MapToText::MapToText(const std::string delim) : delimiter(delim)
{
};

MapToText::~MapToText()
{
};

Text MapToText::operator ()(const std::map<std::string,std::string> message)
{
	std::map<std::string,std::string> source = message;
	Text result;

	for (std::map<std::string,std::string>::iterator i = source.begin(); i != source.end(); i++)
	{
		std::string to_add = i->first + delimiter + i->second;
		result.push_back(to_add);
	};

	return result;
};

//////////////////////////////////////// BlockToXCode /////////////////////////////////////////////
static std::string HEXCHARS = "0123456789ABCDEF";

BlockToXCode::BlockToXCode()
{
};

BlockToXCode::~BlockToXCode()
{
};

std::string BlockToXCode::operator()(const DataBlock source)
{
	std::string result;
	for (long i = 0; i < source.length; i++)
	{
		char t0 = static_cast<char*>(source.data)[i] & 0x0F;
		char t1 = (static_cast<char*>(source.data)[i] & 0xF0) >> 4;
		result += HEXCHARS[t1];
		result += HEXCHARS[t0];
	};
	return result;
};

//////////////////////////////////////// XCodeToBlock /////////////////////////////////////////////

XCodeToBlock::XCodeToBlock()
{
};

XCodeToBlock::~XCodeToBlock()
{
};

DataBlock XCodeToBlock::operator()(const std::string source)
{
	DataBlock result((long) source.length() * 2);
	long counter = 0;
	for (size_t i = 0; i < source.size();)
	{
		char t0, t1;
		do
		{
			if (i >= source.size())
			{
				result.length = counter;
				return result;
			};
			t1 = source[i];
			i++;
		}
		while (HEXCHARS.find(t1) == -1);
		do
		{
			if (i >= source.size())
			{
				result.length = counter;
				return result;
			};
			t0 = source[i];
			i++;
		}
		while (HEXCHARS.find(t0) == -1);
		char data = (char)HEXCHARS.find(t1) << 4 | (char)HEXCHARS.find(t0);
		static_cast<char*>(result.data)[counter++] = data;
	};
	result.length = counter;
	return result;
};
