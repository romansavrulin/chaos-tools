/****************************************************************************************************/
/* CST Middleware 2008                        * Strings.h                                           */
/****************************************************************************************************/
/* Copyright (C) Componentality, Konstantin A. Khait, 2008                                          */
/****************************************************************************************************/
/* Strings management components.                                                                   */
/****************************************************************************************************/
#ifndef STRINGS_H
#define STRINGS_H

#include "MessageEngine.h"
#include "Memory.h"
#include <string>
#include <list>

using namespace CSTMiddleWare;
using namespace CSTMiddleWare::MessageEngine;

namespace CSTMiddleWare
{

namespace StandardComponents
{

	// Component to concat two strings
	class StringConcatenator : public Translator< Joint<std::string, std::string>, std::string >
	{
	public:
		StringConcatenator();
		virtual ~StringConcatenator();
	protected:
		virtual std::string operator()(const Joint<std::string, std::string> message);
	};

	// Component to split given string by given position
	class StringSplitter : public Translator< Joint<std::string, int>, Joint<std::string, std::string> >
	{
	public:
		StringSplitter();
		virtual ~StringSplitter();
	protected:
		virtual Joint<std::string, std::string> operator()(const Joint<std::string, int> message);
	};

	// Component to identify the position of the substring (.y) in the given string(.x) (-1 if no such substring)
	class StringFinder : public Translator< Joint<std::string, std::string>, int >
	{
	public:
		StringFinder();
		virtual ~StringFinder();
	protected:
		virtual int operator()(const Joint<std::string, std::string> message);
	};

	// Component to erase substring from string
	class StringCutter : public Translator< Joint< std::string, Joint<int, int> >, std::string >
	{
	public:
		StringCutter();
		virtual ~StringCutter();
	protected:
		virtual std::string operator()(const Joint< std::string, Joint<int, int> > message);
	};

	// Component to extract substring from the string
	class StringSubExtractor : public Translator< Joint< std::string, Joint<int, int> >, std::string >
	{
	public:
		StringSubExtractor();
		virtual ~StringSubExtractor();
	protected:
		virtual std::string operator()(const Joint< std::string, Joint<int, int> > message);
	};

	// Component to remove space and pre-space characters from the left/right of the string
	class StringTrimmer : public Function<std::string>
	{
	public:
		enum TrimmingType
		{
			LEFT_ONLY,
			RIGHT_ONLY,
			LEFT_AND_RIGHT
		};
	protected:
		const TrimmingType type;					// Type of trimming (left, right or both)
		const std::string char_list;				// List of characters to be removed (if empty, all <=' ')
	public:
		StringTrimmer(const TrimmingType _type = LEFT_AND_RIGHT, const std::string _char_list = "");
		virtual ~StringTrimmer();
	protected:
		virtual std::string operator()(const std::string message);
		virtual bool IsAllowed(const char c);
	};

	// Component to remove the part of string
	class StringSubRemover : public Translator< Joint< std::string, Joint<int, int> >, std::string >
	{
	public:
		StringSubRemover();
		virtual ~StringSubRemover();
	protected:
		virtual std::string operator()(const Joint< std::string, Joint<int, int> > message);
	};

	// Component to get string length
	class StringLength : public Translator<std::string, int>
	{
	public:
		StringLength();
		virtual ~StringLength();
	protected:
		virtual int operator()(const std::string message);
	};

	// Convert string to memory block
	class StringToBlock : public Translator<std::string, DataBlock>
	{
	public:
		StringToBlock();
		virtual ~StringToBlock();
	protected:
		virtual DataBlock operator()(const std::string message);
	};

	// Convert memory block to string
	class BlockToString : public Translator<DataBlock, std::string>
	{
	public:
		BlockToString();
		virtual ~BlockToString();
	protected:
		virtual std::string operator()(const DataBlock message);
	};

	// Convert string to text separating it by given set of delimiters
	class StringToText : public Translator< std::string, std::list<std::string> >,
						 public Execute< std::list<std::string> >
	{
	protected:
		std::list<std::string> delimiters;
	public:
		StringToText();
		StringToText(const std::list<std::string>);
		virtual ~StringToText();
	protected:
		virtual void Run(const std::list<std::string>);
		virtual std::list<std::string> operator()(const std::string);
	public:
		virtual InputGate< std::list<std::string> >& GetDelimitersInput() {return Execute< std::list<std::string> >::GetInput();};
		virtual InputGate< std::string >& GetDataInput()
			{return Translator< std::string, std::list<std::string> >::GetInput();};
		virtual OutputGate< std::list<std::string> >& GetDataOutput()
			{return Translator< std::string, std::list<std::string> >::GetOutput();};
	private:
		bool GetDelimPos(const std::string source, std::string& delimiter, size_t& position);
	};

	// Convert text to single string separating parts with given delimiter
	class TextToString : public Translator< std::list<std::string>, std::string >,
						 public Execute< std::string >
	{
	protected:
		std::string delimiter;
	public:
		TextToString();
		TextToString(const std::string);
		virtual ~TextToString();
	protected:
		virtual void Run(const std::string);
		virtual std::string operator()(const std::list<std::string>);
	public:
		virtual InputGate<std::string>& GetDelimiterInput() {return Execute< std::string >::GetInput();};
		virtual InputGate< std::list<std::string> >& GetDataInput()
			{return Translator< std::list<std::string>, std::string >::GetInput();};
		virtual OutputGate<std::string>& GetDataOutput()
			{return Translator< std::list<std::string>, std::string >::GetOutput();};
	};

	// Split string to pair by given delimiter
	class StringToPair : public Execute< std::string >,
						 public Execute< std::list<std::string> >,
						 public Producer<SIGNAL_TYPE>,								// For error
						 public Entity<SIGNAL_TYPE>,
						 public Producer< Joint<std::string, std::string> >,
						 public Entity< Joint<std::string, std::string> >
	{
	protected:
		std::list<std::string>        delimiters;
		DirectOutputGate<SIGNAL_TYPE> error_output;
		DirectOutputGate< Joint<std::string, std::string> > result_output;
	public:
		StringToPair();
		StringToPair(const std::list<std::string>);
		virtual ~StringToPair();
	protected:
		virtual void Run(const std::list<std::string>);
		virtual void Run(const std::string);
	public:
		virtual InputGate< std::list<std::string> >& GetDelimitersInput()
			{return Execute< std::list<std::string> >::GetInput();};
		virtual InputGate<std::string>& GetDataInput()
			{return Execute< std::string >::GetInput();};
		virtual OutputGate< Joint<std::string, std::string> >& GetDataOutput()
			{return result_output;};
		virtual OutputGate<SIGNAL_TYPE>& GetErrorOutput() {return error_output;};
	};

};
	
};

#endif