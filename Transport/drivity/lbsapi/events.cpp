/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2012-2013 */
/*****************************************************************************/
/* Location based events processing. Describes basic components to           */
/* handle events like passing given geographical area                        */
/* Shall be used as a callback-based OO interface                            */
/*****************************************************************************/
/* Attention! No thread safety!                                              */
/*****************************************************************************/

#include "events.h"

using namespace Componentality::EventManagement;

void Processor::subscribe(std::string evt, Subscriber& sb)
{
	std::map<std::string, Subscribers>::iterator s = mSubscribers.find(evt);
	if (s == mSubscribers.end())
	{
		mSubscribers[evt][&sb] = &sb;
		return;
	};
	if (s->second.find(&sb) == s->second.end())
		mSubscribers[evt][&sb] = &sb;
};

void Processor::unsubscribe(std::string evt, Subscriber& sb)
{
	if (mSubscribers.find(evt) != mSubscribers.end())
	{
		std::map<Subscriber*, Subscriber*>::iterator s = mSubscribers[evt].find(&sb);
		if (s != mSubscribers[evt].end())
			mSubscribers[evt].erase(s);
	};
};

void Processor::sendEvent(Event& evt)
{
	Subscribers& s = mSubscribers[evt.getType()];
	for (std::map<Subscriber*, Subscriber*>::iterator i = s.begin(); i != s.end(); i++)
		i->second->onEvent(evt);
};
