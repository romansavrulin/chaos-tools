#include "lbsimpl.h"
#include "records.h"
#include "KeyList.h"
#include "RecordCrypt.h"
#include <iostream>
#include <fstream>
#include <memory.h>

void main()
{
	VEHICLE_ID vehicle_id;
	std::string sf = "R:\\SVN\\nmea-parser-c\\reference\\data\\data.log";
	vehicle_id.mVehicleID = "VEHICLE-0001";
	Componentality::LBS::FileLogger logger(vehicle_id, Componentality::LBS::FileLogger::TIME, "c:\\temp");
	logger.init();
	Componentality::LBS::PeriodicalLogger pl(1, logger);

	std::ifstream file(sf.c_str());
	logger.parse(file);

	return;

	POSITION x1, x2;

	x1.mLatitude.mCoordinate = 59.0 + 48.0 / 60 + 1.0 / 3600;
	x1.mLongitude.mCoordinate = 30.0 + 15.0 / 60.0 + 45.0 / 3600;
	x1.mAltitude.mCoordinate = 0.0;
	x2.mLatitude.mCoordinate = 59.0 + 58.4907 / 60.0;
	x2.mLongitude.mCoordinate = 30.0 + 14.6841 / 60.0;
	x2.mAltitude.mCoordinate = 16.4;

	double d = Componentality::LBS::distance(x1, x2);

	Componentality::LBS::Gate g1("gate1", x1, 50, 0, 10);
	Componentality::LBS::GateSet gset;
	gset.addGate(g1);
	VEHICLE_INFO vi;
	initVehicleInfo(&vi);
	vi.mLocation.mPosition = x2;
	vi.mLocation.mAvailable = True;
	vi.mHeading.mHeading = 0;

	gset.check(vi);

	std::string s = Componentality::LBS::packVehicleInfo(vi);

	VEHICLE_ID vid1, vid2;
	vid1.mVehicleID = "V1";
	vid2.mVehicleID = "V2";

	Componentality::LBS::LBSecurityProvider sp1(vid1);
	Componentality::LBS::LBSecurityProvider sp2(vid2);

	Componentality::LBS::LBSecureRecord lr1(vid1, vi, sp1);
	Componentality::LBS::LBSecureRecord lr2(vid2, sp2);

	std::string s1 = sp1.init();
	std::string s2 = sp2.init();
	sp1.registerRemoteSide(vid2, s2);
	sp2.registerRemoteSide(vid1, s1);

	s = lr1.Serialize();
	bool v = lr2.Deserialize(s);
};