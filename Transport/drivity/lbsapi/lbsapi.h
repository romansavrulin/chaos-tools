/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2009-2013 */
/*****************************************************************************/
/* The highest level API to access default set of tracking functions         */
/*****************************************************************************/

#ifndef LBSAPI_H
#define LBSAPI_H

#include "lbsimpl.h"

namespace Componentality
{
	namespace LBS
	{

		// API class. Usually application has only one instance of LBSAPI, however it is not
		// implemented as a singleton to allow further extensions use multiple LB interfaces
		class LBSAPI
		{
		private:
			FileLogger* mLogger;					// Actually file logger is only used
			PeriodicalLogger* mPeriodicLogger;		// Periodical logger to handle regular logging messages
			GateLogger* mGateLogger;				// Gate logger to create messages when gate is passed
			std::string mConfigDirectory;			// Directory where config files stored
			char* mVIDLine;							// Vehicle ID (just for memory management to purge storage properly)
		protected:
			virtual void dispose();					// Clean up all parameters (before load() or on disposal)
			virtual void save();					// Save configuration files
			virtual void load();					// Load from configuration files
		public:
			LBSAPI(const std::string config_dir = "");
			virtual ~LBSAPI();
		public:
			// Do initial setup. It must be only run if the device was never configured. Creates public and secure key
			// pairs for both encryption and signing of records
			virtual std::string init(const std::string vehicle_id, const std::string output_folder = "");
			// Get list of paired devices
			virtual std::string list();
			// Pair to the given device (register given device). Device keys come from initialization file
			virtual bool pair(const std::string vehicle_id, const std::string infile);
			// Forget device with given name and don't produce keys for it anymore
			virtual bool forget(const std::string vehicle_id);
			// Create track records to the given folder from the given input stream (cin by default)
			virtual void track(const long period, const std::string output_folder, const std::string input_stream = "");
			// Parse record from the given file and create pair of <device name>, <JSON record>
			virtual std::pair<std::string, std::string> parse(const std::string filename);
			// Parse record from the given file to vehicle info structure
			virtual bool parse(VEHICLE_INFO& result, const std::string filename);
			// Add gates from gate description string (not file!!)
			virtual bool addGates(const std::string gates);
			// Remove gate with given name
			virtual bool removeGate(const std::string gate_name);
			// Get gates list
			virtual std::string getGates();
			// Encrypt custom data
			virtual std::string encrypt(const std::string decrypted);
			// Encrypt custom data for a single target vehicle
			virtual std::string encrypt(const std::string decrypted, const std::string vehicle_id);
			// Decrypt custom data
			virtual std::string decrypt(const std::string encrypted);
		};

		std::string fread(const std::string&);

	}; // namespace LBS
}; // namespace Componentality

#endif