/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2009-2013 */
/*****************************************************************************/
/* Medium level interface. To be used if LBSAPI is not enough functional     */
/*****************************************************************************/

#ifndef LBSIMPL_H
#define LBSIMPL_H

#include "events.h"
#include "lbsdefs.h"
#include "records.h"
#include <iostream>
#include <time.h>

namespace Componentality
{
	namespace LBS
	{

		// Event manifest key
		extern const std::string ARG_EVENT;
		// Gate name manifest key
		extern const std::string ARG_GATE;

		// NMEA parsing object (produces events)
		class NMEAParser : public GateSet
		{
		public:
			NMEAParser();
			virtual ~NMEAParser();
		public:
			virtual void parse(const std::string);
			virtual void parse(std::istream&);
		};

		// Main processing class
		class Logger : public LBSecurityProvider, public NMEAParser
		{
		protected:
			VEHICLE_ID mVehicleID;				// Here is the main storage of the vehicle ID
		public:
			Logger(const VEHICLE_ID vid);
			virtual ~Logger();
			virtual const VEHICLE_ID& getVehicleID() const;
		public:
			// Add record to the log
			virtual void addRecord(const std::string&) = 0;
			// Read both security provider and GateSet from config files
			virtual bool deserialize(const std::string);
		};

		// Sublogger processes events periodically (i.e. filters location events with given frequency)
		class PeriodicalLogger : public Componentality::EventManagement::Subscriber
		{
		protected:
			long mPeriod;									// Period in seconds
			Logger& mOwner;									// Logger
			time_t mLastLoggingMoment;						// Last time when event was logged
		public:
			PeriodicalLogger(const long period, Logger&);
			virtual ~PeriodicalLogger();
		public:
			virtual void onEvent(Componentality::EventManagement::Event&);
		};

		// Sublogger processing Gate events
		class GateLogger : public Componentality::EventManagement::Subscriber
		{
		protected:
			Logger& mOwner;
		public:
			GateLogger(Logger&);
			virtual ~GateLogger();
		public:
			virtual void onEvent(Componentality::EventManagement::Event&);
		};

		// Logger which creates encrypted records upon every message to log
		class FileLogger : public Logger
		{
		public:
			enum NAMING_MODE
			{
				HASH,								// Make file name from SHA hash
				RANDOM,								// Make file name randomly
				TIME								// Use current time and vehicle ID for file naming (default)
			};
		protected:
			NAMING_MODE mNamingMode;				// Naming mode
			std::string mFolder;					// Folder to store output records
		protected:
			// Create file name for given content record
			virtual std::string getFilename(const std::string& content);
		public:
			FileLogger(const VEHICLE_ID vid, const NAMING_MODE, const std::string folder);
			virtual ~FileLogger();
		public:
			// Encrypt and save record
			virtual void addRecord(const std::string&);
			// Set new outgoing folder
			virtual void setFolder(const std::string& fld) {mFolder = fld;};
			// Get currently used folder
			virtual const std::string& getFolder() const {return mFolder;};
		};

	}; // namespace LBS
}; // namespace Componentality

#endif