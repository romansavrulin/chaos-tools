/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2009-2013 */
/*****************************************************************************/
/* Low level interface. Works with separate records in Manifest + Content    */
/* form. Can be used in case if higher level APIs can't be applied because   */
/* of too narrow for required features                                       */
/*****************************************************************************/

#ifndef RECORDS_H
#define RECORDS_H

#include "lbsdefs.h"
#include <list>

namespace Componentality
{
	namespace LBS
	{
		// JSON keys for storing of vehicle location and parameters
		extern std::string ARG_LATITUDE;
		extern std::string ARG_LONGITUDE;
		extern std::string ARG_ALTITUDE;
		extern std::string ARG_AVAILABILITY;
		extern std::string ARG_VEHICLE_ID;
		extern std::string ARG_HEADING;
		extern std::string ARG_VELOCITY;
		extern std::string ARG_TIMESTAMP;
		extern std::string ARG_HDOP;
		extern std::string ARG_VDOP;
		extern std::string ARG_CHECKSUM;

		// Location based data record
		class LBRecord
		{
		protected:
			VEHICLE_INFO mInfo;				// Vehicle location info
			VEHICLE_ID   mId;				// Vehicle ID
			TIMESTAMP	 mTime;				// Time stamp
			bool		 mFreeNeeded;		// Internal field to indicate that memory cleanup required
		public:
			LBRecord(const VEHICLE_ID);
			LBRecord(const VEHICLE_ID, const VEHICLE_INFO);
			LBRecord(const VEHICLE_ID, const VEHICLE_INFO, const TIMESTAMP);
			virtual ~LBRecord();
		public:
			virtual std::string Serialize(const bool add_checksum = false);
			virtual bool Deserialize(const std::string);
		public:
			virtual VEHICLE_INFO& getVehicleInfo() {return mInfo;};
			virtual VEHICLE_ID& getVehicleId() {return mId;};
			virtual TIMESTAMP& getTimeStamp() {return mTime;};
		};

		// Convert double to string and string to double. Useful for other modules
		std::string doubleToString(double);
		double stringToDouble(std::string);

		// Serialize and deserialize vehicle info (not provided by DRVLOC)
		std::string packVehicleInfo(const VEHICLE_INFO&);
		VEHICLE_INFO unpackVehicleInfo(const std::string);

		// Add checksum field to the record
		std::string addChecksum(const std::string&);
		// Verify checksum for the record
		bool verifyChecksum(const std::string&);

		// Split manifest and content
		std::pair< std::string, std::string > __split_nn(const std::string str);
		// Get value for given key in the manifest (header)
		std::string __find(const std::string _header, const std::string keyval);

		// Encryptor
		class LBSecurityProvider
		{
		private:
			void* mRecordEncryptor;						// Record encryption module
		public:
			LBSecurityProvider(const VEHICLE_ID);
			virtual ~LBSecurityProvider();
		public:
			virtual std::string init();													// Initialization of key pairs (only if not created yet)
			virtual std::list<std::string> listRemoteSides();							// List all remote sides
			virtual void registerRemoteSide(const VEHICLE_ID, const std::string keys);	// Pair with remote device
			virtual void unregisterRemoteSide(const VEHICLE_ID);						// Unpair with remote device
			virtual std::string serialize();
			virtual bool deserialize(const std::string);
			virtual std::string encrypt(const std::string&);							// Encrypt record (must have checksum field)
			virtual std::string decrypt(const std::string&);							// Decrypt record
			virtual std::string getID();												// Retrieve vehicle ID
			virtual std::string getKeys(const std::string&);							// Retrieve keys for given remote device
		};

		// LBRecord encrypting/decrypting with security provider
		class LBSecureRecord : public LBRecord
		{
		protected:
			LBSecurityProvider& mSecurityProvider;
		public:
			LBSecureRecord(const VEHICLE_ID, LBSecurityProvider&);
			LBSecureRecord(const VEHICLE_ID, const VEHICLE_INFO, LBSecurityProvider&);
			LBSecureRecord(const VEHICLE_ID, const VEHICLE_INFO, const TIMESTAMP, LBSecurityProvider&);
			virtual ~LBSecureRecord();
		public:
			// Serialize and deserialize use security provider for encryption and decryption
			virtual std::string Serialize(const bool add_checksum = false);
			virtual bool Deserialize(const std::string);
		};

	}; // namespace LBS
}; // namespace Celebrity

#endif