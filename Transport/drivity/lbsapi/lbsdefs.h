/*****************************************************************************/
/* Vehicle Tracking API                   * (C) Componentality Oy, 2012-2013 */
/*****************************************************************************/
/* Main definition of Location Based Systems API with sequential and         */
/* event triggered location processing                                       */
/*****************************************************************************/
/* Attention! No thread safety!                                              */
/*****************************************************************************/

#ifndef LBSDEFS_H
#define LBSDEFS_H

#include "events.h"
#include "drvloc.h"
#include <list>

namespace Componentality
{

	namespace LBS
	{

		extern const char* LBSEVT_LOCATION;				// Event type for periodical location messages
		extern const char* LBSEVT_GATEPASS;				// Event type for gate (geozone) crossing message

		// Event type for periodical messages
		class LocationEvent : public Componentality::EventManagement::Event
		{
		protected:
			const VEHICLE_INFO& mInfo;
		public:
			LocationEvent(const VEHICLE_INFO&, const std::string = LBSEVT_LOCATION);
			virtual ~LocationEvent();
		public:
			const VEHICLE_INFO& getVehicleInfo() const {return mInfo;}
		};

		// Gate structure
		class Gate
		{
			friend class GateSet;
		protected:
			std::string mName;				// Gate name or ID
			POSITION mLocation;				// Gate center
			long	 mRange;				// Range where gate considers
			long	 mDirection;			// Pass direction (0...359)
			long	 mDirectionPrecision;	// Precision of direction (degrees)
		protected:
			Gate() {};
		public:
			Gate(const std::string, const POSITION pos, const long range, const long dir, const long dir_prec);
			virtual ~Gate();
			virtual std::string serialize() const;
			virtual bool deserialize(const std::string&);
			std::string getName() const {return mName;};
		};

		// Function calculates distance between two locations on geoid in meters
		double distance(const POSITION& p1, const POSITION& p2);

		// Event for geozone crossing events
		class GateEvent : public LocationEvent
		{
		protected:
			const Gate& mGate;
		public:
			GateEvent(const VEHICLE_INFO& vi, const Gate& gate) : LocationEvent(vi, LBSEVT_GATEPASS), mGate(gate) {};
			virtual ~GateEvent() {};
		public:
			const Gate& getGate() const {return mGate;}
			virtual std::string getInfo() const;
		};

		// List (map) of gates. Main feature, GateSet is Processor, i.e. produces events on each "check".
		// Location event every time and Gate event if gate zone crossed
		class GateSet : public Componentality::EventManagement::Processor
		{
		private:
			std::list<Gate> mGates;
		public:
			GateSet();
			virtual ~GateSet();
		public:
			virtual void addGate(Gate&);							// Add new gate to the map
			virtual void removeGate(Gate&);							// Remove gate from the map (name is only used)
			virtual bool check(VEHICLE_INFO& p);					// Check if the location stated in vehicle info structure is within the gate zone
			virtual std::string serialize();						// Serialize the list of gates
			virtual bool deserialize(const std::string&);			// Read the list of gates
			virtual GateSet& operator+=(GateSet&);					// Join two lists
			virtual Gate* find(const std::string&);					// Find gate in the list or get NULL if not there
		protected:
			virtual bool checkGate(VEHICLE_INFO& p, Gate& gate);	// Check single gate passing
		};
	}; // namespace LBS
}; // namespace Componentality


#endif