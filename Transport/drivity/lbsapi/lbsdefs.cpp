#include "lbsdefs.h"
#include "Converters.h"
#include "StandardComponents.h"
#include "records.h"
#include <math.h>
#include <memory.h>

using namespace CSTMiddleWare::StandardComponents;
using namespace CSTMiddleWare::Utilities;

const char* Componentality::LBS::LBSEVT_LOCATION = ".LBSLOC";
const char* Componentality::LBS::LBSEVT_GATEPASS = ".LBSGTPS";

namespace Componentality
{
	namespace LBS
	{
		extern std::pair< std::string, std::string > __split_nn(const std::string);
		extern std::string __find(const std::string, const std::string);
	}
}

namespace Componentality {
	namespace LBS
	{
		std::string __find(const std::string record, const std::string arg);
	}
}

const std::string GATE_NAME					= "GT.NAM";
const std::string GATE_LATITUDE				= "GT.LAT";
const std::string GATE_LONGITUDE			= "GT.LON";
const std::string GATE_ALTITUDE				= "GT.ALT";
const std::string GATE_RANGE				= "GT.RNG";
const std::string GATE_DIRECTION			= "GT.DIR";
const std::string GATE_DIRECTION_PRECISION	= "GT.DPR";

using namespace Componentality::LBS;
using namespace CSTMiddleWare::Utilities;

LocationEvent::LocationEvent(const VEHICLE_INFO& info, const std::string type) : Componentality::EventManagement::Event(type), mInfo(info) {};
LocationEvent::~LocationEvent() {};

Gate::Gate(const std::string name, const POSITION pos, const long range, const long dir, const long dir_prec) :
	mName(name),
	mLocation(pos),
	mRange(range),
	mDirection(dir),
	mDirectionPrecision(dir_prec)
{
}

Gate::~Gate()
{
};

double Componentality::LBS::distance(const POSITION& p1, const POSITION& p2)
{
	const double R_EQ = 6378;			// Equatorial Earth radius
	const double R_POL = 6357;			// Polar Earth radius
	const double PI = 3.1415926535897932384626433832795;
	double r = R_EQ + (R_POL - R_EQ) * (p2.mLatitude.mCoordinate + p1.mLatitude.mCoordinate) / (2 * PI);
	double latitude_1 = p1.mLatitude.mCoordinate * PI / 180;
	double latitude_2 = p2.mLatitude.mCoordinate * PI / 180;
	double longitude_1 = p1.mLongitude.mCoordinate * PI / 180;
	double longitude_2 = p2.mLongitude.mCoordinate * PI / 180;

	double dist = r * acos(
		sin(latitude_1) * sin(latitude_2) +
		cos(latitude_1) * cos(latitude_2) * cos(longitude_1 - longitude_2)
		);

	return dist * 1000;
};

GateSet::GateSet()
{
};

GateSet::~GateSet()
{
};

void GateSet::addGate(Gate& gate)
{
	mGates.push_back(gate);
};

Gate* GateSet::find(const std::string& name)
{
	for (std::list<Gate>::iterator i = mGates.begin(); i != mGates.end(); i++)
	{
		if (i->getName() == name)
			return &(*i);
	};
	return NULL;
}

void GateSet::removeGate(Gate& gate)
{
	for (std::list<Gate>::iterator i = mGates.begin(); i != mGates.end();)
	{
		if (i->getName() == gate.getName())
		{
			i = mGates.erase(i);
			return;
		}
		else
			i++;
	};
};

bool GateSet::checkGate(VEHICLE_INFO& p, Gate& gate)
{
	if (!p.mLocation.mAvailable)
		return false;
	double heading = p.mHeading.mHeading;
	POSITION position = p.mLocation.mPosition;
	double direction_lowbound = gate.mDirection - gate.mDirectionPrecision;
	double direction_upbound = gate.mDirection + gate.mDirectionPrecision;
	bool dircheck = (heading >= direction_lowbound) && (heading <= direction_upbound);
	dircheck |= (heading >= direction_lowbound + 360) && (heading <= direction_upbound + 360);
	if (!dircheck)
		return false;
	double dist = distance(position, gate.mLocation);
	return dist <= gate.mRange;
};

bool GateSet::check(VEHICLE_INFO& p)
{
	bool result = false;
	LocationEvent levt(p);
	sendEvent(levt);
	for (std::list<Gate>::iterator i = mGates.begin(); i != mGates.end(); i++)
	{
		if (checkGate(p, *i))
		{
			result = true;
			GateEvent gevt(p, *i);
			sendEvent(gevt);
		};
	};
	return result;
};

std::string GateEvent::getInfo() const
{
	return mGate.getName();
};

std::string Gate::serialize() const
{
	std::string result = GATE_NAME + '=' + mName + '\n';
	result += GATE_LATITUDE + '=' + doubleToString(mLocation.mLatitude.mCoordinate) + '\n';
	result += GATE_LONGITUDE + '=' + doubleToString(mLocation.mLongitude.mCoordinate) + '\n';
	result += GATE_ALTITUDE + '=' + doubleToString(mLocation.mAltitude.mCoordinate) + '\n';
	result += GATE_RANGE + '=' + doubleToString(mRange) + '\n';
	result += GATE_DIRECTION + '=' + doubleToString(mDirection) + '\n';
	result += GATE_DIRECTION_PRECISION + '=' + doubleToString(mDirectionPrecision) + '\n';

	return result;
};

bool Gate::deserialize(const std::string& str)
{
	mName = Componentality::LBS::__find(str, GATE_NAME);
	mLocation.mLatitude.mCoordinate = stringToDouble(Componentality::LBS::__find(str, GATE_LATITUDE));
	mLocation.mLongitude.mCoordinate = stringToDouble(Componentality::LBS::__find(str, GATE_LONGITUDE));
	mLocation.mAltitude.mCoordinate = stringToDouble(Componentality::LBS::__find(str, GATE_ALTITUDE));
	mRange = stringToDouble(Componentality::LBS::__find(str, GATE_RANGE));
	mDirection = stringToDouble(Componentality::LBS::__find(str, GATE_DIRECTION));
	mDirectionPrecision = stringToDouble(Componentality::LBS::__find(str, GATE_DIRECTION_PRECISION));

	return true;
};

std::string GateSet::serialize()
{
	unsigned int i;
	std::string s;
	NumberToString<unsigned int> nts;
	Cell<std::string> keeper;
	nts.GetOutput().Link(keeper.GetDataInput());
	for (std::list<Gate>::iterator i = mGates.begin(); i != mGates.end(); i++)
	{
		s += i->serialize();
		s += "\n\n";
	};

	return s;
};

bool GateSet::deserialize(const std::string& str)
{
	std::string source = str;
	std::pair<std::string, std::string> pair;

	while (source.size() > 5)
	{
		Gate g;
		pair = __split_nn(source);
		if (pair.first.empty())
			pair.first = source;
		if (g.deserialize(pair.first))
			mGates.push_back(g);
		source = pair.second;
	};
	return true;
};

GateSet& GateSet::operator+=(GateSet& gs)
{
	for (std::list<Gate>::iterator i = gs.mGates.begin(); i != gs.mGates.end(); i++)
	{
		mGates.push_back(*i);
	};
	return *this;
};