#include "lbsimpl.h"
#include "drvloc.h"
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>

using namespace Componentality::LBS;
using namespace Componentality::EventManagement;

const std::string Componentality::LBS::ARG_GATE = "gte";
const std::string Componentality::LBS::ARG_EVENT = "evt";

#ifdef WIN32
#define SLASH '\\'
#else
#define SLASH '/'
#endif

extern std::pair< std::string, std::string > Componentality::LBS::__split_nn(const std::string);
extern std::string Componentality::LBS::__find(const std::string, const std::string);

Logger::Logger(const VEHICLE_ID vid) : LBSecurityProvider(vid), mVehicleID(vid)
{
};
	
Logger::~Logger()
{
};

const VEHICLE_ID& Logger::getVehicleID() const
{
	return mVehicleID;
};

bool Logger::deserialize(const std::string str)
{
	bool result = LBSecurityProvider::deserialize(str);
	if (result)
	{
		std::string idline = getID();
		if (mVehicleID.mVehicleID)
			delete[] mVehicleID.mVehicleID;
		mVehicleID.mVehicleID = new char[idline.size() + 1];
		strcpy(mVehicleID.mVehicleID, idline.c_str());
	};
	return result;
};

PeriodicalLogger::PeriodicalLogger(const long period, Logger& owner) : mPeriod(period), mOwner(owner)
{
	mOwner.subscribe(LBSEVT_LOCATION, *this);
	time(&mLastLoggingMoment);
};

PeriodicalLogger::~PeriodicalLogger()
{
	mOwner.unsubscribe(LBSEVT_LOCATION, *this);
};

void PeriodicalLogger::onEvent(Componentality::EventManagement::Event& evt)
{
	time_t current_time;
	time(&current_time);
	if (current_time - mLastLoggingMoment > mPeriod)
	{
		LocationEvent& levt = ((LocationEvent&) evt);
		VEHICLE_INFO vi = levt.getVehicleInfo();
		LBSecureRecord _record(mOwner.getVehicleID(), vi, mOwner);
		std::pair<std::string, std::string> pair = __split_nn(_record.Serialize(true));
		std::string record = pair.first +
			'\n' + ARG_EVENT + '=' + LBSEVT_LOCATION +
			"\n\n" + pair.second;
		mOwner.addRecord(record);
		mLastLoggingMoment = current_time;
	};
};

GateLogger::GateLogger(Logger& owner) : mOwner(owner)
{
	mOwner.subscribe(LBSEVT_GATEPASS, *this);
};

GateLogger::~GateLogger()
{
	mOwner.unsubscribe(LBSEVT_GATEPASS, *this);
};

void GateLogger::onEvent(Componentality::EventManagement::Event& evt)
{
	LBSecureRecord _record(mOwner.getVehicleID(), ((GateEvent&) evt).getVehicleInfo(), mOwner);
	std::pair<std::string, std::string> pair = __split_nn(_record.Serialize(true));
	std::string record = pair.first +
		'\n' + ARG_EVENT + '=' + LBSEVT_GATEPASS +
		'\n' + ARG_GATE + ((GateEvent&) evt).getGate().getName() +
		"\n\n" + pair.second;
	mOwner.addRecord(record);
};

/////////////////////////////////////////////////////////////////////////////////

FileLogger::FileLogger(const VEHICLE_ID vid, const NAMING_MODE mode, const std::string folder) : 
	Logger(vid),
	mNamingMode(mode),
	mFolder(folder)
{
};

FileLogger::~FileLogger()
{
};
	
void FileLogger::addRecord(const std::string& rec)
{
	std::string fname = getFilename(rec);
	if (fname.empty())
		return;
	std::ofstream file(fname.c_str(), std::ios::binary);
	file.write(rec.c_str(), rec.size());
	file.close();
};
	
std::string xcode(const std::string& name)
{
	std::string xcode;
	for (size_t i = 0; i < name.size(); i++)
	{
		xcode += ((name[i] >> 4) & 0x0F) + '0';
		xcode += ((name[i] >> 0) & 0x0F) + '0';	
	};
	return xcode;
};

std::string FileLogger::getFilename(const std::string& content)
{
	std::string name;

	switch (mNamingMode)
	{
	case HASH:
		{
			name = __find(content, ARG_CHECKSUM);
		}
		name = xcode(name);
		break;
	case RANDOM:
		{
			time_t _time;
			time(&_time);
			srand((unsigned int) _time);
			for (int i = 0; i < 0; i++)
			{
				name += (char) rand();
			};
			name = xcode(name);
		}
		break;
	case TIME:
		{
			time_t _time;
			time(&_time);
			while (_time > 0)
			{
				name = ((char) ((_time % 10) + '0')) + name;
				_time /= 10;
			};
			std::string vid = getVehicleID().mVehicleID;
			name = vid + '-' + name;
		}
		break;
	};

	if (!name.empty() && !mFolder.empty())
		name = mFolder + SLASH + name;

	return name;
};

NMEAParser::NMEAParser()
{
};

NMEAParser::~NMEAParser()
{
};

void NMEAParser::parse(const std::string str)
{
	VEHICLE_INFO vi = parseNMEA(str.c_str());
	if (vi.mLocation.mAvailable)
		check(vi);
};

void NMEAParser::parse(std::istream& stream)
{
	std::string read;
	while (!stream.eof() && !stream.bad())
	{
		stream >> read;
		parse(read);
	};
};
