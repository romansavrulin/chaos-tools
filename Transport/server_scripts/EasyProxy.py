#!/usr/bin/python
#-------------------------------------------------------------------------------
# Name:        EasyProxy
# Purpose:     Receive easy JSON records and transfer data to the backend
#              via Web-based API
#              Third Party Neutral Edition
#
# Author:      Konstantin Khait
#
# Created:     25/11/2013
# Copyright:   (c) Componentality Oy
# Licence:     GPL
#-------------------------------------------------------------------------------

import os
import subprocess
import tempfile
import json
import urllib
import sys
import hashlib
import time
import argparse

print("Drivity Tools Library")
print("Easy Proxy for JSON Objects")
print("Copyright (C) Componentality Oy, 2013")


SERVER = 'http://api.flexroad.ru/senddata.py'

python = sys.version[0]

if python == '3':
    from urllib.parse import urlencode
    from urllib.request import urlopen

if python == '3':
    import socketserver
else:
    import SocketServer as socketserver


# Do parsing of parameters
parser = argparse.ArgumentParser()
parser.add_argument('--port')
parser.add_argument('--backend')
args = parser.parse_args()
port = args.port
backend = args.backend

if backend:
    SERVER = backend

TCP_PORT = port

def fillobj(jobj):
    if not ('tms' in jobj):
        jobj['tms'] = str(time.time())
    return jobj

def processRequest(vehicle_id, jobj):
    try:
        # Send JSON to componentality server
        data_to_send = json.dumps(jobj).encode()
        if (not vehicle_id) or (vehicle_id == ""):
            if ('vid' in jobj):
                vehicle_id = jobj['vid']
            else:
                print("No vehicle ID")
                return;
        # Fill object with placeholders
        jobj = fillobj(jobj)
        # If avl == 1 it means that the valid location record is here
        # if avl == 0 it means that invalid location record is here
        # if avl == "" it means there is no such item
        data_to_send = json.dumps(jobj).encode()
        dblock = {'data':data_to_send, 'vid':vehicle_id, 'checksumm':hashlib.md5(data_to_send).hexdigest()}
        if ('avl' in jobj):
            if str(jobj['avl']) == '1':
                if 'lat' in jobj:
                    dblock['lat'] = jobj['lat']
                if 'lon' in jobj:
                    dblock['lon'] = jobj['lon']
                if 'alt' in jobj:
                    dblock['alt'] = jobj['alt']
                if 'alt' in jobj:
                    dblock['alt'] = jobj['alt']
        if 'tms' in jobj:
            dblock['tms'] = jobj['tms']

        if int(python) < 3:
            data = urllib.urlencode(dblock)
            r = urllib.urlopen(SERVER, data)
        else:
            data = urlencode(dblock)
            r = urlopen(SERVER, data.encode())

        return r.read().decode()
    except Exception as e:
        print("ERROR")
        return "ERROR in data"

class TCPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        while True:
           try:
                self.data = self.request.recv(4096).strip()
                # Load JSON content from decoded file
                # No non-JSON data allowed
                data = self.data.decode("utf-8")
                data = data.split("}")[0]
                data += "}"
                try:
                    print(data)
                    jobj = json.loads(data)
                    # Send data to the back end
                    response = processRequest(None, jobj)
                    self.request.sendall(response.encode())
                    # Remove data chunk when processed
                except Exception as e:
                    print(str(e))
                    break
           except:
               break
while True:
   try:
        TCP_IP = '0.0.0.0'
        if TCP_PORT == None:
            TCP_PORT = "1979"
        HOST, PORT = TCP_IP, int(TCP_PORT)
        # Simply open socket connection for multithread mode
        server = socketserver.ThreadingTCPServer((HOST, PORT), TCPHandler)
        server.serve_forever()
   except Exception as e:
       print("SOME ERROR IN MAIN LOOP")