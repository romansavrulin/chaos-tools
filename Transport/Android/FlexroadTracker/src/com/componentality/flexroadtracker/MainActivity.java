package com.componentality.flexroadtracker;

import android.os.Bundle;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	String TAG = "Flexroad Tracker";
	String freq = "30";
	SharedPreferences sPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
				
		sPref = getSharedPreferences("flexroad",MODE_PRIVATE);
		
		Button save = (Button) findViewById(R.id.saveButton);
		Button startStop = (Button) findViewById(R.id.startStopButton);
		
		if(!isMyServiceRunning()) startStop.setBackgroundResource(R.drawable.kn_f2);
		else startStop.setBackgroundResource(R.drawable.kn_f1);
		
		final TextView freqText = (TextView) findViewById(R.id.freq);
		freqText.setText(sPref.getString("freq", "30"));
		
		save.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				freq = (String) freqText.getText().toString();		
			    Editor ed = sPref.edit();
			    ed.putString("freq", freq);
			    ed.commit();
			    finish();
			}
			
		});
		
		startStop.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(!isMyServiceRunning()){
					startService(new Intent(FlexroadService.MY_SERVICE));
					v.setBackgroundResource(R.drawable.kn_f1);
				}
				else {
					stopService(new Intent(FlexroadService.MY_SERVICE));
					v.setBackgroundResource(R.drawable.kn_f2);
				}
			}
			
		});
	}

	
	private boolean isMyServiceRunning() {
	    ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if ("com.componentality.flexroadtracker.FlexroadService".equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}

}
