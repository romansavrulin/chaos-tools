package com.componentality.flexroadtracker;

import java.io.IOException;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

public class FlexroadService extends Service {
	public static final String MY_SERVICE = "com.componentality.flexroadtracker.FlexroadService";
	String TAG = "Flexroad Tracker";
	double lat;
	double lng;
	double alt;
	float speed;
	long tms;
	float hdg;
	float accuracy;
	public static LocationManager lm;
	MyLocationListener locationListener;
	public static Criteria criteria;
	String provider;
	SharedPreferences sPref;
	Editor ed;
	String data;
	String deviceId;
	int freq;
	
	private Handler handler = new Handler();
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override
	public void onCreate() {
		Log.v(TAG, "onStartCreate");
		super.onCreate();
	}
	
	public int onStartCommand(Intent intent, int flags, int startId) {
	    Log.v(TAG, "onStartCommand");
	    deviceId =  android.os.Build.SERIAL;
	    Log.v(TAG, "device ID: "+deviceId);
	    lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

	    locationListener = new MyLocationListener();
	    criteria = new Criteria();
	    criteria.setAccuracy(Criteria.ACCURACY_LOW);
	    provider = lm.getBestProvider(criteria, true);
	    sPref = getBaseContext().getSharedPreferences("flexroad",MODE_PRIVATE);
	    freq = Integer.parseInt(sPref.getString("freq", "30"))*1000;
	    ed = sPref.edit();
	    ed.putBoolean("ServiceStarted", true).commit();
	    
	    RemoteViews view = new RemoteViews(getPackageName(), R.layout.flexroadwidget);
	    view.setImageViewResource(R.id.widget_button, R.drawable.kn_f1);
	    ComponentName thisWidget = new ComponentName(this, FlexroadWidgetProvider.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(this);
        manager.updateAppWidget(thisWidget, view);

	    lm.requestLocationUpdates(provider, 1, 0, locationListener);
	    
	    handler.post(runnable);

	    return super.onStartCommand(intent, flags, startId);
	  }
	
	private void sendData(){
		try {
		SendSocket s = new SendSocket();
		s.write(data.getBytes());
		}
		catch (IOException e){
			Log.v(TAG,"Socket error "+e.getMessage());
		}
		
		
	}
	
	@Override
	public void onDestroy() {
		Log.v(TAG, "onDestroy");
		handler.removeCallbacks(runnable);
	    ed.putBoolean("ServiceStarted", false).commit();
	    RemoteViews view = new RemoteViews(getPackageName(), R.layout.flexroadwidget);
	    view.setImageViewResource(R.id.widget_button, R.drawable.kn_f2);
	    ComponentName thisWidget = new ComponentName(this, FlexroadWidgetProvider.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(this);
        manager.updateAppWidget(thisWidget, view);
		super.onDestroy();
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		ed.putBoolean("ServiceStarted", false).commit();
	    stopSelf();
	    return super.onUnbind(intent);
	}
	
	private Runnable runnable = new Runnable() 
	{

	    public void run() 
	    {
	    	provider = lm.getBestProvider(criteria, true);
    	    Location mostRecentLocation = lm.getLastKnownLocation(provider);
    	    Log.v(TAG, provider);
    	    if(mostRecentLocation != null) {
    	        lat = mostRecentLocation.getLatitude();
    	        lng = mostRecentLocation.getLongitude();
    	        alt = mostRecentLocation.getAltitude();
    	        speed = mostRecentLocation.getSpeed();
    	        tms = mostRecentLocation.getTime()/1000;
    	        hdg = mostRecentLocation.getBearing();
    	        accuracy = mostRecentLocation.getAccuracy();
    	            	    
        	    data = "{ " 
        	    +"\"vid\":\"" + deviceId + "\","
        	    +"\"lat\":\"" + lat + "\","
        	    +"\"lon\":\"" + lng + "\","
        	    +"\"alt\":\"" + alt + "\","
        	    +"\"vel\":\"" + speed + "\","
        	    +"\"tms\":\"" + tms + "\","
        	    +"\"hdg\":\"" + hdg + "\","
        	    //+"\"accu\":\"" + accuracy + "\","
        	    ;
        	    
        	    if(lat > 0 && lng >0)
        	    	data += "\"avl\":\"1\"";
        	    else
        	    	data += "\"avl\":\"0\"";
        	    data += " }";
        	    
        	    Log.v(TAG, mostRecentLocation.getProvider()+" "+data);
        	    sendData();
        	    
        	    lat = 0;
    	        lng = 0;
    	        alt = 0;
    	        speed = 0;
    	        tms = 0;
    	        hdg = 0;
    	        accuracy = 0;
	         handler.postDelayed(this, freq);
    	    }
	    }
	};
	
	public class MyLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location loc) {
		}

		@Override
		public void onProviderDisabled(String provider) {
			provider = lm.getBestProvider(criteria, true);
		}

		@Override
		public void onProviderEnabled(String provider) {
			provider = lm.getBestProvider(criteria, true);
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}
	}
}
	

