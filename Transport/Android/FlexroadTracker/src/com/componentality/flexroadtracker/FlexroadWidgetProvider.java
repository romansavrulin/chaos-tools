package com.componentality.flexroadtracker;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.util.Log;
import android.widget.RemoteViews;

public class FlexroadWidgetProvider extends AppWidgetProvider {
	
	String TAG = "Flexroad Tracker";

	private ComponentName watchWidget;
	public static String ACTION_WIDGET_B = "FlexroadButton";
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.flexroadwidget);
		watchWidget = new ComponentName( context, FlexroadWidgetProvider.class );
		
		Intent active = new Intent(context, FlexroadWidgetProvider.class);
		active.setAction(ACTION_WIDGET_B);
		PendingIntent actionPendingIntent = PendingIntent.getBroadcast(context, 0, active, 0);
		remoteViews.setOnClickPendingIntent(R.id.widget_button, actionPendingIntent);
		
		if(!isMyServiceRunning(context)){
			remoteViews.setImageViewResource(R.id.widget_button, R.drawable.kn_f2);
		}
		else {
			remoteViews.setImageViewResource(R.id.widget_button, R.drawable.kn_f1);
		}
		
		appWidgetManager.updateAppWidget(watchWidget, remoteViews );
		super.onUpdate(context, appWidgetManager, appWidgetIds);
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Vibrator vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		
		Log.v(TAG, "Widget action "+intent.getAction());
		
		if (intent.getAction().equals(ACTION_WIDGET_B)) {
			vibe.vibrate(20);
			if(!isMyServiceRunning(context)){
				context.startService(new Intent(FlexroadService.MY_SERVICE));
			}
			else {
				context.stopService(new Intent(FlexroadService.MY_SERVICE));
			}
		}
		super.onReceive(context, intent);
	}
	
	private boolean isMyServiceRunning(Context ctx) {
		SharedPreferences sPref = ctx.getSharedPreferences("flexroad",Context.MODE_PRIVATE);
		if(sPref.getBoolean("ServiceStarted", false))
			return true;
		else 
			return false;
	}
}
