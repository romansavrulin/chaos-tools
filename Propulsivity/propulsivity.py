#-------------------------------------------------------------------------------
# Name:        Propulsivity management console
# Purpose:     Doing manual management of propulsivity functions
#
# Author:      Konstantin A. Khait
#
# Created:     03/12/2013
# Copyright:   (c) Konstantin Khait, 2013
# Licence:     GPL
#-------------------------------------------------------------------------------

import argparse
import userproc
import documents
import tagging
import utilities
import session
import os

print("Propulsivity Manual Console")
print("Copyright (C) Componentality Oy, 2013")

# Do parsing of parameters
parser = argparse.ArgumentParser()
parser.add_argument('--rootdir')
parser.add_argument('--command')
parser.add_argument('--user')
parser.add_argument('--password')
parser.add_argument('--group')
parser.add_argument('--attribute')
parser.add_argument('--type')
parser.add_argument('--document')
parser.add_argument('--file')
parser.add_argument('--tag')
parser.add_argument('--value')
parser.add_argument('--granularity')
parser.add_argument('--duration')
parser.add_argument('--session')
args = parser.parse_args()
rootdir = args.rootdir
command = args.command
user = args.user
password = args.password
group = args.group
attribute = args.attribute
atype = args.type
document = args.document
file = args.file
tag = args.tag
value = args.value
granularity = args.granularity
duration = args.duration
session_id = args.session

if not rootdir:
    print("--rootdir is not provided")
    quit()

print("")

################################################################################

if (command == "adduser"):
    if not user or not password:
        print("--user and --password are required for adduser command")
        quit()

    result = userproc.userRegister(user, password, rootdir)
    if result == False:
        print("Can not add user. Maybe this name is already used")
    else:
        print("User " + user + " added")
    quit()

if (command == "login"):
    if not user or not password:
        print("--user and --password are required for login command")
        quit()

    result = userproc.userLogin(user, password, rootdir)
    if result == False:
        print("Login failed")
    else:
        print("Login successful")
    quit()

if (command == "rmuser"):
    result = False

    if not user:
        print("--user is required for rmuser command")
        quit()

    try:
        result = userproc.userUnregister(user, rootdir)
    except:
        pass

    if result == False:
        print("Removal failed. Maybe there is no such user")
    else:
        print("User " + user + " removed")
    quit()

if (command == "pass"):
    if not user or not password:
        print("--user and --password are required for pass command")
        quit()

    result = userproc.changePassword(user, password, rootdir)
    if result == False:
        print("Can not change password")
    else:
        print("Password changed")
    quit()

if (command == "lsuser"):
    users = userproc.listUsers(rootdir)
    for u in users:
        print(u)
    quit()

if (command == "addgroup"):
    if not user or not group:
        print("--user and --group are required for addgroup command")
        quit()

    result = userproc.addGroup(user, group, rootdir)
    print("Group added")
    quit()

if (command == "rmgroup"):
    if not user or not group:
        print("--user and --group are required for rmgroup command")
        quit()

    result = userproc.removeGroup(user, group, rootdir)
    print("Group removed")
    quit()

if (command == "lsgroup"):
    if not user:
        print("--user required for lsgroup command")
        quit()

    groups = userproc.listGroups(user, rootdir)
    for g in groups:
        print(g)
    quit()

if (command == "ingroup"):
    if not group:
        print("--group required for lsgroup command")
        quit()

    users = userproc.listByGroup(group, rootdir)
    for u in users:
        print(u)
    quit()

if (command == "addattr"):
    if not user or not attribute or not atype:
        print("--user, --type and --attribute are required for addattr command")
        quit()

    result = userproc.addAttribute(user, attribute, rootdir, atype)
    print("Attribute added")
    quit()

if (command == "rmattr"):
    if not user or not attribute or not atype:
        print("--user, --type and --attribute are required for rmattr command")
        quit()

    result = userproc.removeAttribute(user, attribute, rootdir, atype)
    print("Attribute removed")
    quit()

if (command == "lsattr"):
    if not user or not atype:
        print("--user and --type are required for lsattr command")
        quit()

    attrs = userproc.listAttributes(user, rootdir, atype)
    for a in attrs:
        print(a)
    quit()

if (command == "hasattr"):
    if not attribute or not atype:
        print("--attribute and --type are required for hasattr command")
        quit()

    users = userproc.listByAttribute(attribute, rootdir, atype)
    for u in users:
        print(u)
    quit()

################################################################################

if (command == "create"):
    if not document:
        print("--document required for create command")
        quit()
    documents.create(document, rootdir)
    print("Document " + document + " created")
    quit()

if (command == "rm"):
    if not document:
        print("--document required for rm command")
        quit()
    documents.remove(document, rootdir)
    print("Document " + document + " removed")
    quit()

if (command == "read"):
    if not document or not file:
        print("--document and --file required for read command")
        quit()

    try:
        f = open(file, 'w')
        content = documents.read(document, rootdir)
        f.write(content)
        f.close()
        print("\n\nDocument " + document + " read to file " + file)
    except:
        print("Can not read document")
    quit()

if (command == "write"):
    if not document or not file:
        print("--document and --file required for write command")
        quit()

    if not os.path.exists(file):
        print("File " + file + " not found")
        quit()

    try:
        f = open(file, 'r')
        content = f.read()
        documents.write(document, content, rootdir)
        f.close()
        print("Document " + document + " is written from file " + file)
    except:
        print("Can not write document")
    quit()

if (command == "ls"):
    docs = documents.list(rootdir)
    for d in docs:
        print(d)
    quit()

################################################################################

if (command == "addtag"):
    if not tag or not value or not document:
        print("--tag, --value and --document are required for addtag command")
        quit()

    result = tagging.addTag(documents.makeRecordName(document), tag, value, rootdir)
    print("Tag applied")
    quit()

if (command == "rmtag"):
    if not tag or not value or not document:
        print("--tag, --value and --document are required for rmtag command")
        quit()

    result = tagging.removeTag(documents.makeRecordName(document), tag, value, rootdir)
    print("Tag removed")
    quit()

if (command == "find"):
    if not tag or not value:
        print("--tag and --value are required for find command")
        quit()

    result = tagging.findByTagValue(tag, value, rootdir)
    for r in result:
        print(utilities.__restorehex(r))
    quit()

if (command == "findall"):
    if not tag:
        print("--tag required for findall command")
        quit()

    result = tagging.findByTag(tag, rootdir)
    for r in result:
        try:
            print(utilities.__restorehex(r))
        except:
            pass
    quit()

if (command == "get"):
    if not tag or not document:
        print("--tag and --document are required for get command")
        quit()

    result = tagging.getTagValue(documents.makeRecordName(document), tag, rootdir)
    print(result)
    quit()

if (command == "additag"):
    if not tag or not value or not document or not granularity:
        print("--tag, --value, --granularity and --document are required for additag command")
        quit()

    result = tagging.addIntervalTag(documents.makeRecordName(document), tag, value, granularity, rootdir)
    print("Tag applied")
    quit()

if (command == "rmitag"):
    if not tag or not value or not document or not granularity:
        print("--tag, --value, --granularity and --document are required for rmitag command")
        quit()

    result = tagging.removeIntervalTag(documents.makeRecordName(document), tag, value, granularity, rootdir)
    print("Tag removed")
    quit()

if (command == "findi"):
    if not tag or not value or not granularity:
        print("--tag, --granularity and --value are required for findi command")
        quit()

    result = tagging.findByValue(tag, value, rootdir)
    for r in result:
        print(utilities.__restorehex(r))
    quit()

if (command == "findix"):
    if not tag or not value or not granularity:
        print("--tag, --granularity and --value are required for findall command")
        quit()

    result = tagging.findByInterval(tag, value, granularity, rootdir)
    for r in result:
        print(utilities.__restorehex(r))
    quit()

if (command == "geti"):
    if not tag or not document:
        print("--tag and --document are required for geti command")
        quit()

    result = tagging.getIntervalTagValue(documents.makeRecordName(document), tag, rootdir)
    print(result)
    quit()

################################################################################

if (command == "mks"):
    if not user or not password or not duration:
        print("--duration, --user and --password are required for mks command")
        quit()

    result = session.createSession(user, password, rootdir, float(duration))
    if result == "":
        print("Wrong user login or password")
    else:
        print("\n" + result)
    quit()

if (command == "ends"):
    if not session_id:
        print("--session required for ends command")
        quit()

    session.endSession(session_id, rootdir)
    print("Session ended")
    quit()

if (command == "valids"):
    if not session_id:
        print("--session required for valids command")
        quit()

    result = session.validateSession(session_id, rootdir)
    if result:
        print("Session valid")
    else:
        print("Session expired or not present")
    quit()

if (command == "clears"):
    session.cleanExpiredSessions(rootdir)
    print("Expired sessions removed")
    quit()

if (command == "lss"):
    result = tagging.findByTagValue("type", "session", rootdir)
    print("\n")
    for r in result:
        if r == "":
            continue
        r = r[0 : len(r) - len("_session_")]
        status = session.validateSession(r, rootdir)
        if status:
            status = "Actual"
        else:
            status = "Expired"
        print(r + "\t" + status)
    print("\n")
    quit()

if (command == "getus"):
    if not session_id:
        print("--session required for getus command")
        quit()

    result = session.getSessionUser(session_id, rootdir)
    print(result)
    quit()

if (command == "tags"):
    if not session_id or not tag or not value:
        print("--session, --tag and --value are required for tags command")
        quit()

    session.tagSession(session_id, tag, value, rootdir)
    print("Session " + session_id + " tagged with " + tag + "=" + value)
    quit()

if (command == "gettags"):
    if not session_id or not tag :
        print("--session and --tag are required for gettags command")
        quit()

    result = session.getSessionTagValue(session_id, tag, rootdir)
    print(result)
    quit()

if (command == "renews"):
    if not session_id:
        print("--session required for renews command")
        quit()

    if not session.validateSession(session_id, rootdir):
        print("Session is invaid or expired")
        quit()

    session.renewSession(session_id, rootdir)
    quit()

################################################################################

print("List of commands")
print("\tUser management:")
print("\t\tadduser  - add new user (register user)")
print("\t\trmuser   - remove existing user (unregister user)")
print("\t\tlogin    - check user login/password")
print("\t\tpass     - change user password")
print("\t\tlsuser   - list all users")
print("\t\taddgroup - include user to group")
print("\t\trmgroup  - exclude user from group")
print("\t\tlsgroup  - list all groups for given user")
print("\t\tingroup  - list all users for given group")
print("\t\taddattr  - add attribute for given user")
print("\t\trmattr   - remove attribute for given user")
print("\t\tlsattr   - list all attributes for given user")
print("\t\thasattr  - list all users having given attribute")
print("\tDocuments management:")
print("\t\tcreate   - create new document")
print("\t\trm       - remove existing document")
print("\t\tread     - read document to file")
print("\t\twrite    - write document to file")
print("\t\tls       - list all documents in the database")
print("\tTagging of documents:")
print("\t\taddtag   - apply tag for the document")
print("\t\trmtag    - remove tag from the document")
print("\t\tfind     - find documents by tag")
print("\t\tfindall  - find documents having given tag")
print("\t\tget      - get tag value for the given document")
print("\t\tadditag  - apply interval tag for the document")
print("\t\trmitag   - remove interval tag from the document")
print("\t\tfindi    - find documents by interval tag (precise value)")
print("\t\tfindix   - find documents by tag interval")
print("\t\tgeti     - get interval tag value for the given document")

print("\tSession management:")
print("\t\tmks      - make new session")
print("\t\tends     - end existing session")
print("\t\tvalids   - validate session")
print("\t\tclears   - clear expired sessions")
print("\t\tlss      - list all sessions")
print("\t\ttags     - apply given tag for a session")
print("\t\tgettags  - get tag value for a session tag")
print("\t\tgetus    - get user for a session")

print("")

