#-------------------------------------------------------------------------------
# Name:        Manage records
# Purpose:     Work with pairs of NAME = VALUE (namely records or record sets)
#
# Author:      Konstantin Khait
#
# Created:     30/11/2013
# Copyright:   (c) Componentality Oy, 2013
# Licence:     LGPL
#-------------------------------------------------------------------------------
import json

# Get context (key=value pairs) from the string with pairs separated by "\n"
def parseRecordSet(recordset):
    result = {}
    recs = recordset.split('\n')
    for record in recs:
        try:
            series = record.split('=')
            idx = 0
            for s in series:
                if idx == 0:
                    key = s.strip()
                    val = ""
                else:
                    if idx > 1:
                        val += '='
                    val += s
                idx += 1
            result[key] = val
        finally:
            pass
    return result

# Compose context string from dictionary
def storeRecordSet(records):
    result = ""
    for record_key in records.keys():
        try:
            if result != '':
                result += '\n'
            result += record_key + '=' + str(records[record_key])
        finally:
            pass
    return result

# Add key-value pair to the record (almost never used)
def addToRecordSet(key, value, recordset):
    recordset[key] = value
    return recordset

# Remove key-value pair from the record (almost never used)
def removeFromRecordSet(key, recordset):
    try:
        del recordset[key]
    finally:
        pass
    return recordset

# Convert JSON object to a record
def fromJSON(jstr):
    recordset = {}
    try:
        jobj = json.loads(jstr)
        for j in jobj.keys():
            if j != "":
                recordset[j] = jobj[j]
    except:
        pass
    return recordset

# Convert record to JSON object
def toJSON(recordset):
    result = "{\n"
    frun = True
    for r in recordset.keys():
        if r != "":
            if not frun:
                result += ",\n"
            frun = False
            result += "  " + '"'
            result += r + '":"'
            result += recordset[r] + '"'
    result += "\n}"
    return result

# Sort records by given field
def sortRecords(recarray, keyfield, json = False):
    if json == True:
        i = 0
        while i < len(recarray):
            recarray[i] = fromJSON(recarray[i])
            i += 1
    i = 0
    while i <= len(recarray) / 2:
        j = 1
        while j < len(recarray):
            prevrec = recarray[j - 1]
            rec = recarray[j]
            if (keyfield in prevrec.keys()) and (keyfield in rec.keys()):
                prevval = prevrec[keyfield]
                val = rec[keyfield]
                if val < prevval:
                    temp = recarray[j - 1]
                    recarray[j - 1] = recarray[j]
                    recarray[j] = temp
            else:
                if not (keyfield in prevrec.keys()):
                    del recarray[j - 1]
                if not (keyfield in rec.keys()):
                    del recarray[j]
            j += 1
        i += 1
    if json == True:
        i = 0
        while i < len(recarray):
            recarray[i] = toJSON(recarray[i])
            i += 1
    return recarray

# Select only records with the value for keyfield more or equal than limit
def greaterOnlyFilter(recarray, keyfield, limit, number = True, json = False):
    if json == True:
        i = 0
        while i < len(recarray):
            recarray[i] = fromJSON(recarray[i])
            i += 1
    result = []
    for rec in recarray:
        if keyfield in rec.keys():
            value = rec[keyfield]
            try:
                if (number):
                    value = float(value)
                    limit = float(limit)
                if value >= limit:
                    result.append(rec)
            except:
                pass
    if json == True:
        i = 0
        while i < len(recarray):
            recarray[i] = toJSON(recarray[i])
            i += 1
    return result

# Filter records having keyfield value less or equal than given limit
def smallerOnlyFilter(recarray, keyfield, limit, number = True, json = False):
    if json == True:
        i = 0
        while i < len(recarray):
            recarray[i] = fromJSON(recarray[i])
            i += 1
    result = []
    for rec in recarray:
        if keyfield in rec.keys():
            value = rec[keyfield]
            try:
                if (number):
                    value = float(value)
                    limit = float(limit)
                if value <= limit:
                    result.append(rec)
            except:
                pass
    if json == True:
        i = 0
        while i < len(recarray):
            recarray[i] = toJSON(recarray[i])
            i += 1
    return result
