#-------------------------------------------------------------------------------
# Name:        User registration and logon
# Purpose:     Process user registration, deletion and logging on
#
# Author:      Konstantin Khait
#
# Created:     30/11/2013
# Copyright:   (c) Componentality Oy, 2013
# Licence:     GPL
#-------------------------------------------------------------------------------

import pseudobase
import utilities
import records

def userRegister(username, password, rootdir):
    uname = username
    pswdhash = utilities.__gethash(password)
    content = {}
    content = records.addToRecordSet("user", uname, content)
    content = records.addToRecordSet("pass", pswdhash, content)
    content = records.addToRecordSet("usertag", "", content)
    content = records.storeRecordSet(content)
    record = utilities.__makehex(uname)
    pseudobase.lockrecord(record, rootdir, 3)
    rold = pseudobase.load(record, rootdir)
    if (rold == ""):
        pseudobase.save(record, content, content, rootdir)
    pseudobase.unlockrecord(record, rootdir)
    return rold == ""

def changePassword(username, password, rootdir):
    uname = username
    pswdhash = utilities.__gethash(password)
    record = pseudobase.load(utilities.__makehex(uname), rootdir)
    recordset = records.parseRecordSet(record)
    pseudobase.removeAttribute(utilities.__makehex(uname), "pass=" + recordset["pass"], rootdir)
    del recordset["pass"]
    recordset["pass"] = pswdhash
    content = records.storeRecordSet(recordset)
    pseudobase.save(utilities.__makehex(uname), content, None, rootdir)

def userLogin(username, password, rootdir):
    uname = username
    pswdhash = utilities.__gethash(password)
    record = pseudobase.load(utilities.__makehex(uname), rootdir)
    recordset = records.parseRecordSet(record)
    if not ("user" in recordset):
        return False
    if not ("pass" in recordset):
        return False
    userinfo = recordset["user"]
    pswd = recordset["pass"]
    return (userinfo == uname) and (pswd == pswdhash)

def userUnregister(username, rootdir):
    uname = username
    pseudobase.remove(utilities.__makehex(uname), rootdir)

def listUsers(rootdir):
    lst = pseudobase.list("usertag=", rootdir)
    result = []
    for l in lst:
        l = utilities.__restorehex(l)
        result.append(l)
    return result

def __addAttribute(username, attribute, rootdir, prefix):
    uname = username
    record = utilities.__makehex(uname)
    pseudobase.lockrecord(record, rootdir, 3)
    pseudobase.addAttribute(record, prefix + attribute, rootdir)
    pseudobase.unlockrecord(record, rootdir)

def __removeAttribute(username, attribute, rootdir, prefix):
    uname = username
    record = utilities.__makehex(uname)
    pseudobase.lockrecord(record, rootdir, 3)
    pseudobase.removeAttribute(record, prefix + attribute, rootdir)
    pseudobase.unlockrecord(record, rootdir)

def __listAttribute(username, rootdir, prefix):
    result = []
    uname = username
    record = utilities.__makehex(uname)
    pseudobase.lockrecord(record, rootdir, 3)
    metadata = pseudobase.loadMetadata(record, rootdir)
    for m in metadata:
        if m[0:len(prefix)] == prefix:
            result.append(m[len(prefix):])
    pseudobase.unlockrecord(record, rootdir)
    return result

def __listByAttribute(attribute, rootdir, prefix):
    lst = pseudobase.list(prefix + attribute, rootdir)
    result = []
    for l in lst:
        l = utilities.__restorehex(l)
        result.append(l)
    return result

def addGroup(username, group, rootdir):
    return __addAttribute(username, group, rootdir, "group.")

def removeGroup(username, group, rootdir):
    return __removeAttribute(username, group, rootdir, "group.")

def listGroups(username, rootdir):
    return __listAttribute(username, rootdir, "group.")

def listByGroup(group, rootdir):
    return __listByAttribute(group, rootdir, "group.")

def addAttribute(username, attr, rootdir, attrtype):
    return __addAttribute(username, attr, rootdir, attrtype)

def removeAttribute(username, attr, rootdir, attrtype):
    return __removeAttribute(username, attr, rootdir, attrtype)

def listAttributes(username, rootdir, attrtype):
    return __listAttribute(username, rootdir, attrtype)

def listByAttribute(attr, rootdir, attrtype):
    return __listByAttribute(attr, rootdir, attrtype)
