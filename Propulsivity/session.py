#-------------------------------------------------------------------------------
# Name:        Session management
# Purpose:     Manage session object
#
# Author:      Konstantin Khait
#
# Created:     02/12/2013
# Copyright:   (c) Componentality Oy, 2013
# Licence:     GPL
#-------------------------------------------------------------------------------

import time
import userproc
import utilities
import pseudobase
import tagging

def createSession(user, password, rootdir, duration):
    if userproc.userLogin(user, password, rootdir) == False:
        return ""
    session_id = utilities.__gethash(user + password + str(time.time()))
    session_id = utilities.__makehex(session_id)
    session = session_id + "_session_"
    pseudobase.save(session, session_id, None, rootdir)
    tagging.addIntervalTag(session, "create", str(time.time()), "1", rootdir)
    tagging.addTag(session, "expiration", str(duration), rootdir)
    tagging.addTag(session, "type", "session", rootdir)
    tagging.addTag(session, "user", user, rootdir)
    return session_id

def getSessionUser(session_id, rootdir):
    session = session_id + "_session_"
    return tagging.getTagValue(session, "user", rootdir)

def endSession(session_id, rootdir):
    pseudobase.remove(session_id + "_session_", rootdir)

def validateSession(session_id, rootdir):
    session = session_id + "_session_"
    try:
        content = pseudobase.load(session, rootdir)
        if content != session_id:
            return False
        created = tagging.getIntervalTagValue(session, "create", rootdir)
        expired = tagging.getTagValue(session, "expiration", rootdir)
        if time.time() - float(created) > float(expired):
            return False
    except:
        return False
    return True

def cleanExpiredSessions(rootdir):
    sessions = tagging.findByTagValue("type", "session", rootdir)
    for session in sessions:
        if session == "":
            continue
        r = session[0 : len(session) - len("_session_")]
        if validateSession(r, rootdir) == False:
            try:
                pseudobase.remove(session, rootdir)
            except:
                pass

def tagSession(session_id, tag, value, rootdir):
    session = session_id + "_session_"
    tagging.addTag(session, tag, value, rootdir)

def getSessionTagValue(session_id, tag, rootdir):
    session = session_id + "_session_"
    return tagging.getTagValue(session, tag, rootdir)

def renewSession(session_id, rootdir):
    session = session_id + "_session_"
    created = tagging.getIntervalTagValue(session, "create", rootdir)
    tagging.removeIntervalTag(session, "create", created, "1", rootdir)
    tagging.addIntervalTag(session, "create", str(time.time()), "1", rootdir)


